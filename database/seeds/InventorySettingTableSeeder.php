<?php

use Illuminate\Database\Seeder;

class InventorySettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('inventory_settings')->insert([
            'type' => 'inventory',
            'name' => 'inventory_functionality',
            'status' => 0,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('inventory_settings')->insert([
            'type' => 'inventory',
            'name' => 'show_selling_price',
            'status' => 0,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('inventory_settings')->insert([
            'type' => 'inventory',
            'name' => 'display_custom_field',
            'status' => 0,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('inventory_settings')->insert([
            'type' => 'inventory',
            'name' => 'item_available_location',
            'status' => 0,
            'created_at' => now(),
            'updated_at' => now()
        ]);


        DB::table('inventory_settings')->insert([
            'type' => 'item_display',
            'name' => 'disply_photo_inventory_list',
            'status' => 0,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('inventory_settings')->insert([
            'type' => 'item_display',
            'name' => 'display_item_photo_on_po',
            'status' => 0,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('inventory_settings')->insert([
            'type' => 'item_display',
            'name' => 'display_item_photo_on_wers',
            'status' => 0,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('inventory_settings')->insert([
            'type' => 'item_display',
            'name' => 'display_item_photos_on_invoices',
            'status' => 0,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('inventory_settings')->insert([
            'type' => 'shipping',
            'name' => 'track_shipments',
            'status' => 0,
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
