<?php

use Illuminate\Database\Seeder;

class BillingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('billings')->insert([
            'type' => 'text',
            'name' => 'invoice_name',
            'text' => null,
            'status' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('billings')->insert([
            'type' => 'text',
            'name' => 'estimate_name',
            'text' => null,
            'status' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('billings')->insert([
            'type' => 'number',
            'name' => 'estimate_expiry',
            'text' => null,
            'status' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('billings')->insert([
            'type' => 'textarea',
            'name' => 'default_payment_term',
            'text' => null,
            'status' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('billings')->insert([
            'type' => 'checkbox',
            'name' => 'set_zeroed_out_invoice',
            'text' => null,
            'status' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('billings')->insert([
            'type' => 'checkbox',
            'name' => 'prevent_duplicate_line_item',
            'number' => null,
            'status' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('billings')->insert([
            'type' => 'checkbox',
            'name' => 'show_sku',
            'text' => null,
            'status' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('billings')->insert([
            'type' => 'checkbox',
            'name' => 'add_item_description',
            'text' => null,
            'status' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('billings')->insert([
            'type' => 'checkbox',
            'name' => 'pdf_added_language',
            'text' => null,
            'status' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
