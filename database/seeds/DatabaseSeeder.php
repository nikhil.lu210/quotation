<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CompaniesTableSeeder::class);
        $this->call(InventorySettingTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(BillingsTableSeeder::class);
    }
}
