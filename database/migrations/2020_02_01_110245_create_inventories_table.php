<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventories', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('cat_id')
                  ->unsigned();
            $table->foreign('cat_id')
                  ->references('id')
                  ->on('categories')
                  ->onDelete('cascade');

            $table->string('name');
            $table->date('purchased_date')->nullable();
            $table->text('description')->nullable();
            // $table->binary('avatar')->nullable();
            $table->string('inventory_type')->nullable();
            $table->decimal('purchase_price_per_unit', 10, 2)->nullable();
            $table->decimal('default_selling_price', 10, 2);
            $table->string('sku')->nullable();
            $table->integer('global_quantity')->nullable();
            $table->integer('low_quantity')->nullable();

            $table->bigInteger('unit_id')
                  ->unsigned()->nullable();

            $table->foreign('unit_id')
                  ->references('id')
                  ->on('units')
                  ->onDelete('cascade');

            $table->string('sort_priority')->nullable();
            $table->decimal('purchase_price', 10, 2)->nullable();
            $table->string('serial_number')->nullable();

            $table->string('inventory_url')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });

        DB::statement("ALTER TABLE inventories ADD avatar MEDIUMBLOB after name");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventories');

        Schema::table("inventories", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
