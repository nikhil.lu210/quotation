<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();

            $table->tinyInteger('status')->default(1);  // value ( 1/0 )
            $table->string('lang')->default('en');
            $table->bigInteger('company_id')
                  ->unsigned()->default(1);

            $table->foreign('company_id')
                  ->references('id')
                  ->on('companies')
                  ->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });

        DB::statement("ALTER TABLE users ADD avatar MEDIUMBLOB after name");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');

        Schema::table("users", function ($table) {
            $table->dropSoftDeletes();
        });

    }
}
