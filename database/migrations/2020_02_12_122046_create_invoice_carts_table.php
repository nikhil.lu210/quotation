<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_carts', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('item_id')
                  ->unsigned()->default(1);

            $table->foreign('item_id')
                  ->references('id')
                  ->on('inventories')
                  ->onDelete('cascade');

            $table->bigInteger('invoice_id')
                  ->unsigned()->default(1);

            $table->foreign('invoice_id')
                  ->references('id')
                  ->on('invoices')
                  ->onDelete('cascade');

            $table->integer('quantity');
            $table->double('total_price', 8, 2);
            $table->double('custom_price', 8, 2);
            // $table->double('tax', 8, 2)->default(0.00);
            $table->double('discount', 8, 2)->default(0.00);
            $table->double('dis_price', 8, 2);
            $table->double('original_price', 8, 2);

            $table->string('secret_id');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_carts');

        Schema::table("invoice_carts", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
