<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('company_id')
                  ->unsigned();
            $table->foreign('company_id')
                  ->references('id')
                  ->on('companies')
                  ->onDelete('cascade');

            $table->bigInteger('customer_id')
                  ->unsigned();
            $table->foreign('customer_id')
                  ->references('id')
                  ->on('customers')
                  ->onDelete('cascade');

            $table->bigInteger('currency_id')
                  ->unsigned()->nullable();
            $table->foreign('currency_id')
                  ->references('id')
                  ->on('currencies')
                  ->onDelete('cascade');

            $table->bigInteger('sales_person')
                  ->unsigned()->nullable();
            $table->foreign('sales_person')
                  ->references('id')
                  ->on('users')
                  ->onDelete('cascade');

            $table->string('inv_number');
            $table->date('exp_date')->nullable();
            $table->string('client_email')->nullable();
            $table->string('mobile_number')->nullable();
            $table->text('client_note')->nullable();
            $table->text('payment_term')->nullable();


            $table->decimal('discount', 8, 2)->nullable();

            $table->decimal('tax', 8, 2)->nullable();

            $table->decimal('total_dis_price', 8, 2)->nullable();
            $table->decimal('total_cost', 8, 2)->nullable();
            $table->decimal('total_profit', 8, 2)->nullable();
            $table->decimal('total_profit_percent', 8, 2)->nullable();

            $table->tinyInteger('status')->default(1);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');

        Schema::table("invoices", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
