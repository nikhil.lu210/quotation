<?php
// General
Route::group([
    'prefix' => 'general', //URL
    'as' => 'general.', //Route
    'namespace' => 'General', // Controller
],
    function(){
        Route::get('/company', 'GeneralController@companyIndex')->name('company.index');
        Route::post('/company/update/{company_id}', 'GeneralController@companyUpdate')->name('company.update');


        Route::get('/account', 'GeneralController@accountIndex')->name('account.index');
        Route::post('/account', 'GeneralController@accountStore')->name('account.store');
        Route::post('/account/update/{id}', 'GeneralController@accountUpdate')->name('account.update');
        Route::post('/account/password/{id}', 'GeneralController@accountPassword')->name('account.password');
        Route::get('/account/profile/{id}', 'GeneralController@accountShow')->name('account.show');
        Route::get('/account/status/{id}', 'GeneralController@accountStatus')->name('account.status');




        Route::post('/change_lang/{lang}', 'GeneralController@changeLang')->name('change_lang');
        Route::post('/change_company/{id}', 'GeneralController@changeCompany')->name('change_company');

    }
);
