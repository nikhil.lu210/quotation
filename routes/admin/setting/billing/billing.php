<?php
// Billing
Route::group([
    'prefix' => 'billing', //URL
    'as' => 'billing.', //Route
    'namespace' => 'Billing', // Controller
],
    function(){
        Route::get('/billing', 'BillingController@billingIndex')->name('billing.index');
        Route::post('/billing/update', 'BillingController@billingUpdate')->name('billing.update');


        Route::post('/currency/store', 'BillingController@currencyStore')->name('currency.store');
        Route::post('/currency/update', 'BillingController@currencyUpdate')->name('currency.update');
        Route::get('/currency/status/{id}', 'BillingController@currencyStatus')->name('currency.status');
        Route::get('/currency/is_default/{id}', 'BillingController@isDefault')->name('currency.is_default');


        Route::get('/tax', 'BillingController@taxIndex')->name('tax.index');
        Route::post('/tax', 'BillingController@taxStore')->name('tax.store');
        Route::post('/tax/update', 'BillingController@taxUpdate')->name('tax.update');
        Route::get('/tax/status/{id}', 'BillingController@taxStatus')->name('tax.status');
        Route::get('/tax/is_default/{id}', 'BillingController@taxIsDefault')->name('tax.is_default');

        
        // Route::get('/email', 'BillingController@emailIndex')->name('email.index');
    }
);
