<?php
// Inventory
Route::group([
    'prefix' => 'inventory', //URL
    'as' => 'inventory.', //Route
    'namespace' => 'Inventory', // Controller
],
    function(){
        /**
         * Inventory Settings
         */
        Route::get('/inventory', 'InventoryController@inventoryIndex')->name('inventory.index');
        Route::post('/inventory', 'InventoryController@changeInventoryStatus')->name('change_status');


        /**
         * Unit Settings
         */
        Route::get('/unit', 'InventoryController@unitIndex')->name('unit.index');
        Route::post('/unit/store', 'InventoryController@unitStore')->name('unit.store');


        /**
         * Shipping Settings
         */
        // Route::get('/shipping', 'InventoryController@shippingIndex')->name('shipping.index');
    }
);
