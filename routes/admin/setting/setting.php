<?php
// Setting
Route::group([
    'prefix' => 'setting', //URL
    'as' => 'setting.', //Route
    'namespace' => 'Setting', // Controller
],
    function(){
        /* ==================================
        ============< General Settings >============
        ===================================*/
        include_once 'general/general.php';

        /* ==================================
        ============< Inventory >============
        ===================================*/
        include_once 'inventory/inventory.php';

        /* ==================================
        ============< Billing >============
        ===================================*/
        include_once 'billing/billing.php';
    }
);
