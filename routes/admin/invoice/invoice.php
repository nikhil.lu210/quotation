<?php
// Invoice
Route::group([
    'prefix' => 'quotation', //URL
    'as' => 'invoice.', //Route
    'namespace' => 'Invoice', // Controller
],
    function(){
        Route::get('/quotation_list', 'InvoiceController@invoiceIndex')->name('invoice.index');
        Route::get('/quotation_list/show/{id}', 'InvoiceController@invoiceShow')->name('invoice.show');
        Route::get('/quotation_list/edit/{id}', 'InvoiceController@invoiceEdit')->name('invoice.edit');
        Route::post('/quotation_list/update/{id}', 'InvoiceController@invoiceUpdate')->name('invoice.update');

        Route::get('/new_quotation', 'InvoiceController@invoiceCreate')->name('invoice.create');
        Route::get('/get_item/{id}', 'InvoiceController@getItem')->name('get_item');
        Route::post('/new_invoice', 'InvoiceController@invoiceStore')->name('invoice.store');

        Route::get('/get_customer/{id}', 'InvoiceController@getCustomer')->name('get_customer');


        Route::get('/pdf_generate/{id}', 'InvoiceController@pdfGenerate')->name('pdf_generate');

        Route::get('/send_email/{invoice_id}', 'InvoiceController@sendEmail')->name('send.email');

        Route::get('/pdf_view/{id}', 'InvoiceController@pdfView')->name('pdf.view');
    }
);
