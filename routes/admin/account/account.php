<?php
// Account
Route::group([
    'prefix' => 'account', //URL
    'as' => 'account.', //Route
    'namespace' => 'Account', // Controller
],
    function(){

        Route::get('/customer_list', 'AccountController@customerIndex')->name('customer.index');

        Route::get('/new_customer', 'AccountController@customerCreate')->name('customer.create');
        Route::post('/new_customer', 'AccountController@customerStore')->name('customer.store');
        Route::post('/update_customer/{id}', 'AccountController@customerUpdate')->name('customer.update');

        Route::get('/delete_customer/{id}', 'AccountController@customerDelete')->name('customer.destroy');
        Route::get('/change_status/{id}', 'AccountController@customerStatus')->name('customer.status');
        Route::get('/customer_list/details/{id}', 'AccountController@customerShow')->name('customer.show');
    }
);
