<?php

// Admin Routes
Route::group([
    'prefix' => 'admin', // URL
    'as' => 'admin.', // Route
    'namespace' => 'Admin', // Controller
],
    function(){
        /* ==================================
        ============< Dashboard >============
        ===================================*/
        include_once 'dashboard/dashboard.php';


        /* ==================================
        ============< Account >============
        ===================================*/
        include_once 'account/account.php';


        /* ==================================
        ============< Inventory >============
        ===================================*/
        include_once 'inventory/inventory.php';


        /* ==================================
        ============< Invoice >============
        ===================================*/
        include_once 'invoice/invoice.php';


        /* ==================================
        ============< Setting >============
        ===================================*/
        include_once 'setting/setting.php';
    }
);
