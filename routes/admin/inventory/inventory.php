<?php
// Inventory
Route::group([
    'prefix' => 'inventory', //URL
    'as' => 'inventory.', //Route
    'namespace' => 'Inventory', // Controller
],
    function(){
        Route::get('/inventory_list', 'InventoryController@inventoryIndex')->name('inventory.index');
        Route::post('/inventory_store', 'InventoryController@inventoryStore')->name('inventory.store');
        Route::post('/inventory_update/{id}', 'InventoryController@inventoryUpdate')->name('inventory.update');
        Route::get('/inventory_show/{id}', 'InventoryController@inventoryShow')->name('inventory.show');
        Route::get('/inventory_delete/{id}', 'InventoryController@inventoryDelete')->name('inventory.delete');


        //category control route
        Route::get('/category', 'InventoryController@categoryIndex')->name('category.index');
        Route::post('/category/store', 'InventoryController@categoryStore')->name('category.store');
        Route::get('/category/destroy/{id}', 'InventoryController@categoryDestroy')->name('category.destroy');
        Route::get('/category/change_status/{id}', 'InventoryController@categoryStatus')->name('category.status');
    }
);
