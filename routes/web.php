<?php

Route::redirect('/', '/admin');


Auth::routes();


// Route::get('/', 'Admin\Dashboard\DashboardController@index');
Route::get('/email/pdf_generate/{id}', 'Admin\Invoice\InvoiceController@pdfGenerate')->name('email.pdf_generate');

/*===================================
===========< Admin Routes >==========
===================================*/
Route::group(
    [
        'middleware'    => ['auth'],
    ],
    function () {
        include_once 'admin/admin.php';
    }
);
