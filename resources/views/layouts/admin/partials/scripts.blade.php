<script src="{{ asset('assets/js/vendor.js') }}"></script>

@yield('script_links')

<script src="{{ asset('assets/js/app.min.js') }}"></script>
{{-- Confirmation Alert --}}
<script src="{{ asset('assets/js/confirmation_alert/jquery-confirmv3.3.2.min.js') }}"></script>

<script src="{{ asset('assets/js/script.js') }}"></script>
<script src="{{ asset('assets/js/responsive.js') }}"></script>

<script>
$(document).ready(function(){
    $('.confirmation').confirm({
        type: 'dark',
        typeAnimated: true,
        title: "{{ __('Confirmation Alert') }}",
        content: "{{ __('Are You Sure?') }}",
        confirmButtonClass: 'btn-info',
        cancelButtonClass: 'btn-danger',
        confirmButton: 'Yes',
        cancelButton: 'NO',
        animation: 'zoom',
        closeAnimation: 'scale',
        confirm: function(){
            alert('Confirmed!');
        },
        cancel: function(){
            alert('Canceled!')
        }
    });
});
</script>
<script>
    $.ajaxSetup({
        headers: {

            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

        }
    });
</script>

@yield('custom_script')
