<!-- Footer START -->
<footer class="content-footer">
    <div class="footer">
        <div class="copyright go-right">
            <span>Copyright © 2020 <b class="text-primary">company_name</b>. All rights reserved || Developed By <b><a href="javascript:void(0);" class="text-info">developer_name</a></b></span>
            {{-- <span class="go-right">
                <a href="#" class="text-gray mrg-right-15">Term &amp; Conditions</a>
                <a href="#" class="text-gray">Privacy &amp; Policy</a>
            </span> --}}
        </div>
    </div>
</footer>
<!-- Footer END -->
