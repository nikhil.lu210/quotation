<div class="header navbar">
    <div class="header-container">
        <ul class="nav-left">
            <li>
                <a class="side-nav-toggle" href="javascript:void(0);">
                    <i class="ti-view-grid"></i>
                </a>
            </li>
        </ul>
        <ul class="nav-right">
            {{-- ===================< User Profile Starts >================ --}}
            <li class="user-profile dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    @if (auth()->user()->avatar != null)
                        <img class="profile-img img-fluid" src="{{ auth()->user()->avatar }}" alt="">
                    @else
                        <img class="profile-img img-fluid" src="{{ asset('assets/images/others/img-10.jpg') }}" alt="">
                    @endif
                    <div class="user-info">
                        <span class="name pdd-right-5">{{ auth()->user()->name }}</span>
                        <i class="ti-angle-down font-size-10"></i>
                    </div>
                </a>

                @if(App::isLocale('hebrew'))
                    <ul class="dropdown-menu text-right">
                        <li>
                            <a href="{{ route('admin.setting.general.account.show', ['id' => encrypt(auth()->user()->id)]) }}">
                                <span>{{ __('Profile') }}</span>
                                <i class="ti-user pdd-right-10"></i>
                            </a>
                        </li>
                        <li role="separator" class="divider"></li>
                        <li>
                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <span>{{ __('Logout') }}</span>
                                <i class="ti-power-off pdd-right-10"></i>
                            </a>
                        </li>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </ul>
                @else
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ route('admin.setting.general.account.show', ['id' => encrypt(auth()->user()->id)]) }}">
                                <i class="ti-user pdd-right-10"></i>
                                <span>{{ __('Profile') }}</span>
                            </a>
                        </li>
                        <li role="separator" class="divider"></li>
                        <li>
                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="ti-power-off pdd-right-10"></i>
                                <span>{{ __('Logout') }}</span>
                            </a>
                        </li>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </ul>
                @endif
            </li>
            {{-- ===================< User Profile Ends >================ --}}


            {{-- ===================< Language Starts >================ --}}
            <li class="user-profile dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    @if(App::isLocale('hebrew'))
                        <img class="profile-img img-fluid" src="{{ asset('assets/images/flags/isr_heb.png') }}" alt="English / Hebrew" style="height: 20px; width: 20px; margin-top: 22px;">
                        <div class="user-info">
                            {{-- <span class="name pdd-right-3">English</span> --}}
                            <span class="name pdd-right-3">עברית</span>
                            <i class="ti-angle-down font-size-10"></i>
                        </div>
                    @else
                        <img class="profile-img img-fluid" src="{{ asset('assets/images/flags/uk_eng.png') }}" alt="English / Hebrew" style="height: 20px; width: 20px; margin-top: 22px;">
                        <div class="user-info">
                            <span class="name pdd-right-3">English</span>
                            {{-- <span class="name pdd-right-3">עברית</span> --}}
                            <i class="ti-angle-down font-size-10"></i>
                        </div>
                    @endif
                </a>
                <ul class="dropdown-menu language text-right">
                    <li>
                        <a href="{{ route('admin.setting.general.change_lang', ['lang' => 'en']) }}" onclick="event.preventDefault(); document.getElementById('change_lang_en').submit();">
                            <span>English</span>
                            <img class="profile-img img-fluid" src="{{ asset('assets/images/flags/uk_eng.png') }}" alt="">
                        </a>
                    </li>
                    <form id="change_lang_en" action="{{ route('admin.setting.general.change_lang', ['lang' => 'en']) }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                    <li role="separator" class="divider"></li>
                    <li>
                        <a href="{{ route('admin.setting.general.change_lang', ['lang' => 'hebrew']) }}" onclick="event.preventDefault(); document.getElementById('change_lang_hebrew').submit();">
                            {{-- <span>Hebrew</span> --}}
                            <span>עברית</span>
                            <img class="profile-img img-fluid" src="{{ asset('assets/images/flags/isr_heb.png') }}" alt="">
                        </a>
                    </li>
                    <form id="change_lang_hebrew" action="{{ route('admin.setting.general.change_lang', ['lang' => 'hebrew']) }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </ul>
            </li>
            {{-- ===================< Language Ends >================ --}}


            {{-- ===================< Comapany Starts >================ --}}
            <li class="user-profile dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <img class="profile-img img-fluid" src="{{ auth()->user()->company->logo }}" alt="Company Logo" style="height: 20px; width: 20px; margin-top: 22px;">
                    <div class="user-info">
                        <span class="name pdd-right-3">{{ auth()->user()->company->name }}</span>
                        <i class="ti-angle-down font-size-10"></i>
                    </div>
                </a>
                <ul class="dropdown-menu language text-right">
                    @php
                        $companies = App\Models\Company::all();
                    @endphp
                    @foreach ($companies as $company)
                    <li>
                        <a href="{{ route('admin.setting.general.change_company', ['id' => $company->id]) }}" onclick="event.preventDefault(); document.getElementById('change_lang_{{ $company->id }}').submit();">
                            <span>{{ $company->name }}</span>
                            <img class="profile-img img-fluid" src="{{ $company->logo }}" alt="logo">
                        </a>
                    </li>
                    <form id="change_lang_{{ $company->id }}" action="{{ route('admin.setting.general.change_company', ['id' => $company->id]) }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                    @endforeach
                </ul>
            </li>
            {{-- ===================< Comapany Ends >================ --}}
        </ul>
    </div>
</div>
