<!-- Side Nav START -->
<div class="side-nav">
    <div class="side-nav-inner">
        <div class="side-nav-logo">
            <a href="{{ route('admin.dashboard.index') }}">
                {{-- <div class="logo logo-dark" style="background-image: url('{{ asset('assets/images/logo/logo.png') }}')"></div>
                <div class="logo logo-white" style="background-image: url('{{ asset('assets/images/logo/logo-white.png') }}')"></div> --}}

                <div class="logo logo-dark" style="background-image: url('{{ auth()->user()->company->logo }}')"></div>
                <div class="logo logo-white" style="background-image: url('{{ auth()->user()->company->logo }}')"></div>
            </a>
            <div class="mobile-toggle side-nav-toggle">
                <a href="#">
                    <i class="ti-arrow-circle-left"></i>
                </a>
            </div>
        </div>
        <ul class="side-nav-menu scrollable">

            {{-- ===================< Dashboard Start >====================== --}}
            <li class="nav-item {{ Request::is('admin') ? 'active' : '' }}">
                <a class="mrg-top-30" href="{{ route('admin.dashboard.index') }}">
                    <span class="icon-holder">
                        <i class="ti-home"></i>
                    </span>
                    {{-- {{ dd("adf") }} --}}
                    <span class="title">{{ __('Dashboard') }}</span>
                </a>
            </li>
            {{-- ===================< Dashboard Ends >====================== --}}




            {{-- ===================< Account Start >====================== --}}
            <li class="nav-item dropdown {{ Request::is('admin/account*') ? 'open' : '' }}">
                <a class="dropdown-toggle" href="javascript:void(0);">
                    <span class="icon-holder">
                        <i class="ti-user"></i>
                    </span>
                    <span class="title">{{ __('Customers') }}</span>
                    <span class="arrow">
                        <i class="ti-angle-right"></i>
                    </span>
                </a>
                <ul class="dropdown-menu">
                    <li class="{{ Request::is('admin/account/customer_list*') ? 'active' : '' }}">
                        <a href="{{ route('admin.account.customer.index') }}">{{ __('Customer List') }}</a>
                    </li>
                    <li class="{{ Request::is('admin/account/new_customer*') ? 'active' : '' }}">
                        <a href="{{ route('admin.account.customer.create') }}">{{ __('New Customer') }}</a>
                    </li>
                </ul>
            </li>

            {{-- ===================< Account Ends >====================== --}}




            {{-- ===================< Inventory Start >====================== --}}
            <li class="nav-item dropdown {{ Request::is('admin/inventory*') ? 'open' : '' }}">
                <a class="dropdown-toggle" href="javascript:void(0);">
                    <span class="icon-holder">
                        <i class="ti-shopping-cart-full"></i>
                    </span>
                    <span class="title">{{ __('Inventories') }}</span>
                    <span class="arrow">
                        <i class="ti-angle-right"></i>
                    </span>
                </a>
                <ul class="dropdown-menu">
                    <li class="{{ Request::is('admin/inventory/inventory_list*') ? 'active' : '' }}">
                        <a href="{{ route('admin.inventory.inventory.index') }}">{{ __('Inventory List') }}</a>
                    </li>
                    <li class="{{ Request::is('admin/inventory/category*') ? 'active' : '' }}">
                        <a href="{{ route('admin.inventory.category.index') }}">{{ __('Manage Categories') }}</a>
                    </li>
                </ul>
            </li>

            {{-- ===================< Inventory Ends >====================== --}}




            {{-- ===================< Invoice Start >====================== --}}
            <li class="nav-item dropdown {{ Request::is('admin/quotation*') ? 'open' : '' }}">
                <a class="dropdown-toggle" href="javascript:void(0);">
                    <span class="icon-holder">
                        <i class="ti-notepad"></i>
                    </span>
                    <span class="title">{{ __('Quotations') }}</span>
                    <span class="arrow">
                        <i class="ti-angle-right"></i>
                    </span>
                </a>
                <ul class="dropdown-menu">
                    <li class="{{ Request::is('admin/quotation/quotation_list*') ? 'active' : '' }}">
                        <a href="{{ route('admin.invoice.invoice.index') }}">{{ __('Quotation List') }}</a>
                    </li>
                    <li class="{{ Request::is('admin/quotation/new_quotation*') ? 'active' : '' }}">
                        <a href="{{ route('admin.invoice.invoice.create') }}">{{ __('New Quotation') }}</a>
                    </li>
                </ul>
            </li>

            {{-- ===================< Invoice Ends >====================== --}}



            {{-- ===================< Settings Start >====================== --}}
            <li class="nav-item dropdown {{ Request::is('admin/setting*') ? 'open' : '' }}">
                <a class="dropdown-toggle" href="javascript:void(0);">
                    <span class="icon-holder">
                        <i class="ti-settings"></i>
                    </span>
                    <span class="title">{{ __('Settings') }}</span>
                    <span class="arrow">
                        <i class="ti-angle-right"></i>
                    </span>
                </a>
                <ul class="dropdown-menu">
                    <li class="nav-item dropdown sub-dropdown {{ Request::is('admin/setting/general*') ? 'open' : '' }}">
                        <a href="javascript:void(0);">
                            <span>{{ __('General Settings') }}</span>
                            <span class="arrow">
                                <i class="ti-angle-right"></i>
                            </span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="{{ Request::is('admin/setting/general/company*') ? 'active' : '' }}">
                                <a href="{{ route('admin.setting.general.company.index') }}">{{ __('Company') }}</a>
                            </li>
                            <li class="{{ Request::is('admin/setting/general/account*') ? 'active' : '' }}">
                                <a href="{{ route('admin.setting.general.account.index') }}">{{ __('Account') }}</a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item dropdown sub-dropdown {{ Request::is('admin/setting/inventory*') ? 'open' : '' }}">
                        <a href="javascript:void(0);">
                            <span>{{ __('Inventory') }}</span>
                            <span class="arrow">
                                <i class="ti-angle-right"></i>
                            </span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="{{ Request::is('admin/setting/inventory/inventory*') ? 'active' : '' }}">
                                <a href="{{ route('admin.setting.inventory.inventory.index') }}">{{ __('Inventory') }}</a>
                            </li>
                            <li class="{{ Request::is('admin/setting/inventory/unit*') ? 'active' : '' }}">
                                <a href="{{ route('admin.setting.inventory.unit.index') }}">{{ __('Measure Unit') }}</a>
                            </li>
                            {{-- <li class="{{ Request::is('admin/setting/inventory/shipping*') ? 'active' : '' }}">
                                <a href="{{ route('admin.setting.inventory.shipping.index') }}">{{ __('Shipping') }}</a>
                            </li> --}}
                        </ul>
                    </li>

                    <li class="nav-item dropdown sub-dropdown {{ Request::is('admin/setting/billing*') ? 'open' : '' }}">
                        <a href="javascript:void(0);">
                            <span>{{ __('Billing') }}</span>
                            <span class="arrow">
                                <i class="ti-angle-right"></i>
                            </span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="{{ Request::is('admin/setting/billing/billing*') ? 'active' : '' }}">
                                <a href="{{ route('admin.setting.billing.billing.index') }}">{{ __('Billing') }}</a>
                            </li>
                            <li class="{{ Request::is('admin/setting/billing/tax*') ? 'active' : '' }}">
                                <a href="{{ route('admin.setting.billing.tax.index') }}">{{ __('Tax') }}</a>
                            </li>
                            {{-- <li class="{{ Request::is('admin/setting/billing/email*') ? 'active' : '' }}">
                                <a href="{{ route('admin.setting.billing.email.index') }}">{{ __('Email') }}</a>
                            </li> --}}
                        </ul>
                    </li>
                </ul>
            </li>
            {{-- ===================< Settings Ends >====================== --}}
        </ul>
    </div>
</div>
<!-- Side Nav END -->
