<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no">
    {{-- CSRF Token--}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{--  Page Title  --}}
    <title> {{ __('ADMIN') }} | @yield('page_title') </title>


    @include('layouts.admin.partials.stylesheet')
</head>

<body>
    @php
        $position = (app()->getLocale() == 'hebrew') ? 'rtl':'rtl';
        // $position = (app()->getLocale() == 'hebrew') ? 'rtl':'ltr';
    @endphp
    <div class="app {{ $position }} header-dark side-nav-dark">
        <div class="layout">
            {{-- Side Nav --}}
            @include('layouts.admin.partials.sidenav')

            {{-- Page Container START --}}
            <div class="page-container">
                {{-- Top Nav --}}
                @include('layouts.admin.partials.topnav')

                {{-- Main Content --}}
                <div class="main-content">
                    @yield('main_content')
                </div>

                {{-- Footer --}}
                 @include('layouts.admin.partials.footer')
            </div>
            {{-- Page Container END --}}
        </div>
    </div>
    @include('sweetalert::alert')

    @include('layouts.admin.partials.scripts')

</body>
</html>
