@extends('layouts.admin.app')

@section('page_title', 'Admin Profile')

@section('css_links')
    {{--  External CSS  --}}

@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS Here */
        .form-control {
            padding: 0.700rem 0.75rem;
        }
        .admin-profile-picture{
            max-width: 125px;
        }
        .form-control:disabled, .form-control[readonly] {
            background-color: #ffffff;
            opacity: 1;
            border-color: #fbfbfb;
        }
    </style>
@endsection

@section('main_content')
<div class="container-fluid">
    <div class="page-title">
        <h4>{{ $user->name }}</h4>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-4">
                    <div class="widget-profile-1 card">
                        <div class="profile border bottom">
                            @if($user->avatar != null)
                                <img class="mrg-top-30 admin-profile-picture" src="{{ $user->avatar }}" alt="">
                            @else
                                <img class="mrg-top-30 admin-profile-picture" src="{{ asset('assets/images/others/img-10.jpg') }}" alt="">
                            @endif
                            
                        <h4 class="mrg-top-20 no-mrg-btm text-semibold">{{ $user->name }}</h4>
                            @php
                                $date = new DateTime($user->created_at);
                            @endphp
                            <p>{{ $date->format('d M Y') }}</p>
                        </div>
                    </div>
                    <div class="card">
                        <form action="{{ route('admin.setting.general.account.password', ['id' => encrypt($user->id)]) }}" method="post">
                        @csrf
                            <div class="card-heading border bottom">
                                <h4 class="card-title">Authentication</h4>
                            </div>
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="mrg-bottom-10 text-dark"><b>Old Password</b></label>
                                        <input type="password" name="old_password" class="form-control @error('old_password') is-invalid @enderror" placeholder="Old Password" autocomplete="off" required>

                                        @error('old_password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <hr>

                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="mrg-bottom-10 text-dark"><b>New Password</b></label>
                                        <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" placeholder="New Password" autocomplete="off" required>

                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="row mrg-top-15">
                                    <div class="col-md-12">
                                        <label class="mrg-bottom-10 text-dark"><b>Confirm New Password</b></label>
                                        <input type="password" name="password_confirmation" class="form-control @error('password_confirmation') is-invalid @enderror" placeholder="Confirm New Password" autocomplete="off" required>

                                        @error('password_confirmation')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer border top">
                                <div class="text-right">
                                    <button type="submit" class="btn btn-dark btn-sm">Update Password</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                {{-- update profile form start --}}
                <div class="col-md-8">
                    <form action="{{ route('admin.setting.general.account.update', ['id' => encrypt($user->id)]) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="card">
                            <div class="card-heading border bottom">
                                <h4 class="card-title">{{ __('Admin Account Information') }}</h4>
                            </div>
                            <div class="card-block">

                                {{-- avatar start --}}
                                <div class="row">
                                    <div class="col-md-3">
                                        <p class="mrg-top-10 text-dark"> <b>Avatar</b></p>
                                    </div>
                                    <div class="col-md-6">
                                        <div>
                                            <label for="img-upload" class="pointer text-center">
                                                @if($user->avatar != null)
                                                <img id="img-preview" src="{{ $user->avatar }}"  width="117" alt="">
                                                @else
                                                <img id="img-preview" src="{{ asset('assets/images/others/img-10.jpg') }}"  width="117" alt="">
                                                @endif

                                                <span class="btn btn-default display-block no-mrg-btm">{{ __('Update Profile Picture') }}</span>
                                                <input class="d-none @error('avatar') is-invalid @enderror removeDis" disabled type="file" name="avatar" accept=".png, .jpg, .jpeg" multiple id="img-upload">
                                            </label>
                                            @error('avatar')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                            <br>
                                            <small class="text-info">{{ __('Image Maximum Size 2MB.') }} </small>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                {{-- avatar close --}}


                                {{-- name start --}}
                                <div class="row">
                                    <div class="col-md-3">
                                        <p class="mrg-top-10 text-dark"> <b>{{ __('Admin Name') }}</b></p>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror removeDis" disabled value="{{ $user->name }}" placeholder="Mr. Jhon Doe" autocomplete="off" required>

                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <hr>
                                {{-- name close --}}

                                {{-- Email start --}}
                                <div class="row">
                                    <div class="col-md-3">
                                    <p class="mrg-top-10 text-dark"> <b>{{ __('Email Address') }}</b></p>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" name="email" class="form-control @error('email') is-invalid @enderror removeDis" disabled value="{{ $user->email }}" placeholder="example.com" autocomplete="off" required>

                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <hr>
                                {{-- Email close --}}


                                {{-- address start --}}
                                {{-- <div class="row">
                                    <div class="col-md-3">
                                        <p class="mrg-top-10 text-dark"> <b>{{ __('Address') }}</b></p>
                                    </div>
                                    <div class="col-md-6">
                                    <textarea name="address" class="form-control @error('address') is-invalid @enderror" rows="3" placeholder="{{ __('Address') }}" autocomplete="off" required>address_here</textarea>

                                        @error('address')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <hr> --}}
                                {{-- address close --}}
                            </div>
                            <div class="card-footer border top">
                                <div class="text-right">
                                    <a href="javascript:void(0);" class="btn btn-dark btn-sm" id="removeDisabled">
                                        {{ __('Edit') }}
                                    </a>
                                    <a href="javascript:void(0);" class="btn btn-default btn-sm hiddenButton d-none" id="addDisabled">
                                        {{ __('Cancel') }}
                                    </a>
                                    <button type="submit" class="btn btn-dark btn-sm hiddenButton d-none">
                                        {{ __('Update') }}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                {{-- update profile form close --}}
            </div>
        </div>
    </div>
</div>
@endsection


@section('script_links')
    {{--  External Javascript Links --}}

@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        $("#removeDisabled").click(function(){
            $('.removeDis').removeAttr('disabled', 'disabled');
            $('.removeDis').removeClass('disabled');
            $('#removeDisabled').addClass('d-none');
            $('.hiddenButton').removeClass('d-none');        
        });
        
        $("#addDisabled").click(function(){
            $('.removeDis').attr('disabled', 'disabled');
            $('.removeDis').addClass('disabled');
            $('#removeDisabled').removeClass('d-none');
            $('.hiddenButton').addClass('d-none');        
        });
    </script>



    <script>
        // Custom Script Here
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    // $('#img-preview').css('background-image', 'url('+e.target.result +')');
                    $('#img-preview').attr('src', e.target.result);
                    $('#img-preview').hide();
                    $('#img-preview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#img-upload").change(function() {
            readURL(this);
        });
    </script>
@endsection
