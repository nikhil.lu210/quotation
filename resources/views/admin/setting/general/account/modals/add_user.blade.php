{{-- Upload File Modal --}}
<div class="modal fade" id="add_user">
    <form action="{{ route('admin.setting.general.account.store') }}" method="post" id="add_user_from">
        @csrf
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="float-left"><b><span id="social_name">{{ __('Add New Admin') }}</span></b></h4>
                    <button class="btn btn-default btn-icon btn-rounded p-l-8 p-r-7 p-t-6 p-b-4" data-dismiss="modal">
                        <i class="ti-close"></i>
                    </button>
                </div>
                <div class="modal-body media-details-modal">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label>{{ __('Admin Full Name') }} <span class="required">*</span></label>
                                <input autocomplete="off" type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}" placeholder="{{ __('Admin Full Name') }}" required>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label>{{ __('Email Address') }} <span class="required">*</span></label>
                                <input autocomplete="off" type="text" name="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" placeholder="{{ __('Email Address') }}" required>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label>{{ __('Password') }} <span class="required">*</span></label>
                                <input autocomplete="off" type="text" name="password" class="form-control @error('password') is-invalid @enderror" value="{{ old('password') }}" placeholder="{{ __('Password') }}" minlength="8" required>

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label>{{ __('Confirm Password') }} <span class="required">*</span></label>
                                <input autocomplete="off" type="text" name="password_confirmation" class="form-control @error('password_confirmation') is-invalid @enderror" value="{{ old('password_confirmation') }}" placeholder="{{ __('Confirm Password') }}" minlength="8" required>

                                @error('password_confirmation')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer border top">
                    <ul class="list-unstyled list-inline pull-right">
                        <li class="list-inline-item">
                            <button type="submit" id="new_item_submit" class="btn btn-dark btn-sm text-bold">
                                <i class="ti-save"></i>
                                {{ __('Assign As Admin') }}
                            </button>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </form>
</div>
