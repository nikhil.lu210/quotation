@extends('layouts.admin.app')

@section('page_title', 'SETTINGS | Company')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/selectize/dist/css/selectize.default.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS Here */
        .form-control{
            padding: 0.700rem 0.75rem;
        }
        .selectize-input {
            padding: 0.640rem 0.75rem;
        }

        .responsive-image{
            border-radius: 5px;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
            height: 80px;
            width: 100%;
        }
        .card-block.task-file.p-0 {
            height: 80px;
        }
        .selectize-dropdown.single{
            z-index: 99999999;
        }
        .selectize-dropdown [data-selectable], .selectize-dropdown .optgroup-header {
            padding: 15px 8px !important;
        }
        a.deactive{
            cursor: pointer;
        }
        li.social-media-list{
            border: 0px !important;
            border-bottom: 1px solid #e6ecf5 !important;
            margin-bottom: 10px;
        }
        li.social-media-list:last-child{
            border: 0px !important;
            border-bottom: 0px solid #e6ecf5 !important;
            margin-bottom: 0px;
        }
        .form-control:disabled, .form-control[readonly] {
            background-color: #ffffff;
            opacity: 1;
            border-color: #fbfbfb;
        }
    </style>
@endsection

@section('main_content')
<div class="container-fluid">
    <div class="page-title">
        <h4>{{ __('Company Informations') }}</h4>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <form action="{{ route('admin.setting.general.company.update', ['company_id' => encrypt($company->id)]) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card">
                    <div class="card-block p-25">
                        <div class="card">
                            <div class="card-heading">
                                <h4 class="card-title float-right">{{ __('Company Informations') }}</h4>

                                {{-- <a href="#" class="btn btn-dark btn-sm float-left m-b-0">{{ __('Edit Info') }}</a> --}}
                            </div>
                            <hr class="m-t-0">
                            <div class="card-block p-25">
                                <div class="row justify-content-center m-b-20">
                                    <div class="col-lg-4 text-center">
                                        <label for="img-upload" class="pointer">
                                            <img id="img-preview" src="{{($company->logo != null) ? $company->logo : asset('assets/images/others/img-10.jpg') }}"  width="117" alt="">

                                            <span class="btn btn-default display-block no-mrg-btm">{{ __('Choose Logo') }}</span>
                                            <input class="d-none @error('logo') is-invalid @enderror removeDis" disabled type="file" name="logo" accept=".png, .jpg, .jpeg" id="img-upload" value="{{ old('logo') }}">
                                            <small class="text-info">The logo Size Must be <b class="text-warning">150*65 Pixel</b></small>
                                        </label>
                                        @error('logo')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{ __('Company Name') }} <span class="required">*</span></label>
                                            <input autocomplete="off" type="text" name="name" class="form-control @error('name') is-invalid @enderror removeDis" disabled value="{{ $company->name }}" placeholder="{{ __('Company Name') }}" required>

                                            @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{ __('Country') }}</label>
                                            <div class="mrg-top-0">
                                                <select class="@error('country') is-invalid @enderror" name="country" id="selectize-group" >
                                                    <option value="" disabled @if($company->country == null)selected @endif>{{ __('Select Country') }}</option>
                                                    @if($company->country != null)
                                                        <option value="{{ $company->country }}" selected>{{ $company->country }}</option>
                                                    @endif
                                                    @include('admin.setting.general.company.country')
                                                </select>
                                            </div>

                                            @error('country')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>{{ __('Company City') }}</label>
                                            <input autocomplete="off" type="text" name="city" class="form-control @error('city') is-invalid @enderror removeDis" disabled value="{{ $company->city }}" placeholder="{{ __('Company City') }}">

                                            @error('city')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>{{ __('Province/State') }}</label>
                                            <input autocomplete="off" type="text" name="state" class="form-control @error('state') is-invalid @enderror removeDis" disabled value="{{ $company->state }}" placeholder="{{ __('Province/State') }}">

                                            @error('state')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>{{ __('Postal Code') }}</label>
                                            <input autocomplete="off" type="text" name="postal_code" class="form-control @error('postal_code') is-invalid @enderror removeDis" disabled value="{{ $company->postal_code }}" placeholder="12345">

                                            @error('postal_code')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-group">
                                            <label>{{ __('Address Line 1') }}</label>
                                            <textarea name="address_1" class="form-control @error('address_1') is-invalid @enderror removeDis" disabled placeholder="{{ __('Address Line 1') }}" rows="3">{{ $company->address_1 }}</textarea>

                                            @error('address_1')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-group">
                                            <label>{{ __('Address Line 2') }}</label>
                                            <textarea name="address_2" class="form-control @error('address_2') is-invalid @enderror removeDis" disabled placeholder="{{ __('Address Line 2') }}" rows="3">{{ $company->address_2 }}</textarea>

                                            @error('address_2')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer border top">
                        <ul class="list-unstyled list-inline float-right">
                            <li class="list-inline-item">
                                <a href="javascript:void(0);" class="btn btn-dark btn-sm" id="removeDisabled">
                                    {{ __('Edit') }}
                                </a>
                                <a href="javascript:void(0);" class="btn btn-default btn-sm hiddenButton d-none" id="addDisabled">
                                    {{ __('Cancel') }}
                                </a>
                                <button type="submit" class="btn btn-dark btn-sm hiddenButton d-none">
                                    {{ __('Update') }}
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
            </form>
        </div>
    </div>

    {{-- @include('superadmin.system.modals.team.update_modal') --}}
</div>
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/selectize/dist/js/standalone/selectize.min.js') }}"></script>
    <script src="{{ asset('assets/js/forms/form-elements.js') }}"></script>

@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        $("#removeDisabled").click(function(){
            $('.removeDis').removeAttr('disabled', 'disabled');
            $('.removeDis').removeClass('disabled');
            $('#removeDisabled').addClass('d-none');
            $('.hiddenButton').removeClass('d-none');        
        });
        
        $("#addDisabled").click(function(){
            $('.removeDis').attr('disabled', 'disabled');
            $('.removeDis').addClass('disabled');
            $('#removeDisabled').removeClass('d-none');
            $('.hiddenButton').addClass('d-none');        
        });
    </script>




    <script>
        // Custom Script Here
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    // $('#img-preview').css('background-image', 'url('+e.target.result +')');
                    $('#img-preview').attr('src', e.target.result);
                    $('#img-preview').hide();
                    $('#img-preview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#img-upload").change(function() {
            readURL(this);
        });
    </script>
@endsection
