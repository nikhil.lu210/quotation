@extends('layouts.admin.app')

@section('page_title', 'SETTINGS | Shipping')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/selectize/dist/css/selectize.default.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS Here */
        label.label{
            position: relative;
            top: -15px;
            font-size: 18px;
            font-weight: 500;
            color: #333;
        }
    </style>
@endsection

@section('main_content')
@php
    function getInventory($type, $name){
        $inventory = App\Models\InventorySettings::where('type', $type)->where('name', $name)->firstOrFail();
        return ($inventory != null) ? $inventory->status : false;
    }
@endphp
<div class="container-fluid">
    <div class="page-title">
        <h4>{{ __('Shipping Settings') }}</h4>
    </div>

    <div class="row justify-content-center">
        <div class="col-lg-8">
            <form action="#" method="post">
                @csrf
                <div class="card">
                    <div class="card-heading">
                        <h4 class="card-title float-right">{{ __('Shipping Settings') }}</h4>
                    </div>
                    <hr class="m-t-0">
                    <div class="card-block p-25">
                        <div class="row">
                            <div class="col-lg-12 text-right p-b-10">
                                {{ __('Keep track of which invoiced orders have shipped, as well as track if some orders have only been partially shipped. Enabling shipment tracking will add a new column to your invoice list to clearly display the shipping status of each order. You can enable this to try it out and safely disable it if you don\'t find it useful.')}}
                            </div>

                            <div class="col-lg-12 text-right p-b-30">
                                {{ __('Note: Shipment tracking does not affect inventory levels as invoiced goods are already considered sold and deducted from your available inventory quantities. Partially shipping an order will generate multiple packing lists (one for each shipment).')}}
                            </div>

                            <div class="col-lg-12 text-right">
                                <div class="form-group">
                                    <div class="toggle-checkbox checkbox-inline">
                                        <input type="checkbox" data-type='shipping' data-name="track_shipments" id="track_shipments" @if(getInventory('shipping', 'track_shipments'))checked @endif>
                                        <label for="track_shipments"></label>
                                    </div>
                                    <label class="label">{{ __('Track Shipments?') }}</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    {{-- @include('superadmin.system.modals.team.update_modal') --}}
</div>
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/selectize/dist/js/standalone/selectize.min.js') }}"></script>
    <script src="{{ asset('assets/js/forms/form-elements.js') }}"></script>

@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    // $('#img-preview').css('background-image', 'url('+e.target.result +')');
                    $('#img-preview').attr('src', e.target.result);
                    $('#img-preview').hide();
                    $('#img-preview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#img-upload").change(function() {
            readURL(this);
        });
    </script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script>
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        onOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });
</script>
<script>
    $(document).ready(function(){

        $("input").click( function(event){
            var type = $(this).attr('data-type');
            var name = $(this).attr('data-name');

            $.ajax({
                method: 'POST',
                url: "{{ route('admin.setting.inventory.change_status') }}" ,
                data: {
                    'type': type,
                    'name': name
                },
                datatype: "JSON",
                success: function(data) {
                    Toast.fire({
                        icon: data.status,
                        title: data.value
                    });
                },
                error: function() {
                    console.log("do not add to cart");
                }
            });


            // if( $(this).is(':checked') ) alert("checked");
        });
    });
</script>
@endsection
