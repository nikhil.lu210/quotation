@extends('layouts.admin.app')

@section('page_title', 'SETTINGS | Inventory')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/selectize/dist/css/selectize.default.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS Here */
        label.label{
            position: relative;
            top: -15px;
            font-size: 18px;
            font-weight: 500;
            color: #333;
        }
    </style>
@endsection
@php
    function getInventory($type, $name){
        $inventory = App\Models\InventorySettings::where('type', $type)->where('name', $name)->firstOrFail();
        return ($inventory != null) ? $inventory->status : false;
    }
@endphp
@section('main_content')
<div class="container-fluid">
    <div class="page-title">
        <h4>{{ __('Inventory Settings') }}</h4>
    </div>

    <div class="row justify-content-center">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-heading">
                    <h4 class="card-title float-right">{{ __('Inventory Settings') }}</h4>
                </div>
                <hr class="m-t-0">
                <div class="card-block p-25">
                    <div class="row">
                        <div class="col-lg-12 text-right">
                            <div class="form-group">
                                <div class="toggle-checkbox checkbox-inline">

                                    <input type="checkbox" data-type='inventory' data-name="inventory_functionality" id="inventory_functionality" @if(getInventory('inventory', 'inventory_functionality'))checked @endif>
                                    <label for="inventory_functionality"></label>
                                </div>
                                <label class="label">{{ __('Enable Inventory Functionality?') }}</label>
                            </div>
                        </div>

                        <div class="col-lg-12 text-right">
                            <div class="form-group">
                                <div class="toggle-checkbox checkbox-inline">
                                    <input type="checkbox" data-type='inventory' data-name="show_selling_price" id="show_selling_price"  @if(getInventory('inventory', 'show_selling_price'))checked @endif>
                                    <label for="show_selling_price"></label>
                                </div>
                                <label class="label">{{ __('Show Selling Price instead of Unit Cost?') }}</label>
                            </div>
                        </div>

                        <div class="col-lg-12 text-right">
                            <div class="form-group">
                                <div class="toggle-checkbox checkbox-inline">
                                    <input type="checkbox" data-type='inventory' data-name="display_custom_field" id="display_custom_field" @if(getInventory('inventory', 'display_custom_field'))checked @endif>
                                    <label for="display_custom_field"></label>
                                </div>
                                <label class="label">{{ __('Display Custom Fields on Inventory List?') }}</label>
                            </div>
                        </div>

                        <div class="col-lg-12 text-right">
                            <div class="form-group">
                                <div class="toggle-checkbox checkbox-inline">
                                    <input type="checkbox" data-type='inventory' data-name="item_available_location" id="item_available_location" @if(getInventory('inventory', 'item_available_location'))checked @endif>
                                    <label for="item_available_location"></label>
                                </div>
                                <label class="label">{{ __('Items always available to All Locations?') }}</label>
                            </div>
                        </div>

                        <div class="col-lg-12 text-right">
                            {{ __('Note: If you enable the \'Items always available to All Locations\' setting, whenever a new item is added into your inventory it will have a quantity of zero placed in all your current locations.')}}
                        </div>
                    </div>
                </div>
            </div>


            <div class="card">
                <div class="card-heading">
                    <h4 class="card-title float-right">{{ __('Item Photo Display Options') }}</h4>
                </div>
                <hr class="m-t-0">
                <div class="card-block p-25">
                    <div class="row">
                        <div class="col-lg-12 text-right p-b-30">
                            {{ __('Toggle where you\'d like to display inventory item photos.')}}
                        </div>

                        <div class="col-lg-12 text-right">
                            <div class="form-group">
                                <div class="toggle-checkbox checkbox-inline">
                                    <input type="checkbox" data-type='item_display' data-name="disply_photo_inventory_list" id="disply_photo_inventory_list" @if(getInventory('item_display', 'disply_photo_inventory_list'))checked @endif>
                                    <label for="disply_photo_inventory_list"></label>
                                </div>
                                <label class="label">{{ __('Display Photos on your Inventory List?') }}</label>
                            </div>
                        </div>

                        <div class="col-lg-12 text-right">
                            <div class="form-group">
                                <div class="toggle-checkbox checkbox-inline">
                                    <input type="checkbox" data-type='item_display' data-name="display_item_photo_on_po" id="display_item_photo_on_po" @if(getInventory('item_display', 'display_item_photo_on_po'))checked @endif>
                                    <label for="display_item_photo_on_po"></label>
                                </div>
                                <label class="label">{{ __('Display Item Photos on PO\'s?') }}</label>
                            </div>
                        </div>

                        <div class="col-lg-12 text-right">
                            <div class="form-group">
                                <div class="toggle-checkbox checkbox-inline">
                                    <input type="checkbox" data-type='item_display' data-name="display_item_photo_on_wers" id="display_item_photo_on_wers" @if(getInventory('item_display', 'display_item_photo_on_wers'))checked @endif>
                                    <label for="display_item_photo_on_wers"></label>
                                </div>
                                <label class="label">{{ __('Display Item Photos on wers?') }}</label>
                            </div>
                        </div>

                        <div class="col-lg-12 text-right">
                            <div class="form-group">
                                <div class="toggle-checkbox checkbox-inline">
                                    <input type="checkbox" data-type='item_display' data-name="display_item_photos_on_invoices" id="display_item_photos_on_invoices" @if(getInventory('item_display', 'display_item_photos_on_invoices'))checked @endif>
                                    <label for="display_item_photos_on_invoices"></label>
                                </div>
                                <label class="label">{{ __('Display Item Photos on Invoices?') }}</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- @include('superadmin.system.modals.team.update_modal') --}}
</div>
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/selectize/dist/js/standalone/selectize.min.js') }}"></script>
    {{-- <script src="{{ asset('assets/js/forms/form-elements.js') }}"></script> --}}

@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    // $('#img-preview').css('background-image', 'url('+e.target.result +')');
                    $('#img-preview').attr('src', e.target.result);
                    $('#img-preview').hide();
                    $('#img-preview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#img-upload").change(function() {
            readURL(this);
        });
    </script>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script>
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        });
    </script>
    <script>
        $(document).ready(function(){

            $("input").click( function(event){
                var type = $(this).attr('data-type');
                var name = $(this).attr('data-name');

                $.ajax({
                    method: 'POST',
                    url: "{{ route('admin.setting.inventory.change_status') }}" ,
                    data: {
                        'type': type,
                        'name': name
                    },
                    datatype: "JSON",
                    success: function(data) {
                        Toast.fire({
                            icon: data.status,
                            title: data.value
                        });
                    },
                    error: function() {
                        console.log("do not add to cart");
                    }
                });


                // if( $(this).is(':checked') ) alert("checked");
            });
        });
    </script>
@endsection
