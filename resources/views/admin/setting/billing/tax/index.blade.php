@extends('layouts.admin.app')

@section('page_title', 'SETIING | Tax')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/selectize/dist/css/selectize.default.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/summernote/dist/summernote.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS Here */
        .custom-img-size{
            max-height: 50px;
        }
        /* .form-control{
            padding: 0.700rem 0.75rem;
        } */
        .selectize-input {
            padding: 0.54rem 0.75rem;
        }

        .responsive-image{
            border-radius: 5px;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
            height: 80px;
            width: 100%;
        }
        .card-block.task-file.p-0 {
            height: 80px;
        }
        .selectize-dropdown.single{
            z-index: 99999999;
        }
        .selectize-dropdown [data-selectable], .selectize-dropdown .optgroup-header {
            padding: 15px 8px !important;
        }

        label.label{
            position: relative;
            top: -15px;
            font-size: 18px;
            font-weight: 500;
            color: #333;
        }
        .default{
            color: #333;
        }
    </style>
@endsection

@section('main_content')
<div class="container-fluid">
    <div class="page-title">
        <h4>{{ __('Tax Settings') }}</h4>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-heading">
                    <h4 class="card-title float-right">{{ __('Tax Settings') }}</h4>

                    <a href="javascript:void(0);" data-toggle="modal" data-target="#add_new_tax" class="btn btn-dark btn-sm float-left m-b-0">{{ __('Add New Tax') }}</a>
                </div>
                <hr class="m-t-0">
                <div class="card-block">
                    <div class="table-overflow">
                        <table id="dt-opt" class="table table-lg table-hover table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <th class="text-center">{{ __('Action') }}</th>
                                    <th class="text-center">{{ __('Status') }}</th>
                                    <th class="text-center">{{ __('Tax Name') }}</th>
                                    <th class="text-center">{{ __('Tax Registration') }}</th>
                                    <th class="text-center">{{ __('Tax Rate') }}</th>
                                    <th class="text-center">#</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($taxes as $sl => $data)
                                    <tr>
                                        <td class="text-center">
                                            <div class="m-t-15 dropdown">
                                                <a href="#" class="btn btn-icon btn-flat btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="ti-more"></i></a>
                                                <ul class="dropdown-menu">
                                                    @php
                                                        $id = encrypt($data->id);
                                                    @endphp
                                                    <li>
                                                        <a href="javascript:void(0);" data-toggle="modal" data-id="{{ $id }}" data-todo="{{ $data }}" data-target="#show_tax" >
                                                            <i class="ti-eye pdd-right-10 text-info"></i>
                                                            <span>{{ __('Details') }}</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ route('admin.setting.billing.tax.is_default', ['id' => $id]) }}">
                                                            <i class="ei-claims pdd-right-10 text-warning"></i>
                                                            <span>{{ __('Set as Default?') }}</span>
                                                        </a>
                                                    </li>
                                                    <li>

                                                        @if ($data->status == 1)
                                                            <a href="{{ route('admin.setting.billing.tax.status', ['id' => $id]) }}" class="confirmation">
                                                                <i class="ti-close pdd-right-10 text-danger"></i>
                                                                <span>{{ __('Deactivate') }}</span>
                                                            </a>
                                                        @else
                                                            <a href="{{ route('admin.setting.billing.tax.status', ['id' => $id]) }}" class="confirmation">
                                                                <i class="ti-check pdd-right-10 text-success"></i>
                                                                <span>{{ __('Activate') }}</span>
                                                            </a>
                                                        @endif
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="m-t-15">
                                                <span class="text-dark">
                                                    @if ($data->status == 1)
                                                        <i class="ti-check pdd-right-10 text-success"></i>
                                                    @else
                                                        <b><i class="ti-close pdd-right-10 text-danger"></i></b>
                                                    @endif
                                                </span>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="m-t-15">
                                                <span class="text-dark">
                                                    <b>{{ $data->name }} @if($data->is_default == 1) <sup class="badge badge-info">Default</sup> @endif</b>
                                                </span>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="m-t-15">
                                                <span class="text-dark">
                                                    <b>{{ $data->registration }}</b>
                                                </span>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="m-t-15">
                                                <span class="text-dark">
                                                    <b>{{ $data->rate }}</b>
                                                </span>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="m-t-15">
                                                <span class="text-dark">
                                                    <b>{{ $sl+1 }}</b>
                                                </span>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('admin.setting.billing.modals.add_tax')
</div>
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>

    <script src="{{ asset('assets/vendors/selectize/dist/js/standalone/selectize.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('assets/js/forms/form-elements.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
        $('.selectize-group').selectize({
            sortField: 'text'
        });

        $('#show_tax').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var id = button.data('id');
            var data = button.data('todo');
            
            var modal = $(this);

            modal.find('#show_tax_id').val(id);
            modal.find('#show_name').val(data.name);
            modal.find('#show_registration').val(data.registration);
            modal.find('#show_rate').val(data.rate);
            if(data.is_default == 1) modal.find('#show_is_default').attr('checked', true);
            modal.find('#show_tax_name option[value=' + data.name + ']').attr('selected', 'selected');
            // modal.find('.modal-body #select2-website_hosting_id-container').attr('title', name).html(name);
        });
    </script>
@endsection
