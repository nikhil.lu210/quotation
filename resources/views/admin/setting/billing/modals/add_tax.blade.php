{{-- Upload File Modal --}}
<div class="modal fade" id="add_new_tax">
    <form action="{{ route('admin.setting.billing.tax.store') }}" method="post" id="add_new_tax_from">
        @csrf
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="float-left"><b><span id="social_name">{{ __('Add New Tax') }}</span></b></h4>
                    <button class="btn btn-default btn-icon btn-rounded p-l-8 p-r-7 p-t-6 p-b-4" data-dismiss="modal">
                        <i class="ti-close"></i>
                    </button>
                </div>
                <div class="modal-body media-details-modal">

                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <small>{{ __('Your tax information is used for all billing related functions. Tax percentages can be adjusted on a per line-item basis when needed. Changing the tax percentages below will only affect newly created documents. You can override these tax percentages on a per-account basis as well for customers and prospects that require different tax rates.') }}</small>
                        </div>
                        
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label>{{ __('Tax Name') }} <span class="required">*</span></label>
                                <input autocomplete="off" type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}" placeholder="{{ __('Tax Name') }}" required>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label>{{ __('Tax Registration') }}</label>
                                <input autocomplete="off" type="text" name="registration" class="form-control @error('registration') is-invalid @enderror" value="{{ old('registration') }}" placeholder="{{ __('Tax Registration') }}">

                                @error('registration')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label>{{ __('Tax Rate') }} <span class="required">*</span></label>
                                <input autocomplete="off" type="number" name="rate" class="form-control @error('rate') is-invalid @enderror" value="{{ old('rate') }}" placeholder="{{ __('Tax Rate') }}" required min="0" max="100">
                                <small>{{ __('Must be in Percentage (%)') }}</small>

                                @error('rate')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12">
                            <ul class="list-unstyled list-inline float-right">
                                <li class="list-inline-item">
                                    <div class="checkbox checkbox-primary font-size-12">
                                        <input id="is_default" name="is_default" type="checkbox">
                                        <label for="is_default" class="m-b-0 text-bold text-info">{{__('Set as Default?')}}</label>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="modal-footer border top">
                    <ul class="list-unstyled list-inline pull-right">
                        <li class="list-inline-item">
                            <button type="submit" id="new_item_submit" class="btn btn-dark btn-sm text-bold">
                                <i class="ti-save"></i>
                                {{ __('Add New Tax') }}
                            </button>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </form>
</div>

<div class="modal fade" id="show_tax">
    <form action="{{ route('admin.setting.billing.tax.update') }}" method="post" id="show_tax_from">
        @csrf
        <div class="modal-dialog modal-md" role="document">
            <input type="hidden" id="show_tax_id" name="id">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="float-left"><b><span id="social_name">{{ __('Update Tax') }}</span></b></h4>
                    <button class="btn btn-default btn-icon btn-rounded p-l-8 p-r-7 p-t-6 p-b-4" data-dismiss="modal">
                        <i class="ti-close"></i>
                    </button>
                </div>
                <div class="modal-body media-details-modal">

                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <small>{{ __('Your tax information is used for all billing related functions. Tax percentages can be adjusted on a per line-item basis when needed. Changing the tax percentages below will only affect newly created documents. You can override these tax percentages on a per-account basis as well for customers and prospects that require different tax rates.') }}</small>
                        </div>
                        
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label>{{ __('Tax Name') }} <span class="required">*</span></label>
                                <input autocomplete="off" id="show_name" type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}" placeholder="{{ __('Tax Name') }}" required>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label>{{ __('Tax Registration') }}</label>
                                <input autocomplete="off" id="show_registration" type="text" name="registration" class="form-control @error('registration') is-invalid @enderror" value="{{ old('registration') }}" placeholder="{{ __('Tax Registration') }}">

                                @error('registration')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label>{{ __('Tax Rate') }} <span class="required">*</span></label>
                                <input autocomplete="off" type="number" id="show_rate" name="rate" class="form-control @error('rate') is-invalid @enderror" value="{{ old('rate') }}" placeholder="{{ __('Tax Rate') }}" required min="0" max="100">
                                <small>{{ __('Must be in Percentage (%)') }}</small>

                                @error('rate')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12">
                            <ul class="list-unstyled list-inline float-right">
                                <li class="list-inline-item">
                                    <div class="checkbox checkbox-primary font-size-12">
                                        <input id="show_is_default" name="is_default" type="checkbox">
                                        <label for="show_is_default" class="m-b-0 text-bold text-info">{{__('Set as Default?')}}</label>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="modal-footer border top">
                    <ul class="list-unstyled list-inline pull-right">
                        <li class="list-inline-item">
                            <button type="submit" id="show_item_submit" class="btn btn-dark btn-sm text-bold">
                                <i class="ti-save"></i>
                                {{ __('Update Tax') }}
                            </button>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </form>
</div>