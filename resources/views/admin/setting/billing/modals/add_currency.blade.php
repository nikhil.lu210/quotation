{{-- Upload File Modal --}}
<div class="modal fade" id="add_new_currency">
    <form action="{{ route('admin.setting.billing.currency.store') }}" method="post" id="add_new_currency_from">
        @csrf
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="float-left"><b><span id="social_name">{{ __('Add New Currency') }}</span></b></h4>
                    <button class="btn btn-default btn-icon btn-rounded p-l-8 p-r-7 p-t-6 p-b-4" data-dismiss="modal">
                        <i class="ti-close"></i>
                    </button>
                </div>
                <div class="modal-body media-details-modal">

                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label>{{ __('Currency Name') }} <span class="required">*</span></label>
                                <div class="mrg-top-0">
                                    <select class="selectize-group @error('name') is-invalid @enderror" name="name" required>
                                        <option value="" disabled selected>{{ __('Currency Name') }}</option>
                                        @include('admin.setting.billing.billing.currency')
                                    </select>
                                </div>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label>{{ __('Currency Sign') }} <span class="required">*</span></label>
                                <input autocomplete="off" type="text" name="sign" class="form-control @error('sign') is-invalid @enderror" value="{{ old('sign') }}" placeholder="$" required maxlength="3">

                                @error('sign')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12">
                            <ul class="list-unstyled list-inline float-right">
                                <li class="list-inline-item">
                                    <div class="checkbox checkbox-primary font-size-12">
                                        <input id="is_default" name="is_default" type="checkbox">
                                        <label for="is_default" class="m-b-0 text-bold text-primary">{{__('Set as Default?')}}</label>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="modal-footer border top">
                    <ul class="list-unstyled list-inline pull-right">
                        <li class="list-inline-item">
                            <button type="submit" id="new_item_submit" class="btn btn-dark btn-sm text-bold">
                                <i class="ti-save"></i>
                                {{ __('Add New Currency') }}
                            </button>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </form>
</div>


<div class="modal fade" id="show_currency">
    <form action="{{ route('admin.setting.billing.currency.update') }}" method="post" id="show_currency_from">
        @csrf
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="float-left"><b><span id="social_name">{{ __('Show Currency') }}</span></b></h4>
                    <button class="btn btn-default btn-icon btn-rounded p-l-8 p-r-7 p-t-6 p-b-4" data-dismiss="modal">
                        <i class="ti-close"></i>
                    </button>
                </div>
                <div class="modal-body media-details-modal">

                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label>{{ __('Currency Name') }} <span class="required">*</span></label>
                                <div class="mrg-top-0">
                                    <select class="@error('name') is-invalid @enderror" id="show_currency_name" name="name" required>
                                        <option value="" disabled >{{ __('Currency Name') }}</option>
                                        @include('admin.setting.billing.billing.currency')
                                    </select>
                                </div>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label>{{ __('Currency Sign') }} <span class="required">*</span></label>
                                <input type="hidden" name="id" id="show_currency_id">
                                <input autocomplete="off" type="text" name="sign" id="show_currency_sign" class="form-control @error('sign') is-invalid @enderror" value="" placeholder="$" required maxlength="3">

                                @error('sign')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12">
                            <ul class="list-unstyled list-inline float-right">
                                <li class="list-inline-item">
                                    <div class="checkbox checkbox-primary font-size-12">
                                        <input id="show_is_default" name="is_default" type="checkbox">
                                        <label for="show_is_default" class="m-b-0 text-bold text-primary">{{__('Set as Default?')}}</label>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="modal-footer border top">
                    <ul class="list-unstyled list-inline pull-right">
                        <li class="list-inline-item">
                            <button type="submit" id="show_item_submit" class="btn btn-dark btn-sm text-bold">
                                <i class="ti-save"></i>
                                {{ __('Update Currency') }}
                            </button>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </form>
</div>
