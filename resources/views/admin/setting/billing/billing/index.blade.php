@extends('layouts.admin.app')

@section('page_title', 'SETIING | Measure unit')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/selectize/dist/css/selectize.default.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/summernote/dist/summernote.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS Here */
        .custom-img-size{
            max-height: 50px;
        }
        /* .form-control{
            padding: 0.700rem 0.75rem;
        } */
        .selectize-input {
            padding: 0.54rem 0.75rem;
        }

        .responsive-image{
            border-radius: 5px;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
            height: 80px;
            width: 100%;
        }
        .card-block.task-file.p-0 {
            height: 80px;
        }
        .selectize-dropdown.single{
            z-index: 99999999;
        }
        .selectize-dropdown [data-selectable], .selectize-dropdown .optgroup-header {
            padding: 15px 8px !important;
        }

        label.label{
            position: relative;
            top: -15px;
            font-size: 18px;
            font-weight: 500;
            color: #333;
        }
        .default{
            color: #333;
        }
    </style>
@endsection

@section('main_content')
<div class="container-fluid">
    <div class="page-title">
        <h4>{{ __('Billing Settings') }}</h4>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-heading">
                    <h4 class="card-title float-right">{{ __('Currency Settings') }}</h4>

                    <a href="javascript:void(0);" data-toggle="modal" data-target="#add_new_currency" class="btn btn-dark btn-sm float-left m-b-0">{{ __('Add New Currency') }}</a>
                </div>
                <hr class="m-t-0">
                <div class="card-block">
                    <div class="table-overflow">
                        <table id="dt-opt" class="table table-lg table-hover table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <th class="text-center">{{ __('Action') }}</th>
                                    <th class="text-center">{{ __('Status') }}</th>
                                    <th class="text-center">{{ __('Currency Sign') }}</th>
                                    <th class="text-center">{{ __('Currency Name') }}</th>
                                    <th class="text-center">#</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($currencies as $sl => $data)
                                    <tr>
                                        <td class="text-center">
                                            <div class="m-t-15 dropdown">
                                                <a href="#" class="btn btn-icon btn-flat btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="ti-more"></i></a>
                                                <ul class="dropdown-menu">
                                                    @php
                                                        $id = encrypt($data->id);
                                                    @endphp
                                                    <li>
                                                        <a href="javascript:void(0);" data-toggle="modal" data-id="{{ $id }}" data-todo="{{ $data   }}" data-target="#show_currency" >
                                                            <i class="ti-eye pdd-right-10 text-info"></i>
                                                            <span>{{ __('Details') }}</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ route('admin.setting.billing.currency.is_default', ['id' => $id]) }}">
                                                            <i class="ei-claims pdd-right-10 text-warning"></i>
                                                            <span>{{ __('Set as Default?') }}</span>
                                                        </a>
                                                    </li>
                                                    <li>

                                                        @if ($data->status == 1)
                                                            <a href="{{ route('admin.setting.billing.currency.status', ['id' => $id]) }}" class="confirmation">
                                                                <i class="ti-close pdd-right-10 text-danger"></i>
                                                                <span>{{ __('Deactivate') }}</span>
                                                            </a>
                                                        @else
                                                            <a href="{{ route('admin.setting.billing.currency.status', ['id' => $id]) }}" class="confirmation">
                                                                <i class="ti-check pdd-right-10 text-success"></i>
                                                                <span>{{ __('Activate') }}</span>
                                                            </a>
                                                        @endif
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="m-t-15">
                                                <span class="text-dark">
                                                    @if ($data->status == 1)
                                                        <i class="ti-check pdd-right-10 text-success"></i>
                                                    @else
                                                        <b><i class="ti-close pdd-right-10 text-danger"></i></b>
                                                    @endif
                                                </span>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="m-t-15">
                                                <span class="text-dark">
                                                    <b>{{ $data->sign }}</b>
                                                </span>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="m-t-15">
                                                <span class="text-dark">
                                                    <b>{{ $data->name }} @if($data->is_default == 1) <sup class="badge badge-info">Default</sup> @endif</b>
                                                </span>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="m-t-15">
                                                <span class="text-dark">
                                                    <b>{{ $sl+1 }}</b>
                                                </span>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            @php
                function getBilling($type, $name){
                    $data = App\Models\Billing::where('type', $type)->where('name', $name)->first();
                    if($data != null) return $data;
                    return null;
                }
            @endphp

            <form action="{{ route('admin.setting.billing.billing.update') }}" method="post">
                @csrf
                <div class="card">
                    <div class="card-block p-25">
                        <div class="card">
                            <div class="card-heading">
                                <h4 class="card-title float-right">{{ __('Billing Settings') }}</h4>
                            </div>
                            <hr class="m-t-0">
                            <div class="card-block p-25">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>{{ __('Invoice Name') }}</label>
                                            <input autocomplete="off" type="text" name="invoice_name" class="form-control @error('invoice_name') is-invalid @enderror" value="{{ getBilling('text', 'invoice_name')->text }}" placeholder="{{ __('Invoice Name') }}">

                                            @error('invoice_name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>{{ __('Estimate Name') }}</label>
                                            <input autocomplete="off" type="text" name="estimate_name" class="form-control @error('estimate_name') is-invalid @enderror" value="{{ getBilling('text', 'estimate_name')->text }}" placeholder="{{ __('Estimate Name') }}">

                                            @error('estimate_name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                        <label>{{ __('Estimates Expiry') }} <sup>{{ __('(days)') }}</sup></label>
                                            <input autocomplete="off" type="number" name="estimate_expiry" class="form-control @error('estimate_expiry') is-invalid @enderror" value="{{ getBilling('number', 'estimate_expiry')->number }}" placeholder="{{ __('Estimates Expiry') }}">

                                            @error('estimate_expiry')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-12">
                                        <div class="form-group">
                                            <label>{{ __('Default Payment Terms') }}</label>
                                            <textarea name="default_payment_term" class="form-control @error('default_payment_term') is-invalid @enderror" placeholder="{{ __('Default Payment Terms') }}" rows="3">{{ getBilling('textarea', 'default_payment_term')->text }}</textarea>

                                            @error('default_payment_term')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-6 text-right">
                                        <div class="form-group">
                                            <div class="toggle-checkbox checkbox-inline">
                                                <input type="checkbox" name="set_zeroed_out_invoice" id="set_zeroed_out_invoice" @if(getBilling('checkbox', 'set_zeroed_out_invoice')->status)checked @endif>
                                                <label for="set_zeroed_out_invoice"></label>
                                            </div>
                                            <label class="label">{{ __('Set Zeroed-out Invoices as Paid?') }}</label>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-6 text-right">
                                        <div class="form-group">
                                            <div class="toggle-checkbox checkbox-inline">
                                                <input type="checkbox" name="prevent_duplicate_line_item" id="prevent_duplicate_line_item" @if(getBilling('checkbox', 'prevent_duplicate_line_item')->status)checked @endif>
                                                <label for="prevent_duplicate_line_item"></label>
                                            </div>
                                            <label class="label">{{ __('Prevent Duplicate Line-Items?') }}</label>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-6 text-right">
                                        <div class="form-group">
                                            <div class="toggle-checkbox checkbox-inline">
                                                <input type="checkbox" name="show_sku" id="show_sku" @if(getBilling('checkbox', 'show_sku')->status)checked @endif>
                                                <label for="show_sku"></label>
                                            </div>
                                            <label class="label">{{ __('Show SKU\'s Instead of ID\'s?') }}</label>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-6 text-right">
                                        <div class="form-group">
                                            <div class="toggle-checkbox checkbox-inline">
                                                <input type="checkbox" name="add_item_description" id="add_item_description" @if(getBilling('checkbox', 'add_item_description')->status)checked @endif>
                                                <label for="add_item_description"></label>
                                            </div>
                                            <label class="label">{{ __('Add Item Description to Orders?') }}</label>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-12 text-right">
                                        <div class="form-group">
                                            <div class="toggle-checkbox checkbox-inline">
                                                <input type="checkbox" name="pdf_added_language" id="pdf_added_language" @if(getBilling('checkbox', 'pdf_added_language')->status)checked @endif>
                                                <label for="pdf_added_language"></label>
                                            </div>
                                            <label class="label">{{ __('PDF Added Language Support?') }}</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer border top">
                        <ul class="list-unstyled list-inline float-right">
                            <li class="list-inline-item">
                                <button type="submit" class="btn btn-dark btn-sm text-bold">
                                    <i class="ti-save"></i>
                                    {{ __('Save Settings') }}
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
            </form>
        </div>
    </div>

    @include('admin.setting.billing.modals.add_currency')
</div>
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>

    <script src="{{ asset('assets/vendors/selectize/dist/js/standalone/selectize.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('assets/js/forms/form-elements.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
        $('.selectize-group').selectize({
            sortField: 'text'
        });

        $('#show_currency').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var id = button.data('id');
            var data = button.data('todo');

            var modal = $(this);

            modal.find('#show_currency_id').val(id);
            modal.find('#show_currency_sign').val(data.sign);
            if(data.is_default == 1) modal.find('#show_is_default').attr('checked', true);
            modal.find('#show_currency_name option[value=' + data.name + ']').attr('selected', 'selected');
            // modal.find('.modal-body #select2-website_hosting_id-container').attr('title', name).html(name);
        });
    </script>
@endsection
