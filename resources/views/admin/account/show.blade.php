@extends('layouts.admin.app')

@section('page_title', 'ACCOUNTS | Customer Details')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/selectize/dist/css/selectize.default.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/summernote/dist/summernote.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS Here */
        .form-control{
            padding: 0.700rem 0.75rem;
        }
        .selectize-input {
            padding: 0.640rem 0.75rem;
        }

        .responsive-image{
            border-radius: 5px;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
            height: 80px;
            width: 100%;
        }
        .card-block.task-file.p-0 {
            height: 80px;
        }
        .selectize-dropdown.single{
            z-index: 99999999;
        }
        .selectize-dropdown [data-selectable], .selectize-dropdown .optgroup-header {
            padding: 15px 8px !important;
        }
        a.deactive{
            cursor: pointer;
        }
        li.social-media-list{
            border: 0px !important;
            border-bottom: 1px solid #e6ecf5 !important;
            margin-bottom: 10px;
        }
        li.social-media-list:last-child{
            border: 0px !important;
            border-bottom: 0px solid #e6ecf5 !important;
            margin-bottom: 0px;
        }
        .form-control:disabled, .form-control[readonly] {
            background-color: #ffffff;
            opacity: 1;
            border-color: #fbfbfb;
        }
    </style>
@endsection

@section('main_content')
<div class="container-fluid">
    <div class="page-title">
        <h4>{{ __('Update Customer Details') }}</h4>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <form action="{{ route('admin.account.customer.update', ['id' => encrypt($customer->id)]) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card">
                    <div class="card-block p-25">
                        <div class="card">
                            <div class="card-heading">
                                <h4 class="card-title float-right text-bold">{{ __('Business Contact Information') }}</h4>
                            </div>
                            <div class="card-block p-25">
                                <div class="row justify-content-center m-b-20">
                                    <div class="col-lg-4 text-center">
                                        <label for="img-upload" class="pointer">
                                            @if($customer->avatar != null)
                                                <img id="img-preview" src="{{ $customer->avatar }}"  width="117" alt="">
                                            @else
                                                <img id="img-preview" src="{{ asset('assets/images/others/img-10.jpg') }}"  width="117" alt="">
                                            @endif

                                            <span class="btn btn-default display-block no-mrg-btm">{{ __('Update Profile Photo') }}</span>
                                            <input class="d-none @error('avatar') is-invalid @enderror removeDis" disabled type="file" name="avatar" accept=".png, .jpg, .jpeg" id="img-upload" value="{{ old('avatar') }}">
                                        </label>
                                        @error('avatar')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>{{ __('Full Name') }} <span class="required">*</span></label>
                                            <input autocomplete="off" type="text" name="name" class="form-control @error('name') is-invalid @enderror removeDis" disabled value="{{ $customer->name }}" placeholder="{{ __('Full Name') }}" required>

                                            @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>{{ __('Email') }}</label>
                                            <input autocomplete="off" type="email" name="email" class="form-control @error('email') is-invalid @enderror removeDis" disabled value="{{ $customer->email }}" placeholder="yourmail@mail.com" required>

                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>{{ __('Mobile Number') }}</label>
                                            <input autocomplete="off" type="text" name="mobile" class="form-control @error('mobile') is-invalid @enderror removeDis" disabled value="{{ $customer->mobile_number }}" placeholder="+44 0800 689 3176">

                                            @error('mobile')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>{{ __('Office Phone') }}</label>
                                            <input autocomplete="off" type="text" name="office_phone" class="form-control @error('office_phone') is-invalid @enderror removeDis" disabled value="{{ $customer->office_phone }}" placeholder="+44 0800 689 3176">

                                            @error('office_phone')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>{{ __('Website') }}</label>
                                            <input autocomplete="off" type="url" name="website" class="form-control @error('website') is-invalid @enderror removeDis" disabled value="{{ $customer->website }}" placeholder="www.customerwebsite.com">

                                            @error('website')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>{{ __('Select Account Manager') }}</label>
                                            <div class="mrg-top-0">
                                                <select class="selectize-group @error('account_manager') is-invalid @enderror" name="account_manager" id="" >
                                                    <option value="" disabled>{{ __('Select Account Manager') }}</option>
                                                    @foreach($managers as $manager)
                                                        <option value="{{ $manager->id }}" @if ($manager->id == $customer->manager_id) selected @endif>{{ $manager->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            @error('account_manager')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="card">
                            <div class="card-heading">
                                <h4 class="card-title float-right text-bold">{{ __('Billing and Shipping Addresses') }}</h4>
                            </div>
                            <div class="card-block p-25">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>{{ __('Billing Address Line 1') }}</label>
                                                    <input autocomplete="off" type="text" name="billing_address_line_one" class="form-control @error('billing_address_line_one') is-invalid @enderror removeDis" disabled value="{{ $customer->billing_address_1 }}" placeholder="{{ __('Billing Address Line 1') }}">

                                                    @error('billing_address_line_one')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>{{ __('Billing Address Line 2') }}</label>
                                                    <input autocomplete="off" type="text" name="billing_address_line_two" class="form-control @error('billing_address_line_two') is-invalid @enderror removeDis" disabled value="{{ $customer->billing_address_2 }}" placeholder="{{ __('Billing Address Line 2') }}">

                                                    @error('billing_address_line_two')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>{{ __('City') }}</label>
                                                    <input autocomplete="off" type="text" name="billing_city" class="form-control @error('billing_city') is-invalid @enderror removeDis" disabled value="{{ $customer->billing_city }}" placeholder="{{ __('City') }}">

                                                    @error('billing_city')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>{{ __('State') }}</label>
                                                    <input autocomplete="off" type="text" name="billing_state" class="form-control @error('billing_state') is-invalid @enderror removeDis" disabled value="{{ $customer->state }}" placeholder="{{ __('State') }}">

                                                    @error('billing_state')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>{{ __('Postal Code / Zip Code') }}</label>
                                                    <input autocomplete="off" type="text" name="billing_post" class="form-control @error('billing_post') is-invalid @enderror removeDis" disabled value="{{ $customer->billing_postal }}" placeholder="{{ __('Postal Code / Zip Code') }}">

                                                    @error('billing_post')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>{{ __('Country') }}</label>
                                                    <input autocomplete="off" type="text" name="billing_country" class="form-control @error('billing_country') is-invalid @enderror removeDis" disabled value="{{ $customer->billing_country }}" placeholder="{{ __('Country') }}">

                                                    @error('billing_country')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    {{-- <div class="col-lg-6">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>{{ __('Shipping Address Line 1') }}</label>
                                                    <input autocomplete="off" type="text" name="shipping_address_line_one" class="form-control @error('shipping_address_line_one') is-invalid @enderror removeDis" disabled value="{{ $customer->shipping_address_1 }}" placeholder="{{ __('Shipping Address Line 1') }}">

                                                    @error('shipping_address_line_one')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>{{ __('Shipping Address Line 2') }}</label>
                                                    <input autocomplete="off" type="text" name="shipping_address_line_two" class="form-control @error('shipping_address_line_two') is-invalid @enderror removeDis" disabled value="{{ $customer->shipping_address_2 }}" placeholder="{{ __('Shipping Address Line 2') }}">

                                                    @error('shipping_address_line_two')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>{{ __('City') }}</label>
                                                    <input autocomplete="off" type="text" name="shipping_city" class="form-control @error('shipping_city') is-invalid @enderror removeDis" disabled value="{{ $customer->shipping_city }}" placeholder="{{ __('City') }}">

                                                    @error('shipping_city')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>{{ __('State') }}</label>
                                                    <input autocomplete="off" type="text" name="shipping_state" class="form-control @error('shipping_state') is-invalid @enderror removeDis" disabled value="{{ $customer->shipping_state }}" placeholder="{{ __('State') }}">

                                                    @error('shipping_state')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>{{ __('Postal Code / Zip Code') }}</label>
                                                    <input autocomplete="off" type="text" name="shipping_post" class="form-control @error('shipping_post') is-invalid @enderror removeDis" disabled value="{{ $customer->shipping_postal }}" placeholder="{{ __('Postal Code / Zip Code') }}">

                                                    @error('shipping_post')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>{{ __('Country') }}</label>
                                                    <input autocomplete="off" type="text" name="shipping_country" class="form-control @error('shipping_country') is-invalid @enderror removeDis" disabled value="{{ $customer->shipping_country }}" placeholder="{{ __('Country') }}">

                                                    @error('shipping_country')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer border top">
                        {{-- <ul class="list-unstyled list-inline pull-left">
                            <li class="list-inline-item">
                                <div class="checkbox checkbox-primary font-size-12">
                                    <input id="status" name="status" type="checkbox" checked required>
                                    <label for="status" class="m-b-0 text-bold text-primary">{{__('Activate This Customer?')}}</label>
                                </div>
                            </li>
                        </ul> --}}
                        <ul class="list-unstyled list-inline float-right">
                            <li class="list-inline-item">
                                <a href="javascript:void(0);" class="btn btn-dark btn-sm" id="removeDisabled">
                                    {{ __('Edit') }}
                                </a>
                                <a href="javascript:void(0);" class="btn btn-default btn-sm hiddenButton d-none" id="addDisabled">
                                    {{ __('Cancel') }}
                                </a>
                                <button type="submit" class="btn btn-dark btn-sm hiddenButton d-none">
                                    {{ __('Update') }}
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
            </form>
        </div>
    </div>

    {{-- @include('superadmin.system.modals.team.update_modal') --}}
</div>
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>

    <script src="{{ asset('assets/vendors/selectize/dist/js/standalone/selectize.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('assets/js/forms/form-elements.js') }}"></script>

@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        $("#removeDisabled").click(function(){
            $('.removeDis').removeAttr('disabled', 'disabled');
            $('.removeDis').removeClass('disabled');
            $('#removeDisabled').addClass('d-none');
            $('.hiddenButton').removeClass('d-none');        
        });
        
        $("#addDisabled").click(function(){
            $('.removeDis').attr('disabled', 'disabled');
            $('.removeDis').addClass('disabled');
            $('#removeDisabled').removeClass('d-none');
            $('.hiddenButton').addClass('d-none');        
        });
    </script>





    <script>
        // Custom Script Here
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    // $('#img-preview').css('background-image', 'url('+e.target.result +')');
                    $('#img-preview').attr('src', e.target.result);
                    $('#img-preview').hide();
                    $('#img-preview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#img-upload").change(function() {
            readURL(this);
        });


        $('.selectize-group').selectize({
            sortField: 'text'
        });
    </script>
@endsection
