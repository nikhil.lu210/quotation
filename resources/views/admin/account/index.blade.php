@extends('layouts.admin.app')

@section('page_title', 'ACCOUNTS | Customer')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS Here */
    </style>
@endsection

@section('main_content')
<div class="container-fluid">
    <div class="page-title">
        <h4>{{ __('Customer List') }}</h4>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-heading">
                    <h4 class="card-title float-right">{{ __('All Customers') }}</h4>

                    <a href="{{ route('admin.account.customer.create') }}" class="btn btn-dark btn-sm float-left m-b-0">{{ __('Add Customer') }}</a>
                </div>
                <hr class="m-t-0">
                <div class="card-block">
                    <div class="table-overflow">
                        <table id="dt-opt" class="table table-lg table-hover table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <th class="text-center">{{ __('Action') }}</th>
                                    <th class="text-center">{{ __('Status') }}</th>
                                    <th class="text-center">{{ __('Register Date') }}</th>
                                    <th class="text-center">{{ __('City') }}</th>
                                    <th class="text-center">{{ __('Email') }}</th>
                                    <th class="text-center">{{ __('Full Name') }}</th>
                                    <th class="text-center">#</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($customers as $item => $customer)
                                <tr>
                                    <td class="text-center">
                                        <div class="m-t-2 dropdown">
                                            <a href="#" class="btn btn-icon btn-flat btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="ti-more"></i></a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="{{ route('admin.account.customer.show', ['id' => encrypt($customer->id)]) }}">
                                                        <i class="ti-eye pdd-right-10 text-info"></i>
                                                        <span>{{ __('Details') }}</span>
                                                    </a>
                                                </li>
                                                @if ($customer->status == 1)
                                                    <li>
                                                        <a href="{{ route('admin.account.customer.status', ['id' => encrypt($customer->id)]) }}" class="confirmation">
                                                            <i class="ti-close pdd-right-10 text-danger"></i>
                                                            <span>{{ __('Deactivate') }}</span>
                                                        </a>
                                                    </li>
                                                @else
                                                    <li>
                                                        <a href="{{ route('admin.account.customer.status', ['id' => encrypt($customer->id)]) }}" class="confirmation">
                                                            <i class="ti-check pdd-right-10 text-success"></i>
                                                            <span>{{ __('Activate') }}</span>
                                                        </a>
                                                    </li>
                                                @endif
                                            </ul>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="m-t-2">
                                            @if ($customer->status == 1)
                                                <i class="ti-check pdd-right-10 text-success"></i>
                                            @else
                                                <i class="ti-close pdd-right-10 text-danger"></i>
                                            @endif
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="m-t-2">
                                            @php
                                                $date = new DateTime($customer->created_at);
                                            @endphp
                                            <span class="text-dark">
                                                <b>{{ $date->format('d M Y') }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="m-t-2">
                                            <span class="text-dark">
                                                <b>{{ $customer->billing_city }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="m-t-2">
                                            <span class="text-dark">
                                                <b>{{ $customer->email }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="m-t-2">
                                            <span class="text-dark">
                                                <a href="{{ route('admin.account.customer.show', ['lang' => app()->getLocale(), 'id' => encrypt($customer->id)]) }}"><b>{{ $customer->name }}</b></a>
                                            </span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="m-t-2">
                                            <span class="text-dark">
                                                <b>{{ $item+1}}</b>
                                            </span>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here

    </script>
@endsection
