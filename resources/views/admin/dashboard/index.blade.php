@extends('layouts.admin.app')

@section('page_title', 'DASHBOARD')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS Here */
        .clickable-row{
            cursor: pointer;
        }
        .table > tbody > tr > td {
            vertical-align: middle !important;
        }
    </style>
@endsection

@section('main_content')
<div class="container-fluid">
    <div class="page-title">
        <h4>{{ __('All My Quotation') }}</h4>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-block">
                    <div class="table-overflow">
                        <table id="dt-opt" class="table table-bordered table-lg table-hover table-responsive">
                            <thead>
                                <tr>
                                    <th class="text-center">{{ __('Action') }}</th>
                                    <th class="text-center">{{ __('Issued Date') }}</th>
                                    <th class="text-center">{{ __('Total Amount') }}</th>
                                    <th class="text-center">{{ __('Customer') }}</th>
                                    <th class="text-center">{{ __('Quotation') }}</th>
                                    <th class="text-center">{{ __('#') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($invoices as $sl => $data)
                                    <tr >
                                        <td class="text-center">
                                            <div class="m-t-15 dropdown">
                                                <a href="#" class="btn btn-icon btn-flat btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="ti-more"></i></a>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <a href="{{ route('admin.invoice.invoice.edit', ['id' => encrypt($data->id)]) }}">
                                                            <i class="ti-pencil pdd-right-10 text-info"></i>
                                                            <span>{{ __('Edit') }}</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ route('admin.invoice.invoice.show', ['id' => encrypt($data->id)]) }}">
                                                            <i class="ti-eye pdd-right-10 text-info"></i>
                                                            <span>{{ __('Details') }}</span>
                                                        </a>
                                                    </li>
                                                    {{-- <li>
                                                        <a href="{{ route('admin.inventory.inventory.delete', ['id' => encrypt($inventory->id)]) }}" class="confirmation">
                                                            <i class="ti-close pdd-right-10 text-danger"></i>
                                                            <span>{{ __('Delete') }}</span>
                                                        </a>
                                                    </li> --}}
                                                </ul>
                                            </div>
                                        </td>
                                        {{-- <td class="text-center clickable-row"  data-href="{{ route('admin.invoice.invoice.show', ['id' => encrypt($data->id)]) }}">
                                            @if($data->invoice_status == 1)
                                                <div class="relative ">
                                                    <span class="status online"> </span>
                                                    <span class="pdd-left-20 text-success"><b>Confirmed</b></span>
                                                </div>
                                            @elseif($data->invoice_status == 0)
                                                <div class="relative ">
                                                    <span class="status away"> </span>
                                                    <span class="pdd-left-20 text-warning"><b>Pending</b></span>
                                                </div>
                                            @elseif($data->invoice_status == -1)
                                                <div class="relative ">
                                                    <span class="status no-disturb"> </span>
                                                    <span class="pdd-left-20 text-danger"><b>Rejected</b></span>
                                                </div>
                                            @endif
                                        </td> --}}
                                        <td class="text-center clickable-row"  data-href="{{ route('admin.invoice.invoice.show', ['id' => encrypt($data->id)]) }}">
                                            <div class="">
                                                @php
                                                    $date = new DateTime($data->created_at);
                                                @endphp
                                               <span>{{ $date->format('d M Y') }}</span>
                                            </div>
                                        </td>
                                        <td class="text-center clickable-row"  data-href="{{ route('admin.invoice.invoice.show', ['id' => encrypt($data->id)]) }}">
                                            <div class="">
                                                @php
                                                    $total_payable = $data->total_dis_price - (($data->total_dis_price*$data->discount)/100);
                                                    $total_payable += (($total_payable*$data->tax)/100);
                                                @endphp
                                                <span>
                                                <b>{{ $data->currency->sign }}{{ round($total_payable, 2) }}</b>
                                                </span>
                                            </div>
                                        </td>
                                        <td class="text-center clickable-row"  data-href="{{ route('admin.invoice.invoice.show', ['id' => encrypt($data->id)]) }}">
                                            <div class="">
                                                <span>
                                                    <b>{{ $data->customer->name }}</b>
                                                </span>
                                            </div>
                                        </td>
                                        <td class="text-center clickable-row"  data-href="{{ route('admin.invoice.invoice.show', ['id' => encrypt($data->id)]) }}">
                                            <div class="">
                                                <span>
                                                    <b>{{ $data->inv_number }}</b>
                                                </span>
                                            </div>
                                        </td>
                                        <td class="text-center clickable-row"  data-href="{{ route('admin.invoice.invoice.show', ['id' => encrypt($data->id)]) }}">
                                            <div class="">
                                                <span>
                                                    <b>{{ $sl+1 }}</b>
                                                </span>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
        $(document).ready(function($) {
            $(".clickable-row").click(function() {
                window.location = $(this).data("href");
            });
        });
    </script>
@endsection
