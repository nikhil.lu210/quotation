
<script>
    function changeCurrency(that){
        var str = $(that).val();
        var sign = str.split('-');
        $('.selected_currency').html(sign[1]);
    }

    function checkCurrency(that){
        var str = $(that).val();
        var sign = str.split('-');
        return sign[1];
    }
</script>

<script>
    function changedItem(that){
        that = $(that);
        var id = that.val();

        $.ajax({
            method: 'GET',
            url: "{{ url('admin/quotation/get_item') }}"+'/'+id ,
            datatype: "JSON",
            success: function(data) {
                changes(data);
            },
            error: function() {
                console.log("something wrong!!!");
            }
        });

    }

    function changes(data){
        if(data.global_quantity != null){
            $("#item_quantity").attr('max',data.global_quantity);
            $("#max_quantity").html(data.global_quantity);
        }

        if(data.inventory_type == 'quantity_inventory_item'){
            $('#original_price').val(data.purchase_price_per_unit);
        } else{
            $('#original_price').val(data.purchase_price);
        }

        if(data.default_selling_price != null){
            $('#item_custom_price').val(data.default_selling_price);
        }
        $('#new_item_name').val(data.name);
        $('#item_image').val(data.avatar);
        $('#item_description').val(data.description);
    }


    $("#add_item_from").submit(function(event){
        event.preventDefault(); //event prevent

        //get value from input field
        var item_id = $('#new_item_id').val();
        var item_name = $('#new_item_name').val();
        var quantity = $('#item_quantity').val();
        var original_price = $('#original_price').val();
        var custom_price = $('#item_custom_price').val();
        // var tax = $('#new_item_tax').val();
        var discount = $('#new_item_discount').val();
        var image = $('#item_image').val();
        var item_description = $('#item_description').val();

        //set null value in this fields
        $('#new_item_id').val(null);
        $('#item_name').val(null);
        $('#item_quantity').val(null);
        $('#original_price').val(null);
        $('#item_custom_price').val(null);
        // $('#new_item_tax').val(0);
        $('#new_item_discount').val(0);
        $('#item_image').val(null);
        $('#item_description').val(null);

        //close modal
        $("#add_item").modal("hide");

        // set date in view field and hidden input field
        setItem(item_id, item_name, quantity, original_price, custom_price, discount, image, item_description);
    });



    /*
    / unique id generator method
    */
    function generateID() {
        // Math.random should be unique because of its seeding algorithm.
        // Convert it to base 36 (numbers + letters), and grab the first 9 characters
        // after the decimal.
        return '_' + Math.random().toString(36).substr(2, 9);
    };

    function setItem(item_id, item_name, quantity, original_price, custom_price, discount, image, item_description){

        // call method for taking new unique id
        var id = generateID();

        var dis_price = (custom_price*quantity) - (custom_price*quantity)*(discount/100);
        // var price = dis_price + (dis_price*(tax/100));
        var price = (custom_price*quantity);
        var dis_str = '';
        var sign = checkCurrency('#selected_currency');
        if(discount != 0) dis_str = '<sup class="text-bold text-danger"><del>(<span class="selected_currency">'+sign+'</span>'+price+')</del></sup>';

        var str = '<tr class="'+id+'"><td><div class="m-6 text-center"><a href="javascript:void(0);" onclick="deleteItem(this);" class="m-0 text-danger" co><i class="ti-close"></i></a></div></td><td><div class="m-6 text-center"><span class="text-dark"><b><span class="selected_currency">'+sign+'</span>'+dis_price+'</b>'+dis_str+'</span></div></td><td><div class="m-6 text-center"><b class="text-dark"><b><span class="selected_currency">'+sign+'</span>'+price+'</b></span></div></td><td><div class="m-6 text-center"><span class="text-dark"><b>'+ quantity +'</b></span></div></td><td><div class="m-6 text-center"><span class="text-dark"><b>'+item_description.substring(0, 10)+'...</b></span></div></td><td><div class="m-6 text-center"><span class="text-dark"><b>'+ item_name +'</b><input type="hidden" class="hidden_id" name="items['+id+'][item_id]" value="'+item_id+'"><input type="hidden" class="hidden_quantity" name="items['+id+'][quantity]" value="'+quantity+'"><input type="hidden" class="hidden_total_price" name="items['+id+'][total_price]" value="'+price+'"><input type="hidden" class="hidden_custom_price" name="items['+id+'][custom_price]" value="'+custom_price+'"><input type="hidden" class="hidden_discount" name="items['+id+'][discount]" value="'+discount+'"><input type="hidden" class="hidden_dis_price" name="items['+id+'][dis_price]" value="'+dis_price+'"><input type="hidden" class="hidden_original_price" name="items['+id+'][original_price]" value="'+original_price+'"></span></div></td></tr>';

        $('#item_body').append(str);

        var inv_str = '<tr class="'+id+'"><td><a href="#" class="item-image-div"><img src="'+image+'" alt="Image" class="img-responsive img-thumbnail item-image"></a></td><td>'+item_name+'</td><td>'+item_description+'</td><td>'+quantity+'</td><td><span class="selected_currency">'+sign+'</span>'+price+'</td><td>'+discount+'%</td><td class="text-right"><span class="selected_currency">'+sign+'</span>'+dis_price+'</td></tr>';
        $('#invoice_item').append(inv_str);

        addCartValue(price, dis_price);

        calcProfit(quantity, original_price, dis_price);
    }
    /*
    / function for delte item
    */
    function deleteItem(that){

        var a = confirm("Are you sure to delete this..?");
        if(a == false) return;
        var select_item = $(that);
        var select_item_class = '.'+ select_item.parent().parent().parent().attr('class');

        var select_tr = $(select_item_class);

        var price = select_tr.find(".hidden_total_price").val();
        var dis_price = select_tr.find(".hidden_dis_price").val();
        deleteCartValue(price, dis_price);

        var quantity = select_tr.find(".hidden_quantity").val();
        var original_price = select_tr.find(".hidden_original_price").val();
        var custom_price = select_tr.find(".hidden_custom_price").val();
        var discount = select_tr.find(".hidden_discount").val();

        deleteCalcProfit(quantity, original_price, dis_price);

        select_tr.remove();
    }




    //change cart value function
    function addCartValue(selling_price, discount_price)
    {
        var total_selling_id = $("#cart_total");     // orginal product value
        var total_discount_id = $("#cart_grand_total"); // after providering discount exept overall discount
        // var total_discount_id = $("#cart_discount");   // total discount calculate value

        var total_selling_price = Number(total_selling_id.val());
        var total_discount_price = Number(total_discount_id.val());
        selling_price = Number(selling_price);
        discount_price = Number(discount_price);
        
        /*
        * Added New Cart Value in total
        */
        total_selling_price += selling_price;
        total_discount_price += discount_price;

        total_selling_id.val(total_selling_price);
        total_discount_id.val(total_discount_price);

        var overall_discount = Number($('#overall_discount').val());
        total_discount_price = total_discount_price - ((overall_discount*total_discount_price)/100);

        var total_discount = total_selling_price - total_discount_price;

        // TAX Calcualtion
        var overall_tax = Number($('#overall_tax').val());
        var total_tax = (total_discount_price*overall_tax)/100;
        var price_to_show = total_tax + total_discount_price;



        $("#show_total_price").html(total_selling_price.toFixed(2)); // Summary -> Total Price
        $("#show_total_discount").html(total_discount.toFixed(2)); // Summary -> Total Discount
        $("#show_total_tax").html(total_tax.toFixed(2)); // Summary -> Total Tax
        // $("#show_grand_price").html(total_grand_price.val());
        $("#show_grand_price").html(price_to_show.toFixed(2));


        $("#invoice_sub_total").html(total_selling_price.toFixed(2)); // Invoice -> Total Price
        $("#invoice_total_discount").html(total_discount.toFixed(2)); // Invoice -> Total Discount
        $("#invoice_total_tax").html(total_tax.toFixed(2)); // Invoice -> Total Tax
        $("#invoice_grand_total").html(price_to_show.toFixed(2));

    }


    // Profit Caculation
    function calcProfit(quantity, original_price, discount_price){
        var total_purchase_id      = $("#analysis_total_cost"); // Total Purchase id
        var total_selling_id       = $("#analysis_total_payment"); // Total Selling id with Discount
        // var total_profit    = $("#analysis_total_profit");

        quantity        = Number(quantity);
        original_price  = Number(original_price);
        discount_price  = Number(discount_price);

        var total_purchase_cost = Number(total_purchase_id.val());
        var total_selling_cost  = Number(total_selling_id.val());

        total_purchase_cost     += (original_price*quantity);
        total_selling_cost      += discount_price;

        total_purchase_id.val(total_purchase_cost);
        total_selling_id.val(total_selling_cost);


        var overall_discount = Number($('#overall_discount').val());
        total_selling_cost -= ((total_selling_cost*overall_discount)/100);
        
        // Profit Calculation
        var profit = total_selling_cost - total_purchase_cost;





        var prof_str = '';
        if(profit >= 0) {
            var prof_per = (profit*100)/total_purchase_cost; // Profit % Calculation
            var prof_str = '<sup class="text-success">('+prof_per.toFixed(2)+'%)</sup>';
        } else{
            profit = -1 * profit;
            var prof_per = (profit*100)/total_purchase_cost; // Profit % Calculation
            var prof_str = '<sup class="text-danger">('+prof_per.toFixed(2)+'%)</sup>';
        }


        $("#profit_total_payment").html(total_selling_cost.toFixed(2));
        $("#profit_total_cost").html(total_purchase_cost.toFixed(2));
        $("#profit_total_profit").html(profit.toFixed(2));

        $("#profit_percent").html(prof_str);
    }


    // Profit Caculation
    function deleteCalcProfit(quantity, original_price, discount_price){
        var total_purchase_id      = $("#analysis_total_cost"); // Total Purchase id
        var total_selling_id       = $("#analysis_total_payment"); // Total Selling id with Discount
        // var total_profit    = $("#analysis_total_profit");

        quantity        = Number(quantity);
        original_price  = Number(original_price);
        discount_price  = Number(discount_price);

        var total_purchase_cost = Number(total_purchase_id.val());
        var total_selling_cost  = Number(total_selling_id.val());

        total_purchase_cost     -= (original_price*quantity);
        total_selling_cost      -= discount_price;

        total_purchase_id.val(total_purchase_cost);
        total_selling_id.val(total_selling_cost);


        var overall_discount = Number($('#overall_discount').val());
        total_selling_cost -= ((total_selling_cost*overall_discount)/100);
        
        // Profit Calculation
        var profit = total_selling_cost - total_purchase_cost;

        var prof_str = '';
        if(profit > 0) {
            var prof_per = (profit*100)/total_purchase_cost; // Profit % Calculation
            var prof_str = '<sup class="text-success">('+prof_per.toFixed(2)+'%)</sup>';
        } else if(profit == 0) {
            var prof_per = 0.00; // Profit % Calculation
            var prof_str = '<sup class="text-info">('+prof_per.toFixed(2)+'%)</sup>';
        } else{
            profit = -1 * profit;
            var prof_per = (profit*100)/total_purchase_cost; // Profit % Calculation
            var prof_str = '<sup class="text-danger">('+prof_per.toFixed(2)+'%)</sup>';
        }


        $("#profit_total_payment").html(total_selling_cost.toFixed(2));
        $("#profit_total_cost").html(total_purchase_cost.toFixed(2));
        $("#profit_total_profit").html(profit.toFixed(2));

        $("#profit_percent").html(prof_str);
    }


    //delete from cart value
    function deleteCartValue(selling_price, discount_price)
    {
        var total_selling_id = $("#cart_total");     // orginal product value
        var total_discount_id = $("#cart_grand_total"); // after providering discount exept overall discount
        // var total_discount_id = $("#cart_discount");   // total discount calculate value

        var total_selling_price = Number(total_selling_id.val());
        var total_discount_price = Number(total_discount_id.val());
        selling_price = Number(selling_price);
        discount_price = Number(discount_price);
        
        /*
        * Added New Cart Value in total
        */
        total_selling_price -= selling_price;
        total_discount_price -= discount_price;

        total_selling_id.val(total_selling_price);
        total_discount_id.val(total_discount_price);

        var overall_discount = Number($('#overall_discount').val());
        total_discount_price = total_discount_price - ((overall_discount*total_discount_price)/100);

        var total_discount = total_selling_price - total_discount_price;

        // TAX Calcualtion
        var overall_tax = Number($('#overall_tax').val());
        var total_tax = (total_discount_price*overall_tax)/100;
        var price_to_show = total_tax + total_discount_price;



        $("#show_total_price").html(total_selling_price.toFixed(2)); // Summary -> Total Price
        $("#show_total_discount").html(total_discount.toFixed(2)); // Summary -> Total Discount
        $("#show_total_tax").html(total_tax.toFixed(2)); // Summary -> Total Tax
        // $("#show_grand_price").html(total_grand_price.val());
        $("#show_grand_price").html(price_to_show.toFixed(2));


        $("#invoice_sub_total").html(total_selling_price.toFixed(2)); // Invoice -> Total Price
        $("#invoice_total_discount").html(total_discount.toFixed(2)); // Invoice -> Total Discount
        $("#invoice_total_tax").html(total_tax.toFixed(2)); // Invoice -> Total Tax
        $("#invoice_grand_total").html(price_to_show.toFixed(2));
    }


    // Onchange Discount 
    function changeDiscount(that){
        var field = $(that);
        var discount = field.val();    

        changeCartDiscount(discount);
        changeProfitDiscount(discount);
    }



    function changeCartDiscount(discount){
        var total_selling_id = $("#cart_total");     // orginal product value
        var total_discount_id = $("#cart_grand_total"); // after providering discount exept overall discount
        // var total_discount_id = $("#cart_discount");   // total discount calculate value

        var total_selling_price = Number(total_selling_id.val());
        var total_discount_price = Number(total_discount_id.val());

        var overall_discount = Number(discount);
        total_discount_price = total_discount_price - ((overall_discount*total_discount_price)/100);

        var total_discount = total_selling_price - total_discount_price;

        // TAX Calcualtion
        var overall_tax = Number($('#overall_tax').val());
        var total_tax = (total_discount_price*overall_tax)/100;
        var price_to_show = total_tax + total_discount_price;



        $("#show_total_price").html(total_selling_price.toFixed(2)); // Summary -> Total Price
        $("#show_total_discount").html(total_discount.toFixed(2)); // Summary -> Total Discount
        $("#show_total_tax").html(total_tax.toFixed(2)); // Summary -> Total Tax
        // $("#show_grand_price").html(total_grand_price.val());
        $("#show_grand_price").html(price_to_show.toFixed(2));


        $("#invoice_sub_total").html(total_selling_price.toFixed(2)); // Invoice -> Total Price
        $("#invoice_total_discount").html(total_discount.toFixed(2)); // Invoice -> Total Discount
        $("#invoice_total_tax").html(total_tax.toFixed(2)); // Invoice -> Total Tax
        $("#invoice_grand_total").html(price_to_show.toFixed(2));
    }


    function changeProfitDiscount(discount){
        var total_purchase_id      = $("#analysis_total_cost"); // Total Purchase id
        var total_selling_id       = $("#analysis_total_payment"); // Total Selling id with Discount
        // var total_profit    = $("#analysis_total_profit");

        var total_purchase_cost = Number(total_purchase_id.val());
        var total_selling_cost  = Number(total_selling_id.val());

        var overall_discount = Number(discount);
        total_selling_cost -= ((total_selling_cost*overall_discount)/100);
        
        // Profit Calculation
        var profit = total_selling_cost - total_purchase_cost;

        var prof_str = '';
        if(profit > 0) {
            var prof_per = (profit*100)/total_purchase_cost; // Profit % Calculation
            var prof_str = '<sup class="text-success">('+prof_per.toFixed(2)+'%)</sup>';
        } else if(profit == 0) {
            var prof_per = 0.00; // Profit % Calculation
            var prof_str = '<sup class="text-info">('+prof_per.toFixed(2)+'%)</sup>';
        } else{
            profit = -1 * profit;
            var prof_per = (profit*100)/total_purchase_cost; // Profit % Calculation
            var prof_str = '<sup class="text-danger">('+prof_per.toFixed(2)+'%)</sup>';
        }


        $("#profit_total_payment").html(total_selling_cost.toFixed(2));
        $("#profit_total_cost").html(total_purchase_cost.toFixed(2));
        $("#profit_total_profit").html(profit.toFixed(2));

        $("#profit_percent").html(prof_str);
    }



    // Onchange Tax 
    function changeTax(that){
        var field = $(that);
        var tax = field.val();

        var total_selling_id = $("#cart_total");     // orginal product value
        var total_discount_id = $("#cart_grand_total"); // after providering discount exept overall discount
        // var total_discount_id = $("#cart_discount");   // total discount calculate value

        var total_selling_price = Number(total_selling_id.val());
        var total_discount_price = Number(total_discount_id.val());

        var overall_discount = Number($('#overall_discount').val());
        total_discount_price = total_discount_price - ((overall_discount*total_discount_price)/100);

        var total_discount = total_selling_price - total_discount_price;

        // TAX Calcualtion
        var overall_tax = Number(tax);
        var total_tax = (total_discount_price*overall_tax)/100;
        var price_to_show = total_tax + total_discount_price;



        $("#show_total_price").html(total_selling_price.toFixed(2)); // Summary -> Total Price
        $("#show_total_discount").html(total_discount.toFixed(2)); // Summary -> Total Discount
        $("#show_total_tax").html(total_tax.toFixed(2)); // Summary -> Total Tax
        // $("#show_grand_price").html(total_grand_price.val());
        $("#show_grand_price").html(price_to_show.toFixed(2));


        $("#invoice_sub_total").html(total_selling_price.toFixed(2)); // Invoice -> Total Price
        $("#invoice_total_discount").html(total_discount.toFixed(2)); // Invoice -> Total Discount
        $("#invoice_total_tax").html(total_tax.toFixed(2)); // Invoice -> Total Tax
        $("#invoice_grand_total").html(price_to_show.toFixed(2));

    }



    // set client note
    function setClientNote(that){
        var field = $(that);

        var value = field.val();

        $('#client_note').html(value);

    }

    // set Payment note
    function setPaymentNote(that){
        var field = $(that);

        var value = field.val();

        $('#payment_note').html(value);

    }
</script>

<script>
    $('#selected_customer').change(function(){
        var customer_id = $(this).val();

        $('#client_email').val(null);
        $('#mobile_number').val(null);

        $.ajax({
            method: 'GET',
            url: "{{ url('admin/quotation/get_customer') }}"+'/'+customer_id ,
            datatype: "JSON",
            success: function(data) {
                if(data != null){
                    $('#customer_name').html(data.name);
                    if(data.billing_address_1 != null) $('#customer_address').html(data.billing_address_1);
                    if(data.billing_state != null || data.billing_city != null) $('#customer_city').html(data.billing_state +" "+data.billing_city);
                    if(data.mobile_number != null) $('#customer_office_phone').html(data.mobile_number);

                    if(data.email != null) $('#client_email').val(data.email);
                    if(data.mobile_number != null) $('#mobile_number').val(data.mobile_number);
                }
            },
            error: function() {
                console.log("something wrong!!!");
            }
        });
    });
</script>
