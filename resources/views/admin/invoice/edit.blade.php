@extends('layouts.admin.app')

@section('page_title', 'Quotes | Create')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/selectize/dist/css/selectize.default.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS Here */
        .btn-xs {
            padding: 5px 6px 4px 6px;
        }
        .form-control{
            padding: 0.660rem 0.75rem;
        }
        .selectize-input {
            padding: 0.530rem 0.75rem;
        }
        .selectize-dropdown.single{
            z-index: 99999999;
        }
        .selectize-dropdown [data-selectable], .selectize-dropdown .optgroup-header {
            padding: 12px 8px !important;
        }
        /* .option.selected {
            background: #262a35;
            color: #ffffff;
        } */

        .form-wizard .nav-pills > li > a.active .step {
            background-color: #262a35;
            border-color: #313644;
            color: #ffffff;
        }

        .datepicker table tr td span.active:active, .datepicker table tr td span.active:hover:active, .datepicker table tr td span.active.disabled:active, .datepicker table tr td span.active.disabled:hover:active, .datepicker table tr td span.active.active, .datepicker table tr td span.active:hover.active, .datepicker table tr td span.active.disabled.active, .datepicker table tr td span.active.disabled:hover.active, .datepicker table tr td span.active.active:hover, .datepicker table tr td span.active:hover.active:hover, .datepicker table tr td.active:active, .datepicker table tr td.active.highlighted:active, .datepicker table tr td.active.active, .datepicker table tr td.active.highlighted.active, .datepicker table tr td.active:active:hover, .datepicker table tr td.active.highlighted:active:hover, .datepicker table tr td.active.active:hover, .datepicker table tr td.active.highlighted.active:hover, .datepicker table tr td.active:active:focus, .datepicker table tr td.active.highlighted:active:focus, .datepicker table tr td.active.active:focus, .datepicker table tr td.active.highlighted.active:focus, .datepicker table tr td.active:active.focus, .datepicker table tr td.active.highlighted:active.focus, .datepicker table tr td.active.active.focus, .datepicker table tr td.active.highlighted.active.focus{
            background-color: #262a35;
            border-color: #313644;
            color: #ffffff;
        }

        .button-next.disabled{
            display: none !important;
        }
        .inv-btn-grp{
            position: absolute;
            left: -30px;
            bottom: -97px;
        }
        .btn-group .btn-rounded{
            margin-left: 5px;
            margin-right: 5px;
            border-radius: 50px !important;
        }
        .btn-group .btn-default{
            border: 1px solid #d4deee !important;
        }
        .form-control:disabled, .form-control[readonly]{
            background-color: #ffffff;
        }

        /* .item-image-div{
            margin: 2px 12px 2px 0;
            border: 1px solid #bbb;
            -moz-border-radius: 5px;
            border-radius: 5px;
            -webkit-border-radius: 5px;
            background-color: #fff;
            float: left;
            width: 90px;
            height: 90px;
            position: relative;
            z-index: 1;
            overflow: hidden;
        }
        .item-image-div .item-image{
            height: 100%;
            display: block;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate3d(-50%,-50%,0);
        } */

        .item-image{
            max-height: 125px;
        }
        .table > tbody > tr > td {
            vertical-align: middle !important;
        }
    </style>
@endsection

@section('main_content')
<div class="container-fluid">
    <div class="page-title">
        <h4>{{ __('Create New Quote') }}</h4>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-block">
                    <div class="row relative">
                        <form id="invoiceFormCreate" class="width-100" action="{{ route('admin.invoice.invoice.update', ['id' => encrypt($invoice->id)]) }}" method="post">
                            @csrf
                            <div id="rootwizard" class="form-wizard col-md-12 ml-auto mr-auto">
                                <ul class="nav nav-pills nav-fill">
                                    <li class="nav-item">
                                        <a href="#customerTab" data-toggle="tab">
                                            <span class="step">01</span>
                                            <span class="title">{{ __('Customer Info') }}</span>
                                        </a>
                                    </li>

                                    <li class="nav-item">
                                        <a href="#invoiceTab" data-toggle="tab">
                                            <span class="step">02</span>
                                            <span class="title">{{ __('Quotation Info') }}</span>
                                        </a>
                                    </li>

                                    <li class="nav-item">
                                        <a href="#confirmationTab" data-toggle="tab">
                                            <span class="step">03</span>
                                            <span class="title">{{ __('Confirmation') }}</span>
                                        </a>
                                    </li>
                                </ul>
                                <div id="bar" class="progress progress-primary" style="width: 65%">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                                </div>
                                <div class="tab-content">

                                    {{-- ================================< Customer Tab Starts >===================================== --}}
                                    <div id="customerTab" class="tab-pane fade in active">
                                        <div class="row">
                                            <div class="col-md-10 mr-auto ml-auto">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>{{ __('Customer') }} <span class="required">*</span></label>
                                                            <div class="mrg-top-0">
                                                                <select class="selectize-group @error('customer_id') is-invalid @enderror" name="customer_id" id="selected_customer" required>
                                                                    <option value="" disabled>{{ __('Select Customer') }}</option>
                                                                    @foreach($customers as $customer)
                                                                        <option value="{{ $customer->id }}" @if($invoice->customer_id == $customer->id)selected @endif>{{ $customer->name }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>

                                                            @error('name')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>{{ __('Email') }}</label>
                                                            <input type="email" class="form-control" id="client_email" name="client_email" value="{{ $invoice->client_email }}" placeholder="client@mail.com">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>{{ __('Phone Number') }}</label>
                                                            <input type="text" class="form-control" id="mobile_number" name="mobile_number" value="{{ $invoice->mobile_number }}" placeholder="+880 1712 145678">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>{{ __('Client Notes') }}</label>
                                                            <textarea class="form-control" oninput="setClientNote(this);" name="client_note" rows="4" placeholder="{{ __('Client Notes') }}">{{ $invoice->client_note }}</textarea>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <label>{{ __('Payment Terms') }}</label>
                                                        <textarea class="form-control" oninput="setPaymentNote(this);" name="payment_term" rows="4" placeholder="{{ __('Payment Terms') }}">{!! $invoice->payment_term !!}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- ================================< Customer Tab Ends >===================================== --}}

                                    {{-- ================================< Invoice Tab Starts >===================================== --}}
                                    <div id="invoiceTab" class="tab-pane fade">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-xl-12 col-md-12">
                                                        <div class="card">
                                                            <div class="card-block p-b-0">
                                                                <div class="row">
                                                                    <div class="col-xl-6 col-md-6">
                                                                        <div class="form-group">
                                                                            <label>{{ __('Currency') }} <span class="required">*</span></label>
                                                                            <div class="mrg-top-0">
                                                                                <select class="selectize-group" name="currency" onchange="changeCurrency(this);" id="selected_currency" required>
                                                                                    <option value="" disabled>{{ __('Select Currency') }}</option>
                                                                                    @php
                                                                                        $selected_sign = $invoice->currency->sign;
                                                                                    @endphp
                                                                                    @foreach($currencies as $currency)
                                                                                        <option value="{{ $currency->id.'-'.$currency->sign }}" @if($currency->id == $invoice->currency_id)selected @endif>{{ $currency->name." (".$currency->sign }})</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-xl-6 col-md-6">
                                                                        <div class="form-group">
                                                                            <label>{{ __('Quotation Number') }} <span class="required">*</span></label>
                                                                            <input type="text" class="form-control" name="invoice_number" value="{{ $invoice->inv_number }}" placeholder="INV-000001" required readonly>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-xl-6 col-md-6">
                                                                        <div class="form-group">
                                                                            <label>{{ __('Expire Date') }}</label>
                                                                            <div class="timepicker-input input-icon form-group">
                                                                                {{-- <i class="ti-time m-t-5"></i> --}}
                                                                                @php
                                                                                    $date = new DateTime($invoice->exp_date);
                                                                                @endphp
                                                                                <input type="text" value="{{ $date->format('m/d/Y') }}" name="expire_date" class="form-control datepicker-1" placeholder="mm/dd/yyyy" data-provide="datepicker">
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-xl-6 col-md-6">
                                                                        <div class="form-group">
                                                                            <label>{{ __('Sales Person') }}</label>
                                                                            <div class="mrg-top-0">
                                                                                <select class="selectize-group" name="sales_person">
                                                                                    <option value="" disabled>{{ __('Select Sales Person') }}</option>
                                                                                    @foreach($admins as $admin)
                                                                                        <option value="{{ $admin->id }}" @if($admin->id == $invoice->sales_person)selected @endif>{{ $admin->name }}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xl-12 col-md-12">
                                                        <div class="card">
                                                            <div class="card-heading border bottom">
                                                                <h4 class="card-title float-left p-t-3 text-uppercase text-bold">{{ __('All Items') }}</h4>

                                                                <a href="javascript:void(0);" data-toggle="modal" data-target="#add_item" class="btn btn-dark btn-sm float-right m-b-0">{{ __('Add Item') }}</a>
                                                            </div>
                                                            <div class="card-block p-3 p-b-0">
                                                                <table class="table table-hover table-bordered product-table table-responsive m-b-0">
                                                                    <thead>
                                                                        <tr>
                                                                            <th class="text-center"><i class="ti-arrow-down text-danger"></i></th>
                                                                            <th class="text-center">{{ __('Subtotal') }}</th>
                                                                            <th class="text-center">{{ __('Price') }}</th>
                                                                            <th class="text-center">{{ __('Quantity') }}</th>
                                                                            <th class="text-center">{{ __('Description') }}</th>
                                                                            <th class="text-center">{{ __('Item') }}</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody id="item_body">
                                                                        @php
                                                                            $sub_total = 0;
                                                                            $selling_price = 0;
                                                                            $total_original_price = 0;
                                                                        @endphp
                                                                        @foreach($invoice->items as $item)
                                                                            <tr class="{{ $item->secret_id }}">
                                                                                @php
                                                                                    $price = $item->custom_price*$item->quantity;
                                                                                    $selling_price += $price;
                                                                                    $total_original_price += $item->original_price*$item->quantity;
                                                                                    $dis_price = $price - (($price*$item->discount)/100);
                                    
                                                                                    $sub_total += $dis_price;
                                                                                @endphp
                                                                                <td>
                                                                                    <div class="m-6 text-center"><a href="javascript:void(0);" onclick="deleteItem(this);" class="m-0 text-danger" co><i class="ti-close"></i></a></div>
                                                                                </td>
                                                                                <td>
                                                                                    <div class="m-6 text-center"><span class="text-dark"><b><span class="selected_currency">{{ $invoice->currency->sign }}</span>{{ $item->dis_price }}</b><sup class="text-bold text-danger"><del>(<span class="selected_currency">{{ $invoice->currency->sign }}</span>{{ $item->total_price }})</del></sup></span>
                                                                                    </div>
                                                                                </td>
                                                                                <td>
                                                                                    <div class="m-6 text-center"><b class="text-dark"><b><span class="selected_currency">{{ $invoice->currency->sign }}</span>{{ $item->total_price }}</b></span>
                                                                                    </div>
                                                                                </td>
                                                                                <td>
                                                                                    <div class="m-6 text-center"><span class="text-dark"><b>{{ $item->quantity }}</b></span></div>
                                                                                </td>
                                                                                <td>
                                                                                    <div class="m-6 text-center"><span class="text-dark"><b>{{ $item->item->description }}...</b></span></div>
                                                                                </td>
                                                                                <td>
                                                                                    <div class="m-6 text-center">
                                                                                        <span class="text-dark"><b>{{ $item->item->name }}</b>
                                                                                            <input type="hidden" class="hidden_id" name="items[{{ $item->secret_id }}][item_id]" value="{{ $item->item_id }}">
                                                                                            <input type="hidden" class="hidden_quantity" name="items[{{ $item->secret_id }}][quantity]" value="{{ $item->quantity }}">
                                                                                            <input type="hidden" class="hidden_total_price" name="items[{{ $item->secret_id }}][total_price]" value="{{ $item->total_price }}">
                                                                                            <input type="hidden" class="hidden_custom_price" name="items[{{ $item->secret_id }}][custom_price]" value="{{ $item->custom_price }}">
                                                                                            <input type="hidden" class="hidden_discount" name="items[{{ $item->secret_id }}][discount]" value="{{ $item->discount }}">
                                                                                            <input type="hidden" class="hidden_dis_price" name="items[{{ $item->secret_id }}][dis_price]" value="{{ $item->dis_price }}">
                                                                                            <input type="hidden" class="hidden_original_price" name="items[{{ $item->secret_id }}][original_price]" value="{{ $item->original_price }}">
                                                                                        </span>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        @endforeach
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @php
                                                        $overall_discount = (($sub_total*$invoice->discount)/100);
                                                        $grand_sub_total = $sub_total - $overall_discount;
                                                        $total_tax = (($grand_sub_total*$invoice->tax)/100);
                                                        $total_payable = $grand_sub_total + $total_tax;

                                                        //profit calcualation
                                                        $profit = $grand_sub_total - $invoice->total_cost;
                                                    @endphp

                                                    <div class="col-xl-4 col-md-4">
                                                        <div class="card">
                                                            <div class="card-block">
                                                                <h4 class="card-title mrg-btm-30 text-center text-uppercase text-bold">{{ __('Profit Analysis') }}</h4>
                                                                <div class="border bottom">
                                                                    <input type="hidden" name="analysis_total_payment" id="analysis_total_payment" value="{{ $sub_total }}">
                                                                    <input type="hidden" name="analysis_total_cost" id="analysis_total_cost" value="{{ $total_original_price }}">
                                                                    {{-- <input type="hidden" name="analysis_total_profit" id="analysis_total_profit" value="0"> --}}
                                                                    <p>{{ __('Total Payment') }} <span class="pull-right"><span class="selected_currency">{{ $selected_sign }}</span><span id="profit_total_payment">{{ round($grand_sub_total, 2) }}</span></span></p>
                                                                    <p class="mrg-top-20">{{ __('Our Total Cost') }} <span class="pull-right"><span class="selected_currency">{{ $selected_sign }}</span><span id="profit_total_cost">{{ round($total_original_price, 2) }}</span></span></p>
                                                                </div>
                                                                <p class="mrg-top-20">{{ __('Total Profit') }} <span class="pull-right text-dark font-size-18"><b><span class="selected_currency">{{ $selected_sign }}</span><span id="profit_total_profit">{{ round($profit, 2) }}</span> <span id="profit_percent">
                                                                    @php
                                                                        $prof_str = '';
                                                                        if($profit >= 0) {
                                                                            $prof_per = ($profit*100)/$invoice->total_cost; // $Profit % Calculation
                                                                            $prof_str = '<sup class="text-success">('.round($prof_per, 2).'%)</sup>';
                                                                        } else{
                                                                            $profit = -1 * $profit;
                                                                            $prof_per = ($profit*100)/$invoice->total_cost; // $Profit % Calculation
                                                                            $prof_str = '<sup class="text-danger">('.round($prof_per, 2).'%)</sup>';
                                                                        }
                                                                    @endphp
                                                                    {!! $prof_str !!}
                                                                </span> </b></span></p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xl-4 col-md-4">
                                                        <div class="card">
                                                            <div class="card-block">
                                                                <div class="col-12">
                                                                    <div class="form-group">
                                                                        <label>{{ __('Overall Discount') }} <span class="required">%</span></label>
                                                                        <input type="number" oninput="changeDiscount(this);" min="0" id="overall_discount" max="100" class="form-control" name="overall_discount" value="{{ $invoice->discount }}" placeholder="10%">
                                                                        <small>{{ __('If you wish to give extra discount') }}</small>
                                                                    </div>
                                                                </div>
                                                                <div class="col-12">
                                                                    <div class="form-group">
                                                                        <label>{{ __('Overall Tax') }} <span class="required">%</span></label>
                                                                        <input type="number" oninput="changeTax(this);" min="0" class="form-control" id="overall_tax" name="tax" value="{{ $invoice->tax }}" placeholder="17%">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xl-4 col-md-4">
                                                        <div class="card">
                                                            <div class="card-block">
                                                                <h4 class="card-title mrg-btm-30 text-center text-uppercase text-bold">{{ __('Summary') }}</h4>
                                                                <div class="border bottom">
                                                                    <input type="hidden" name="cart_total" id="cart_total" value="{{ round($selling_price, 2) }}">
                                                                    {{-- <input type="hidden" name="cart_discount" id="cart_discount" value="0"> --}}
                                                                    <input type="hidden" name="cart_grand_total" id="cart_grand_total" value="{{ round($sub_total, 2) }}">
                                                                    <p>
                                                                        {{ __('Total Price') }} 
                                                                        <span class="pull-right">
                                                                            <span class="selected_currency">{{ $selected_sign }}</span>
                                                                            <span id="show_total_price">{{ round($selling_price, 2) }}</span>
                                                                        </span>
                                                                    </p>
                                                                    <p class="mrg-top-20">
                                                                        {{ __('Total Discount') }} 
                                                                        <span class="pull-right">
                                                                            <span class="selected_currency">{{ $selected_sign }}</span>
                                                                            <span id="show_total_discount">{{ round($overall_discount, 2) }}</span>
                                                                        </span>
                                                                    </p>
                                                                    <p class="mrg-top-20">
                                                                        {{ __('Total Tax') }} 
                                                                        <span class="pull-right">
                                                                            <span class="selected_currency">{{ $selected_sign }}</span>
                                                                            <span id="show_total_tax">{{ round($total_tax, 2) }}</span>
                                                                        </span>
                                                                    </p>
                                                                </div>
                                                                <p class="mrg-top-20">{{ __('Grand Total') }} <span class="pull-right text-dark font-size-18"><b><span class="selected_currency">{{ $selected_sign }}</span><span id="show_grand_price">{{ round($total_payable, 2) }}</span></b></span></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- ================================< Invoice Tab Ends >===================================== --}}

                                    {{-- ================================< Confirmation Tab Starts >===================================== --}}
                                    <div id="confirmationTab" class="tab-pane fade">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <div class="pdd-horizon-30">
                                                            <div class="row">
                                                                <div class="col-lg-6 col-md-6 text-left">
                                                                    @if($invoice->company->logo != null)
                                                                        <img class="img-responsive" src="{{ $invoice->company->logo }}" alt="">
                                                                    @else
                                                                        <img class="img-responsive" src="{{ auth()->user()->company->logo }}" alt="">
                                                                    @endif
                                                                </div>
                                                                <div class="col-lg-6 col-md-6">
                                                                    {{-- <address class="pdd-left-10 mrg-top-20">
                                                                        <b class="text-dark">{{ auth()->user()->company->name }}</b><br>
                                                                        <span>{{ auth()->user()->company->state.", ".auth()->user()->company->city }}</span><br>
                                                                        <span>{{ auth()->user()->company->country }}</span><br>
                                                                        <span><span class="text-bold">Contact:</span> {{ auth()->user()->company->address_1 }}</span><br>
                                                                    </address> --}}
                                                                </div>
                                                            </div>

                                                            <div class="row mrg-top-20">
                                                                <div class="col-md-6 col-sm-6 text-right">
                                                                    <h3 class="pdd-left-10 mrg-top-10 text-bold">{{ __('QUOTATION FROM') }}</h3>
                                                                    <address class="pdd-left-10 mrg-top-20">
                                                                        <b class="text-dark">{{ $invoice->company->name }}</b><br>
                                                                        <span>{{ $invoice->company->state.", ".$invoice->company->city }}</span><br>
                                                                        <span>{{ $invoice->company->country }}</span><br>
                                                                        <span><span class="text-bold">{{ __('Contact:') }}</span> {{ $invoice->user->email }}</span><br>
                                                                    </address>
                                                                </div>

                                                                <div class="col-md-6 col-sm-6 text-right">
                                                                    <h3 class="pdd-left-10 mrg-top-10 text-bold">{{ __('QUOTATION TO') }}</h3>
                                                                    <address class="pdd-left-10 mrg-top-20">
                                                                        <b class="text-dark" id="customer_name">{{ $invoice->customer->name }}</b><br>
                                                                        <span id="customer_address">{{ $invoice->customer->billing_city.", ".$invoice->customer->billing_state }}</span><br>
                                                                        <span id="customer_city">{{ $invoice->customer->billing_country }}</span><br>
                                                                        <span><span class="text-bold" id="customer_office_phone">{{ __('Contact:') }} {{ $invoice->client_email }}</span></span><br>
                                                                    </address>
                                                                </div>
                                                            </div>
                                                            <div class="row mrg-top-20">
                                                                <div class="col-md-12">
                                                                    <table class="table table-hover table-responsive">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>{{ __('Image') }}</th>
                                                                                <th>{{ __('Items') }}</th>
                                                                                <th>{{ __('Description') }}</th>
                                                                                <th>{{ __('Quantity') }}</th>
                                                                                <th>{{ __('Price') }}</th>
                                                                                <th>{{ __('Discount') }}</th>
                                                                                <th class="text-right">{{ __('Total') }}</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody id="invoice_item">
                                                                            @foreach($invoice->items as $item)
                                                                                <tr class="{{ $item->secret_id }}">
                                                                                    <td>
                                                                                        <a href="#" class="item-image-div"><img src="{{ $item->item->avatar }}" alt="Image" class="img-responsive img-thumbnail item-image"></a>
                                                                                    </td>
                                                                                    <td>{{ $item->item->name }}</td>
                                                                                    <td>{{ $item->item->description }}</td>
                                                                                    <td>{{ $item->quantity }}</td>
                                                                                    <td><span class="selected_currency">{{ $invoice->currency->sign }}</span>{{ $item->custom_price }}</td>
                                                                                    <td>{{ $item->discount }}%</td>
                                                                                    <td class="text-right"><span class="selected_currency">{{ $invoice->currency->sign }}</span>{{ round($item->dis_price, 2) }}</td>
                                                                                </tr>
                                                                            @endforeach
                                                                        </tbody>
                                                                    </table>
                                                                    <div class="row mrg-top-30 justify-content-end">
                                                                        <div class="col-md-5">
                                                                            <hr>
                                                                            <table class="table table-borderless table-responsive">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <th class="text-right"><b>{{ __('Sub Total:') }}</b></th>
                                                                                        <td class="text-right"><b>
                                                                                            <span class="selected_currency">{{ $selected_sign }}</span>
                                                                                            <span id="invoice_sub_total">{{ round($selling_price, 2) }}</span></b>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <th class="text-right"><b>{{ __('Discount:') }}</b></th>
                                                                                        <td class="text-right"><b>
                                                                                            <span class="selected_currency">{{ $selected_sign }}</span>
                                                                                            <span id="invoice_total_discount">{{ round($overall_discount, 2) }}</span></b>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <th class="text-right"><b>{{ __('Tax:') }}</b></th>
                                                                                        <td class="text-right"><b>
                                                                                            <span class="selected_currency">{{ $selected_sign }}</span>
                                                                                            <span id="invoice_total_tax">{{ round($total_tax, 2) }}</span></b>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr class="text-primary">
                                                                                        <th class="text-right border-top-1">
                                                                                            <b>{{ __('Total Amount To Pay:') }}</b>
                                                                                        </th>
                                                                                        <td class="text-right border-top-1"><b><span class="selected_currency">{{ $selected_sign }}</span><span id="invoice_grand_total">{{ round($total_payable, 2) }}</span></b></td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                            <hr>
                                                                        </div>
                                                                    </div>

                                                                    {{-- Payment Method --}}
                                                                    <div class="row mrg-top-30">
                                                                        <div class="col-lg-6 col-md-6 text-right border right">
                                                                            <hr>
                                                                            <div class="pdd-vertical-20">
                                                                                <h4 class="text-info text-bold">{{ __('Client Notes') }}</h4>
                                                                                <p class="text-dark" id="client_note">
                                                                                    {!! $invoice->client_note !!}
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-6 col-md-6 text-right">
                                                                            <hr>
                                                                            <div class="pdd-vertical-20">
                                                                                <h4 class="text-info text-bold">{{ __('Payment Terms') }}</h4>
                                                                                <p class="text-dark" id="payment_note">
                                                                                    {!! $invoice->payment_term !!}
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="btn-group inv-btn-grp">
                                                                        <button type="submit" class="btn btn-dark button-submit pull-right btn-rounded">{{ __('Save Quote') }}</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- ================================< Confirmation Tab Ends >===================================== --}}

                                    <div class="wizard-pager">
                                        <div class="">
                                            <button type="button" class="btn btn-default button-previous btn-rounded">Previous</button>
                                            <button type="button" class="btn btn-dark button-next pull-right btn-rounded">Next</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('admin.invoice.modals.add_item')
</div>
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/selectize/dist/js/standalone/selectize.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/jquery-validation/dist/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/summernote/dist/summernote.min.js') }}"></script>

    {{-- <script src="{{ asset('assets/js/forms/form-wizard.js') }}"></script> --}}
    <script src="{{ asset('assets/js/forms/form-elements.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
        $('.selectize-group').selectize({
            sortField: 'text'
        });
        (function ($) {
            'use strict';

            var $validator = $("#invoiceFormCreate").validate({
                rules: {
                    // invoice_number: {
                    //     required: true,
                    //     // minlength: 3
                    // },
                    // recurring_days: {
                    //     // required: true,
                    //     min: 1,
                    //     max:365
                    // },
                    // expire_date: {
                    //     required: true,
                    //     date: true
                    // },
                    // client_id: {
                    //     required: true
                    // },
                    // client_email: {
                    //     email: true
                    // },
                    // mobile_number: {
                    //     required: true
                    // },
                }
            });

            function validationChecking() {
                var $valid = $('#invoiceFormCreate').valid();
                if (!$valid) {
                    $validator.focusInvalid();
                    return false;
                }
            }

            $('#rootwizard').bootstrapWizard({
                tabClass: '',
                'nextSelector': '.button-next',
                'previousSelector': '.button-previous',
                onNext: validationChecking,
                onLast: validationChecking,
                onTabClick: validationChecking,
                onTabShow: function(tab, navigation, index) {
                    var $total = navigation.find('li').length;
                    var $current = index+0;
                    var $percent = ($current/$total) * 133;
                    $('#rootwizard .progress-bar').css({width:$percent+'%'});
                }});
        })(jQuery);
    </script>
    @include('admin.invoice.custom_scripts')
@endsection

