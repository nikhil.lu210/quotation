<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title> {{ $invoice->inv_number }} </title>
    <meta name="robots" content="noindex,nofollow" />
    <meta name="viewport" content="width=device-width; initial-scale=1.0;" />
    <style type="text/css">
        @import url(https://fonts.googleapis.com/css?family=Open+Sans:400,700);
        body {
            margin: 0;
            padding: 0;
            background: #fefefe;
        }
        
        div,
        p,
        a,
        li,
        td {
            -webkit-text-size-adjust: none;
        }
    </style>

</head>

<body>
    <!-- Header -->
    <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="fullTable" bgcolor="#fefefe">
        <tr>
            <td height="20"></td>
        </tr>
        <tr>
            <td>
                <table width="800" border="0" cellpadding="0" cellspacing="0" align="center" class="fullTable" bgcolor="#ffffff" style="border-radius: 10px 10px 0 0;">
                    <tr class="hiddenMobile">
                        <td height="40"></td>
                    </tr>
                    <tr class="visibleMobile">
                        <td height="30"></td>
                    </tr>

                    <tr>
                        <td>
                            <table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="fullPadding">
                                <tbody>
                                    <tr>
                                        <td>
                                            <table width="100%" border="0" cellpadding="0" cellspacing="0" align="left" class="col">
                                                <tbody>
                                                    <tr>
                                                        <td align="left">
                                                            @php
                                                                $date = $invoice->created_at;
                                                                $date = $date->format('m/d/Y')
                                                            @endphp
                                                            
                                                            <div style="font-size: 14px; font-family: 'Open Sans', sans-serif; color: #5b5b5b; line-height: 0;">
                                                                <strong>{{ __('Date:') }} {{ $date }}</strong>
                                                            </div>
                                                            <br>
                                                            <div style="font-size: 14px; font-family: 'Open Sans', sans-serif; color: #5b5b5b; line-height: 1;">
                                                                <strong>{{ __('Quotation Number:') }} <span style="color: #ff0000;">{{ $invoice->inv_number }}</span></strong>
                                                            </div>
                                                        </td>
                                                        <td align="right">
                                                            @if($invoice->company->logo != null)
                                                                <img src="{{ $invoice->company->logo }}" alt="logo" border="0" />
                                                            @else
                                                                <h2 class="text-info">{{ $invoice->company->name }}</h2>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr class="hiddenMobile">
                                                        <td height="40"></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <!-- /Header -->

    
    
    
    <!-- Information -->
    <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="fullTable" bgcolor="#fefefe">
        <tbody>
            <tr>
                <td>
                    <table width="800" border="0" cellpadding="0" cellspacing="0" align="center" class="fullTable" bgcolor="#ffffff">
                        <tbody>
                            <tr>
                                <tr>
                                    <td>
                                        <table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="fullPadding">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <table width="220" border="0" cellpadding="0" cellspacing="0" align="left" class="col">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #5b5b5b; line-height: 1; vertical-align: top; text-align: right;">
                                                                        <strong>{{ __('QUOTATION FROM') }}</strong>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="100%" height="10"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #5b5b5b; line-height: 20px; vertical-align: top; text-align: right;">
                                                                        {{ $invoice->company->name }}
                                                                        <br> {{ $invoice->company->state }}, {{ $invoice->company->postal_code }}, {{ $invoice->company->country }}
                                                                        @if($invoice->user != null)
                                                                            <br> <a href="mailto:admin_mail" style="color: #ff0000; text-decoration:underline;">{{ $invoice->user->email }}</a>
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>

                                                        <table width="220" border="0" cellpadding="0" cellspacing="0" align="right" class="col">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #5b5b5b; line-height: 1; vertical-align: top; text-align: right;">
                                                                        <strong>{{ __('QUOTATION TO') }}</strong>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="100%" height="10"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #5b5b5b; line-height: 20px; vertical-align: top; text-align: right;">
                                                                        {{ $invoice->customer->name }}
                                                                        <br> {{ $invoice->customer->billing_state }}, {{ $invoice->customer->billing_postal }}, {{ $invoice->customer->billing_country }}
                                                                        <br> <a href="mailto:customer_mail" style="color: #ff0000; text-decoration:underline;">{{ ($invoice->client_email != $invoice->customer->email) ? $invoice->client_email : $invoice->customer->email }}</a>
                                                                        <br> <a href="tel:customer_phone" style="color: #ff0000; text-decoration:underline;">{{ $invoice->customer->mobile_number }}</a>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="visibleMobile">
                                    <td height="30"></td>
                                </tr>
                        </tbody>
                    </table>
                </td>
                </tr>
        </tbody>
    </table>
    <!-- /Information -->


    <!-- Order Details -->
    <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="fullTable" bgcolor="#fefefe">
        <tbody>
            <tr>
                <td>
                    <table width="800" border="0" cellpadding="0" cellspacing="0" align="center" class="fullTable" bgcolor="#ffffff">
                        <tbody>
                            <tr>
                                <tr class="visibleMobile">
                                    <td height="30"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="fullPadding">
                                            <tbody>
                                                <tr>
                                                    <th style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #1e2b33; font-weight: normal; line-height: 1; vertical-align: top; padding: 0 0 7px; width: 10%;" align="right">
                                                        {{ __('Subtotal') }}
                                                    </th>
                                                    <th style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #5b5b5b; font-weight: normal; line-height: 1; vertical-align: top; padding: 0 0 7px; width: 10%;" align="right">
                                                        {{ __('Discount') }}
                                                    </th>
                                                    <th style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #5b5b5b; font-weight: normal; line-height: 1; vertical-align: top; padding: 0 0 7px; width: 10%;" align="right">
                                                        {{ __('Price') }}
                                                    </th>
                                                    <th style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #5b5b5b; font-weight: normal; line-height: 1; vertical-align: top; padding: 0 0 7px; width: 10%;" align="right">
                                                        {{ __('Quantity') }}
                                                    </th>
                                                    <th style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #5b5b5b; font-weight: normal; line-height: 1; vertical-align: top; padding: 0 0 7px; width: 30%;" align="right">
                                                        <small>{{ __('Description') }}</small>
                                                    </th>
                                                    <th style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #5b5b5b; font-weight: normal; line-height: 1; vertical-align: top; padding: 0 10px 7px 0; width: 20%;" align="right">
                                                        {{ __('Item') }}
                                                    </th>
                                                    <th style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #5b5b5b; font-weight: normal; line-height: 1; vertical-align: top; padding: 0 10px 7px 0; width: 10%;" align="right">
                                                        {{ __('Image') }}
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td height="1" style="background: #bebebe;" colspan="8"></td>
                                                </tr>
                                                <tr>
                                                    <td height="10" colspan="8"></td>
                                                </tr>

                                                @php
                                                    $sub_total = 0;
                                                @endphp
                                                @foreach($invoice->items as $list => $item)
                                                    @if($item->item == null)
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td>{{ __('Item not found') }}</td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                                        @php
                                                            continue;
                                                        @endphp
                                                    @endif

                                                    <tr valign="middle">

                                                        @php
                                                            $price = $item->custom_price*$item->quantity;
                                                            $dis_price = $price - (($price*$item->discount)/100);

                                                            $sub_total += $dis_price;
                                                        @endphp

                                                        <td style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #1e2b33; padding:0;" align="right">
                                                            <a  href="{{ $item->item->inventory_url }}" target="_blank" style="text-decoration: none; color: #333; font-weight: bold;">{{ $invoice->currency->sign }}{{ round($dis_price, 2) }}</a>
                                                        </td>

                                                        <td style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #646a6e; padding:0;"  align="right">
                                                            <a href="{{ $item->item->inventory_url }}" target="_blank" style="text-decoration: none; color: #333; font-weight: bold;">{{ $item->discount }}%</a>
                                                        </td>

                                                        <td style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #646a6e; padding:0;"  align="right">
                                                            <a href="{{ $item->item->inventory_url }}" target="_blank" style="text-decoration: none; color: #333; font-weight: bold;">{{ $invoice->currency->sign }}{{ round($price, 2) }}</a>
                                                        </td>
                                                        
                                                        <td style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #646a6e; padding:0;"  align="right">
                                                            <a href="{{ $item->item->inventory_url }}" target="_blank" style="text-decoration: none; color: #333; font-weight: bold;">{{ $item->quantity }}</a>
                                                        </td>

                                                        <td style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #646a6e; padding:0;" align="right">
                                                            <a href="{{ $item->item->inventory_url }}" target="_blank" style="text-decoration: none; color: #333; font-weight: bold;">{{ $item->item->description }}</a>
                                                        </td>

                                                        <td style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #ff0000; padding:0;" class="article" align="right">
                                                            <a href="{{ $item->item->inventory_url }}" target="_blank" style="text-decoration: none; color: #333; font-weight: bold;">{{ $item->item->name }}</a>
                                                        </td>

                                                        <td style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #ff0000; padding:0;" class="article" align="right">
                                                            <a href="{{ $item->item->inventory_url }}" target="_blank" style="text-decoration: none; color: #333; font-weight: bold;">
                                                                <img src="{{ $item->item->avatar }}" alt="" style="max-height: 100px;">
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    
                                                    <tr>
                                                        <td height="1" colspan="8" style="border-bottom:1px solid #e4e4e4"></td>
                                                    </tr>
                                                @endforeach

                                                <tr>
                                                    <td height="1" colspan="8" style="border-bottom:1px solid #e4e4e4"></td>
                                                </tr>

                                                <tr>
                                                    <td height="1" colspan="8" style="border-bottom:1px solid #e4e4e4"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="20"></td>
                                </tr>
                        </tbody>
                    </table>
                </td>
                </tr>
        </tbody>
    </table>
    <!-- /Order Details -->




    <!-- Total -->
    <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="fullTable" bgcolor="#fefefe">
        <tbody>
            <tr>
                <td>
                    <table width="800" border="0" cellpadding="0" cellspacing="0" align="center" class="fullTable" bgcolor="#ffffff">
                        <tbody>
                            <tr>
                                <td>

                                    <!-- Table Total -->
                                    <table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="fullPadding">
                                        <tbody>
                                            <tr>
                                                <td style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #646a6e; line-height: 22px; vertical-align: top; text-align:right; ">
                                                    {{ __('Sub Total:') }}
                                                </td>
                                                <td style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #646a6e; line-height: 22px; vertical-align: top; text-align:right; white-space:nowrap;" width="80">
                                                    {{ $invoice->currency->sign }}{{ $sub_total }}
                                                </td>
                                            </tr>
                                            @php
                                                $overall_discount = (($sub_total*$invoice->discount)/100);
                                                $sub_total = $sub_total - $overall_discount;
                                                $total_tax = (($sub_total*$invoice->tax)/100);
                                                $total_payable = $sub_total + $total_tax;
                                            @endphp
                                            <tr>
                                                <td style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #646a6e; line-height: 22px; vertical-align: top; text-align:right; ">
                                                    {{ __('Overall Discount:') }} 
                                                </td>
                                                <td style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #646a6e; line-height: 22px; vertical-align: top; text-align:right; ">
                                                    {{ $invoice->currency->sign }}{{ round($overall_discount, 2) }}
                                                    <sup style="display: none;">({{ ($invoice->discount != null)? $invoice->discount:0 }}%)</sup>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #646a6e; line-height: 22px; vertical-align: top; text-align:right; ">
                                                    {{ __('Tax:') }}
                                                </td>
                                                <td style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #646a6e; line-height: 22px; vertical-align: top; text-align:right; ">
                                                    {{ $invoice->currency->sign }}{{ round($total_tax, 2) }}
                                                    <sup style="display: none;">({{ ($invoice->tax != null)? $invoice->tax:0 }}%)</sup>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #000; line-height: 22px; vertical-align: top; text-align:right; ">
                                                    <strong>{{ __('Grand Total (Incl.Tax)') }}</strong>
                                                </td>
                                                <td style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #000; line-height: 22px; vertical-align: top; text-align:right; ">
                                                    <strong>{{ $invoice->currency->sign }}{{ round($total_payable, 2) }}</strong>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <!-- /Table Total -->
                                    
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <!-- /Total -->




    <table style="margin-top:25px;" width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="fullTable" bgcolor="#fefefe">
        <tr>
            <td>
                <table width="800" border="0" cellpadding="0" cellspacing="0" align="center" class="fullTable" bgcolor="#ffffff" style="border-radius: 0 0 10px 10px;">
                    <tr>
                        <td>
                            <table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="fullPadding">
                                <tbody>
                                    <tr style="margin-top:25px;">
                                        <td style="font-size: 12px; color: #5b5b5b; font-family: 'Open Sans', sans-serif; line-height: 18px; vertical-align: top; text-align: right;">
                                            <h3>{{ __('Client Notes') }}</h3>
                                            <p>{!! $invoice->client_note !!}</p>
                                        </td>
                                    </tr>
                                    <tr style="margin-top:25px;">
                                        <td style="font-size: 12px; color: #5b5b5b; font-family: 'Open Sans', sans-serif; line-height: 18px; vertical-align: top; text-align: right;">
                                            <h3>{{ __('Payment Terms') }}</h3>
                                            <p>{!! $invoice->payment_term !!}</p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr class="spacer">
                        <td height="50"></td>
                    </tr>

                </table>
            </td>
        </tr>
        <tr>
            <td height="20"></td>
        </tr>
    </table>

</body>

</html>