@extends('layouts.admin.app')

@section('page_title', 'Quotations')

@section('css_links')
    {{--  External CSS  --}}

@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS Here */
        a{
            text-decoration: none !important;
        }
        table.table.table-borderless tbody tr th,
        table.table.table-borderless tbody tr td{
            line-height: 0.5;
            border-color: #ffffff;
        }
        .border-top-1{
            border-top: 1px dashed #dddddd !important;
        }
        .table > tbody > tr > td {
            vertical-align: middle !important;
        }
        .item-image{
            max-height: 125px;
        }
    </style>
    <style>
        /* Custom CSS Here */
        .clickable-row{
            cursor: pointer;
        }
        .table > tbody > tr > td {
            vertical-align: middle !important;
        }
    </style>
@endsection

@section('main_content')
<div class="container-fluid">
    <div class="container">
        <div class="card">
            <div class="pdd-vertical-5 pdd-horizon-10 border bottom print-invisible">
                <ul class="list-unstyle list-inline text-right">
                    <li class="list-inline-item">
                        <a href="#" class="btn text-gray text-hover display-block padding-10 no-mrg-btm" onclick="window.print();">
                            <i class="ti-printer text-info pdd-right-5"></i>
                            <b>{{ __('Print Quotation') }}</b>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="{{ route('admin.invoice.pdf_generate', ['id' => encrypt($invoice->id)]) }}" class="text-gray text-hover display-block padding-10 no-mrg-btm">
                            <i class="fa fa-file-pdf-o text-danger pdd-right-5"></i>
                            <b>{{ __('Export PDF') }}</b>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="{{ route('admin.invoice.send.email', ['invoice_id' => encrypt($invoice->id)]) }}" class="text-gray text-hover display-block padding-10 no-mrg-btm confirmation">
                            <i class="fa fa-envelope text-primary pdd-right-5"></i>
                            <b>{{ __('Send Mail') }}</b>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="{{ route('admin.invoice.invoice.edit', ['id' => encrypt($invoice->id)]) }}" class="text-gray text-hover display-block padding-10 no-mrg-btm confirmation">
                            <i class="fa fa-pencil text-warning pdd-right-5"></i>
                            <b>{{ __('Edit Quotation') }}</b>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="card-body">
                <div class="pdd-horizon-30">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 text-left">
                            @if($invoice->company->logo != null)
                                <img class="img-responsive" src="{{ $invoice->company->logo }}" alt="">
                            @else
                                <h2 class="text-info">{{ $invoice->company->name }}</h2>
                            @endif
                        </div>
                        {{-- <div class="col-lg-6 col-md-6">
                            <address class="pdd-left-10 mrg-top-20">
                                <b class="text-dark">Company Name</b><br>
                                <span>Company building</span><br>
                                <span>Company city & country</span><br>
                                <span><span class="text-bold">Contact:</span> 0123 4567 899</span><br>
                            </address>
                        </div> --}}
                    </div>

                    <div class="row mrg-top-20">
                        <div class="col-md-6 col-sm-6 text-right">
                            <h3 class="pdd-left-10 mrg-top-10 text-bold">{{ __('QUOTATION FROM') }}</h3>
                            <address class="pdd-left-10 mrg-top-20">
                                <b class="text-dark">{{ $invoice->company->name }}</b><br>
                                <span>{{ $invoice->company->city.", ".$invoice->company->state }}</span><br>
                                <span>{{ $invoice->company->address_1 }}</span><br>
                                @if($invoice->user != null)
                                    <span><span class="text-bold">Email: </span>{{ $invoice->user->email }}</span><br>
                                @endif
                            </address>
                        </div>

                        <div class="col-md-6 col-sm-6 text-right">
                            <h3 class="pdd-left-10 mrg-top-10 text-bold">{{ __('QUOTATION TO') }}</h3>
                            <address class="pdd-left-10 mrg-top-20">
                                <b class="text-dark">{{ $invoice->customer->name }}</b><br>
                                <span>{{ $invoice->customer->billing_city.", ".$invoice->customer->billing_state }}</span><br>
                                <span>{{ $invoice->customer->billing_address_1 }}</span><br>
                                <span><span class="text-bold">Contact:</span> {{ $invoice->customer->mobile_number }}</span><br>
                                <span><span class="text-bold">Email:</span> {{ ($invoice->client_email != $invoice->customer->email) ? $invoice->client_email : $invoice->customer->email }}</span><br>
                            </address>
                        </div>
                    </div>
                    <div class="row mrg-top-20">
                        <div class="col-md-12">
                            <table class="table table-hover table-responsive">
                                <thead>
                                    <tr>
                                        <th>{{ __('Image') }}</th>
                                        <th>{{ __('Items') }}</th>
                                        <th>{{ __('Description') }}</th>
                                        <th>{{ __('Quantity') }}</th>
                                        <th>{{ __('Price') }}</th>
                                        <th>{{ __('Discount') }}</th>
                                        <th class="text-right">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $sub_total = 0;
                                    @endphp
                                    @foreach($invoice->items as $list => $item)
                                        @if($item->item == null)
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td>{{ __('Item not found') }}</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            @php
                                                continue;
                                            @endphp
                                        @endif


                                        <tr>
                                            <td class="clickable-row" data-href="{{ $item->item->inventory_url }}" data-target="_blank">
                                                <div class="item-image-div">
                                                    <img src="{{ $item->item->avatar }}" alt="Image" class="img-responsive img-thumbnail item-image">
                                                </div>
                                            </td>
                                            @php
                                                $price = $item->custom_price*$item->quantity;
                                                $dis_price = $price - (($price*$item->discount)/100);

                                                $sub_total += $dis_price;
                                            @endphp
                                            <td class="clickable-row" data-href="{{ $item->item->inventory_url }}" data-target="_blank">{{ $item->item->name }}</td>
                                            <td class="clickable-row" data-href="{{ $item->item->inventory_url }}" data-target="_blank">{{ $item->item->description }}</td>
                                            <td class="clickable-row" data-href="{{ $item->item->inventory_url }}" data-target="_blank">{{ $item->quantity }}</td>
                                            <td class="clickable-row" data-href="{{ $item->item->inventory_url }}" data-target="_blank"><span class="selected_currency">{{ $invoice->currency->sign }}</span>{{ round($price, 2) }}</td>
                                            <td class="clickable-row" data-href="{{ $item->item->inventory_url }}" data-target="_blank">{{ $item->discount }}%</td>
                                            <td class="text-right clickable-row" data-href="{{ $item->item->inventory_url }}" data-target="_blank">{{ $invoice->currency->sign }}{{ $dis_price }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="row mrg-top-30 justify-content-end">
                                <div class="col-md-5">
                                    <hr>
                                    <table class="table table-borderless table-responsive">
                                        <tbody>
                                            <tr>
                                                <th class="text-right"><b>{{ __('Sub Total:') }}</b></th>
                                                <td class="text-right"><b><span class="selected_currency">{{ $invoice->currency->sign }}{{ $sub_total }}</span><span id="invoice_sub_total"></span></b></td>
                                            </tr>
                                            @php
                                                $overall_discount = (($sub_total*$invoice->discount)/100);
                                                $sub_total = $sub_total - $overall_discount;
                                                $total_tax = (($sub_total*$invoice->tax)/100);
                                                $total_payable = $sub_total + $total_tax;
                                            @endphp
                                            <tr>
                                                <th class="text-right"><b>{{ __('Overall Discount:') }}</b></th>
                                                <td class="text-right"><b><span id="invoice_total_discount"></span>{{ $invoice->currency->sign }}{{ round($overall_discount, 2) }}<sup>({{ ($invoice->discount != null)? $invoice->discount:0 }}%)</sup></b></td>
                                            </tr>
                                            <tr>
                                                <th class="text-right"><b>{{ __('Tax:') }}</b></th>
                                                <td class="text-right"><b><span id="invoice_total_tax"></span>{{ $invoice->currency->sign }}{{ round($total_tax, 2) }}<sup>({{ ($invoice->tax != null)? $invoice->tax:0 }}%)</sup></b></td>
                                            </tr>
                                            <tr class="text-primary">
                                                <th class="text-right border-top-1">
                                                    <b>{{ __('Grand Total (Incl.Tax)') }}</b>
                                                </th>
                                                <td class="text-right border-top-1"><b><span class="selected_currency">{{ $invoice->currency->sign }}{{ round($total_payable, 2) }}</span><span id="invoice_grand_total"></span></b></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <hr>
                                </div>
                            </div>

                            {{-- Payment Method --}}
                            <div class="row mrg-top-30">
                                <div class="col-lg-6 col-md-6 text-right border right">
                                    <hr>
                                    <div class="pdd-vertical-20">
                                        <h4 class="text-info text-bold">{{ __('Client Notes') }}</h4>
                                        <p class="text-dark">{!! $invoice->client_note !!}</p>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 text-right">
                                    <hr>
                                    <div class="pdd-vertical-20">
                                        <h4 class="text-info text-bold">{{ __('Payment Terms') }}</h4>
                                        <p class="text-dark">{!! $invoice->payment_term !!}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('script_links')
    {{--  External Javascript Links --}}

@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
        $(document).ready(function($) {
            $(".clickable-row").click(function() {
                // window.location = $(this).data("href");
                window.open($(this).attr('data-href'), $(this).attr('data-target'));
            });
        });
    </script>
@endsection
