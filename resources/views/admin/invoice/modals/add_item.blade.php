{{-- Upload File Modal --}}
<div class="modal fade" id="add_item">
    <form action="#" method="get" id="add_item_from">
        @csrf
        <div class="modal-dialog" role="document">
        {{-- <div class="modal-dialog modal-sm" role="document"> --}}
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="float-left"><b><span id="social_name">{{ __('Add New Item') }}</span></b></h4>
                    <button class="btn btn-default btn-icon btn-rounded p-l-8 p-r-7 p-t-6 p-b-4" data-dismiss="modal">
                        <i class="ti-close"></i>
                    </button>
                </div>
                <div class="modal-body media-details-modal">

                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label>{{ __('Inventory Item') }} <span class="required">*</span></label>
                                <div class="mrg-top-0">
                                    <input type="hidden" id="new_item_name" name="item_name">
                                    <select class="selectize-group" name="item_id" id="new_item_id" required onchange="changedItem(this)">
                                        <option value="" disabled selected>{{ __('Select Inventory Item') }}</option>
                                        @foreach($inventories as $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label class="text-bold">{{ __('Quantity') }} <span class="required">*</span></label>
                                <input autocomplete="off" type="number" id="item_quantity" min="1" name="quantity" class="form-control" placeholder="02" required>
                                <small>Quantity Between <span class="text-info text-bold">Minimum : 01</span> & <span class="text-danger text-bold">Maximum : <span id="max_quantity"></span></span></small>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label class="text-bold">{{ __('Original Price') }} <span class="required">*</span></label>
                                <input autocomplete="off" type="number" step="0.1" min="0" id="original_price" name="original_price" class="form-control" placeholder="500" required readonly>
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <input type="hidden" name="item_image" id="item_image">
                                <input type="hidden" name="item_description" id="item_description">
                                <label class="text-bold">{{ __('Custom Price') }}</label>
                                <input autocomplete="off" type="number" step="0.1" min="0" id="item_custom_price" name="custom_price" class="form-control" placeholder="400" value="0">
                            </div>
                        </div>

                        {{-- <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label class="text-bold">{{ __('Tax') }}<sup>(%)</sup></label>
                                <input autocomplete="off" type="number" step="0.1" min="0" max="100" id="new_item_tax" name="tax" class="form-control" placeholder="10%" value="0">
                                <small>{{ __('Tax Value in') }} <span class="text-info text-bold">{{ __('Percentage (%)') }}</span></small>
                            </div>
                        </div> --}}

                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label class="text-bold">{{ __('Discount') }}<sup>(%)</sup></label>
                                <input autocomplete="off" type="number" step="0.1" min="0" max="100" id="new_item_discount" name="custom_price" class="form-control" placeholder="10%" value="0">
                                <small>{{ __('Discount Value in') }} <span class="text-info text-bold">{{ __('Percentage (%)') }}</span></small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer border top">
                    <ul class="list-unstyled list-inline pull-right">
                        <li class="list-inline-item">
                            <button type="submit" id="new_item_submit" class="btn btn-dark btn-sm text-bold">
                                <i class="ti-save"></i>
                                {{ __('Add Item') }}
                            </button>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </form>
</div>
