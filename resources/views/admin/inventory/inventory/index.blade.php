@extends('layouts.admin.app')

@section('page_title', 'INVENTORY | Inventory')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/selectize/dist/css/selectize.default.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/summernote/dist/summernote.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS Here */
        .custom-img-size{
            max-height: 50px;
        }
        /* .form-control{
            padding: 0.700rem 0.75rem;
        } */
        .selectize-input {
            padding: 0.54rem 0.75rem;
        }

        .responsive-image{
            border-radius: 5px;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
            height: 80px;
            width: 100%;
        }
        .card-block.task-file.p-0 {
            height: 80px;
        }
        .selectize-dropdown.single{
            z-index: 99999999;
        }
        .selectize-dropdown [data-selectable], .selectize-dropdown .optgroup-header {
            padding: 15px 8px !important;
        }
        .table > tbody > tr > td {
            vertical-align: middle !important;
        }
    </style>
@endsection

@section('main_content')
<div class="container-fluid">
    <div class="page-title">
        <h4>{{ __('Inventory List') }}</h4>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-heading">
                    <h4 class="card-title float-right">{{ __('All Inventories') }}</h4>

                    <a href="javascript:void(0);" data-toggle="modal" data-target="#add_inventory_item" class="btn btn-dark btn-sm float-left m-b-0">{{ __('Add Inventory Item') }}</a>
                </div>
                <hr class="m-t-0">
                <div class="card-block">
                    <div class="table-overflow">
                        <table id="dt-opt" class="table table-lg table-hover table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <th class="text-center">{{ __('Action') }}</th>
                                    <th class="text-center">{{ __('Added Date') }}</th>
                                    <th class="text-center">{{ __('Unit Cost') }}</th>
                                    <th class="text-center">{{ __('Quantity') }}</th>
                                    <th class="text-center">{{ __('Category') }}</th>
                                    <th class="text-center">{{ __('Item Name') }}</th>
                                    <th class="text-center">{{ __('Item Image') }}</th>
                                    <th class="text-center">{{ __('ID') }}</th>
                                    <th class="text-center">#</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($inventories as $item => $inventory)
                                <tr>
                                    <td class="text-center">
                                        <div class="m-t-15 dropdown">
                                            <a href="#" class="btn btn-icon btn-flat btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="ti-more"></i></a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="{{ route('admin.inventory.inventory.show', ['id' => encrypt($inventory->id)]) }}">
                                                        <i class="ti-eye pdd-right-10 text-info"></i>
                                                        <span>{{ __('Details') }}</span>
                                                    </a>
                                                </li>
                                                {{-- <li>
                                                    <a href="{{ route('admin.inventory.inventory.delete', ['id' => encrypt($inventory->id)]) }}" class="confirmation">
                                                        <i class="ti-close pdd-right-10 text-danger"></i>
                                                        <span>{{ __('Delete') }}</span>
                                                    </a>
                                                </li> --}}
                                            </ul>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="m-t-15">
                                            <span class="text-dark">
                                                @php
                                                    $date = new DateTime($inventory->created_at);
                                                @endphp
                                                <b>{{ $date->format('d M Y') }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="m-t-15">
                                            <span class="text-dark">
                                                <b>@if($currency != null){{  $currency->sign." " }}@endif{{ $inventory->default_selling_price }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="m-t-15">
                                            <span class="text-dark">
                                                <b>{{ ($inventory->global_quantity != null)?$inventory->global_quantity:'-' }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="m-t-15">
                                            <span class="text-dark">
                                                <b>{{ $inventory->category->name }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="m-t-15">
                                            <span class="text-dark">
                                                <a href="{{ route('admin.inventory.inventory.show', ['id' => encrypt($inventory->id)]) }}"><b>{{ $inventory->name }}</b></a>
                                            </span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="m-t-2">
                                            <span class="text-dark">
                                                <a href="{{ route('admin.inventory.inventory.show', ['id' => encrypt($inventory->id)]) }}">
                                                <img src="{{ $inventory->avatar }}" alt="" class="img-responsive img-thumnail custom-img-size">
                                                </a>
                                            </span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="m-t-15">
                                            <span class="text-dark">
                                                @php
                                                    $gen_id = new DateTime($inventory->created_at);
                                                @endphp
                                                <a href="{{ route('admin.inventory.inventory.show', ['id' => encrypt($inventory->id)]) }}"><b>{{ $gen_id->format('Ymd')."-".$inventory->id }}</b></a>
                                            </span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="m-t-15">
                                            <span class="text-dark">
                                                <b>{{ $item+1 }}</b>
                                            </span>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('admin.inventory.modals.add_inventory')
</div>
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>

    <script src="{{ asset('assets/vendors/selectize/dist/js/standalone/selectize.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('assets/js/forms/form-elements.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    // $('#img-preview').css('background-image', 'url('+e.target.result +')');
                    $('#img-preview').attr('src', e.target.result);
                    $('#img-preview').hide();
                    $('#img-preview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#img-upload").change(function() {
            readURL(this);
        });

        $('.selectize-group').selectize({
            sortField: 'text'
        });

        $('#quantity_inventory_item_tab').click(function(){
            $('#quantity_inventory_item').val(1);
            $('#unique_inventory_item').val(0);
        });
        $('#unique_inventory_item_tab').click(function(){
            $('#quantity_inventory_item').val(0);
            $('#unique_inventory_item').val(1);
        });
    </script>
@endsection
