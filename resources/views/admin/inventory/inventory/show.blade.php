@extends('layouts.admin.app')

@section('page_title', 'Inventory Item Details')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/selectize/dist/css/selectize.default.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/summernote/dist/summernote.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS Here */
        .form-control {
            padding: 0.700rem 0.75rem;
        }
        .admin-profile-picture{
            max-width: 125px;
        }
        .form-control:disabled, .form-control[readonly] {
            background-color: #ffffff;
            opacity: 1;
            border-color: #fbfbfb;
        }
    </style>
@endsection

@section('main_content')
<div class="container-fluid">
    <div class="page-title">
        <h4>{{ $inventory->name }}</h4>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-heading border bottom">
                            <h4 class="card-title">{{ __('Item Image') }}</h4>
                        </div>
                        <div class="card-block">
                            <div class="row">
                                <div class="col-md-12">
                                    @if($inventory->avatar != null)
                                        <img class="img-responsive img-thumbnail" src="{{ $inventory->avatar }}" alt="">
                                    @else
                                        <img class="img-responsive img-thumbnail" src="{{ asset('assets/images/others/img-10.jpg') }}" alt="">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- update profile form start --}}
                <div class="col-md-8">
                    <form action="{{ route('admin.inventory.inventory.update', ['id' => encrypt($inventory->id)]) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="card">
                            <div class="card-heading border bottom">
                                <h4 class="card-title">{{ __('Inventory Item Details') }}</h4>
                            </div>
                            <div class="card-block">
                                <div class="row justify-content-center m-b-20">
                                    <div class="col-lg-4 text-center">
                                        <label for="img-upload" class="pointer">
                                            @if($inventory->avatar != null)
                                                <img id="img-preview" src="{{ $inventory->avatar }}"  width="117" alt="">
                                            @else
                                                <img id="img-preview" src="{{ asset('assets/images/others/img-10.jpg') }}"  width="117" alt="">
                                            @endif
                                            <span class="btn btn-default display-block no-mrg-btm">{{ __('Inventory Item Photo') }}</span>
                                            <input class="d-none @error('avatar') is-invalid @enderror removeDis" disabled type="file" name="avatar" accept=".png, .jpg, .jpeg" id="img-upload" value="{{ old('avatar') }}">
                                        </label>
                                        @error('avatar')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="form-group">
                                            <label>{{ __('Item Name') }} <span class="required">*</span></label>
                                            <input autocomplete="off" type="text" name="name" class="form-control @error('name') is-invalid @enderror removeDis" disabled value="{{ $inventory->name }}" placeholder="{{ __('Item Name') }}" required>

                                            @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-group">
                                            <label>{{ __('Select Category') }} <span class="required">*</span></label>
                                            <div class="mrg-top-0">
                                                <select class="selectize-group @error('category_id') is-invalid @enderror" name="category_id" required>
                                                    <option value="" disabled>{{ __('Select Category') }}</option>
                                                    @foreach($categories as $cat)
                                                        <option value="{{ $cat->id }}" @if($inventory->category_id == $cat->id)selected @endif>{{ $cat->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            @error('category_id')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-group">
                                            <label>{{ __('Purchased Date') }}</label>
                                            <div class="timepicker-input input-icon form-group">
                                                <i class="ti-time"></i>
                                                @php
                                                    $date = new DateTime($inventory->purchased_date);
                                                @endphp
                                                <input type="text" value="{{ $date->format('m/d/Y') }}" name="purchased_date" class="form-control datepicker-1  @error('purchased_date') is-invalid @enderror removeDis" disabled placeholder="mm/dd/yyyy" data-provide="datepicker">

                                                @error('purchased_date')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-12">
                                        <div class="form-group">
                                            <label>{{ __('Description') }}</label>
                                            <textarea autocomplete="off" type="text" name="description" class="form-control @error('description') is-invalid @enderror removeDis" disabled placeholder="{{ __('Description') }}" rows="3">{{ $inventory->description }}</textarea>

                                            @error('description')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-12">
                                        <div class="form-group">
                                            <label>{{ __('Inventory Url') }}</label>
                                            <input type="url" autocomplete="off" name="inventory_url" class="form-control @error('inventory_url') is-invalid @enderror removeDis" disabled placeholder="{{ __('Inventory Url') }}" value="{{ $inventory->inventory_url }}">
            
                                            @error('inventory_url')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="card">
                                            <div class="padding-10 text-center border bottom">
                                                <h4 class="card-title no-mrg-btm text-bold text-center">
                                                    @if($inventory->inventory_type == 'quantity_inventory_item')
                                                        {{ __('Quantity Inventory Item') }}
                                                    @else
                                                        {{ __('Unique Inventory Item') }}
                                                    @endif
                                                </h4>
                                            </div>

                                            <div class="card-block">
                                                @if($inventory->inventory_type == 'quantity_inventory_item')
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6">
                                                            <div class="form-group">
                                                                <label>{{ __('Purchase Price per Unit') }} <span class="required">*</span></label>
                                                                <input autocomplete="off" type="number" name="quantity_price_per_unit" class="form-control @error('quantity_price_per_unit') is-invalid @enderror removeDis" disabled value="{{ $inventory->purchase_price_per_unit }}" placeholder="{{ __('Purchase Price per Unit') }}" min="1">

                                                                @error('quantity_price_per_unit')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-6 col-md-6">
                                                            <div class="form-group">
                                                                <label>{{ __('Default Selling Price') }} <span class="required">*</span></label>
                                                                <input autocomplete="off" type="number" name="quantity_default_selling_price" class="form-control @error('quantity_default_selling_price') is-invalid @enderror removeDis" disabled value="{{ $inventory->default_selling_price }}" placeholder="{{ __('Default Selling Price') }}" min="1">

                                                                @error('quantity_default_selling_price')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-6 col-md-6">
                                                            <div class="form-group">
                                                                <label>{{ __('SKU') }}</label>
                                                                <input autocomplete="off" type="text" name="quantity_sku" class="form-control @error('quantity_sku') is-invalid @enderror removeDis" disabled value="{{ $inventory->sku }}" placeholder="{{ __('SKU') }}" min="1">

                                                                @error('quantity_sku')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-6 col-md-6">
                                                            <div class="form-group">
                                                                <label>{{ __('Global Quantity') }}</label>
                                                                <input autocomplete="off" type="number" name="quantity_global_quantity" class="form-control @error('quantity_global_quantity') is-invalid @enderror removeDis" disabled value="{{ $inventory->global_quantity }}" placeholder="{{ __('Global Quantity') }}" min="1">

                                                                @error('quantity_global_quantity')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-6 col-md-6">
                                                            <div class="form-group">
                                                                <label>{{ __('Low Quantity') }}</label>
                                                                <input autocomplete="off" type="number" name="quantity_low_quantity" class="form-control @error('quantity_low_quantity') is-invalid @enderror removeDis" disabled value="{{ $inventory->low_quantity }}" placeholder="{{ __('Low Quantity') }}" min="1">

                                                                @error('quantity_low_quantity')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-6 col-md-6">
                                                            <div class="form-group">
                                                                <label>{{ __('Unit Of Measure') }}</label>
                                                                <div class="mrg-top-0">
                                                                    <select class="selectize-group @error('quantity_unit_id') is-invalid @enderror" name="quantity_unit_id">
                                                                        <option value="" disabled>{{ __('Select Unit Of Measure') }}</option>
                                                                        @foreach($units as $unit)
                                                                            <option value="{{ $unit->id }}" @if($inventory->unit_id == $unit->id)selected @endif>{{ $unit->name." (".$unit->short_form.")" }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>

                                                                @error('quantity_unit_id')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                        {{-- <div class="col-lg-6 col-md-6">
                                                            <div class="form-group">
                                                                <label>{{ __('Sorting Priority') }}</label>
                                                                <div class="mrg-top-0">
                                                                    <select class="selectize-group @error('quantity_shorting_priority') is-invalid @enderror" name="quantity_shorting_priority">
                                                                        <option value="" disabled>{{ __('Select Sorting Priority') }}</option>
                                                                        <option value="Highest" @if($inventory->sort_priority == 'Highest') selected @endif>Highest</option>
                                                                        <option value="High" @if($inventory->sort_priority == 'High') selected @endif>High</option>
                                                                        <option value="Normal" @if($inventory->sort_priority == 'Normal') selected @endif>Normal</option>
                                                                        <option value="Low" @if($inventory->sort_priority == 'Low') selected @endif>Low</option>
                                                                        <option value="Lowest" @if($inventory->sort_priority == 'Lowest') selected @endif>Lowest</option>
                                                                    </select>
                                                                </div>

                                                                @error('quantity_shorting_priority')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                            </div>
                                                        </div> --}}
                                                    </div>
                                                @else
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6">
                                                            <div class="form-group">
                                                                <label>{{ __('Purchase Price') }} <span class="required">*</span></label>
                                                                <input autocomplete="off" type="number" name="unique_purchase_price" class="form-control @error('unique_purchase_price') is-invalid @enderror removeDis" disabled value="{{ $inventory->purchase_price }}" placeholder="{{ __('Purchase Price') }}" min="1">

                                                                @error('unique_purchase_price')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-6 col-md-6">
                                                            <div class="form-group">
                                                                <label>{{ __('Default Selling Price') }} <span class="required">*</span></label>
                                                                <input autocomplete="off" type="number" name="unique_default_selling_price" class="form-control @error('unique_default_selling_price') is-invalid @enderror removeDis" disabled value="{{ $inventory->default_selling_price }}" placeholder="{{ __('Default Selling Price') }}" min="1">

                                                                @error('unique_default_selling_price')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-6 col-md-6">
                                                            <div class="form-group">
                                                                <label>{{ __('SKU') }}</label>
                                                                <input autocomplete="off" type="text" name="unique_sku" class="form-control @error('unique_sku') is-invalid @enderror removeDis" disabled value="{{ $inventory->sku }}" placeholder="{{ __('SKU') }}" min="1">

                                                                @error('unique_sku')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-6 col-md-6">
                                                            <div class="form-group">
                                                                <label>{{ __('Serial Number') }}</label>
                                                                <input autocomplete="off" type="text" name="unique_serial_number" class="form-control @error('unique_serial_number') is-invalid @enderror removeDis" disabled value="{{ $inventory->serial_number }}" placeholder="{{ __('Serial Number') }}" min="1">

                                                                @error('unique_serial_number')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-6 col-md-6">
                                                            <div class="form-group">
                                                                <label>{{ __('Unit Of Measure') }}</label>
                                                                <div class="mrg-top-0">
                                                                    <select class="selectize-group @error('unique_unit_id') is-invalid @enderror" name="unique_unit_id">
                                                                        <option value="" disabled>{{ __('Select Unit Of Measure') }}</option>
                                                                        @foreach($units as $unit)
                                                                            <option value="{{ $unit->id }}" @if($inventory->unit_id == $unit->id)selected @endif>{{ $unit->name." (".$unit->short_form.")" }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>

                                                                @error('unique_unit_id')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                        {{-- <div class="col-lg-6 col-md-6">
                                                            <div class="form-group">
                                                                <label>{{ __('Sorting Priority') }}</label>
                                                                <div class="mrg-top-0">
                                                                    <select class="selectize-group @error('unique_shorting_priority') is-invalid @enderror" name="unique_shorting_priority">
                                                                        <option value="" disabled>{{ __('Select Sorting Priority') }}</option>
                                                                        <option value="Highest" @if($inventory->sort_priority == 'Highest') selected @endif>Highest</option>
                                                                        <option value="High" @if($inventory->sort_priority == 'High') selected @endif>High</option>
                                                                        <option value="Normal" @if($inventory->sort_priority == 'Normal') selected @endif>Normal</option>
                                                                        <option value="Low" @if($inventory->sort_priority == 'Low') selected @endif>Low</option>
                                                                        <option value="Lowest" @if($inventory->sort_priority == 'Lowest') selected @endif>Lowest</option>
                                                                    </select>
                                                                </div>

                                                                @error('unique_shorting_priority')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                            </div>
                                                        </div> --}}
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer border top">
                                <div class="text-right">
                                    <a href="javascript:void(0);" class="btn btn-dark btn-sm" id="removeDisabled">
                                        {{ __('Edit') }}
                                    </a>
                                    <a href="javascript:void(0);" class="btn btn-default btn-sm hiddenButton d-none" id="addDisabled">
                                        {{ __('Cancel') }}
                                    </a>
                                    <button type="submit" class="btn btn-dark btn-sm hiddenButton d-none">
                                        {{ __('Update') }}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                {{-- Update Information form close --}}
            </div>
        </div>
    </div>
</div>
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/selectize/dist/js/standalone/selectize.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('assets/js/forms/form-elements.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        $("#removeDisabled").click(function(){
            $('.removeDis').removeAttr('disabled', 'disabled');
            $('.removeDis').removeClass('disabled');
            $('#removeDisabled').addClass('d-none');
            $('.hiddenButton').removeClass('d-none');        
        });
        
        $("#addDisabled").click(function(){
            $('.removeDis').attr('disabled', 'disabled');
            $('.removeDis').addClass('disabled');
            $('#removeDisabled').removeClass('d-none');
            $('.hiddenButton').addClass('d-none');        
        });
    </script>





    <script>
        // Custom Script Here
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    // $('#img-preview').css('background-image', 'url('+e.target.result +')');
                    $('#img-preview').attr('src', e.target.result);
                    $('#img-preview').hide();
                    $('#img-preview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#img-upload").change(function() {
            readURL(this);
        });

        $('.selectize-group').selectize({
            sortField: 'text'
        });

        $('#quantity_inventory_item_tab').click(function(){
            console.log('this');
            $('#quantity_inventory_item').val(1);
            $('#unique_inventory_item').val(0);
        });
        $('#unique_inventory_item_tab').click(function(){
            $('#quantity_inventory_item').val(0);
            $('#unique_inventory_item').val(1);
        });
    </script>
@endsection
