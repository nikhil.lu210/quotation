{{-- Upload File Modal --}}
<div class="modal fade" id="add_inventory_item">
    <form action="{{ route('admin.inventory.inventory.store') }}" method="post" id="add_inventory_item_from" enctype="multipart/form-data">
        @csrf
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="float-left"><b><span id="social_name">{{ __('Add Inventory Item') }}</span></b></h4>
                    <button class="btn btn-default btn-icon btn-rounded p-l-8 p-r-7 p-t-6 p-b-4" data-dismiss="modal">
                        <i class="ti-close"></i>
                    </button>
                </div>
                <div class="modal-body media-details-modal">
                    <div class="row justify-content-center m-b-20">
                        <div class="col-lg-4 text-center">
                            <label for="img-upload" class="pointer">
                                <img id="img-preview" src="{{ asset('assets/images/others/img-10.jpg') }}"  width="117" alt="">

                                <span class="btn btn-default display-block no-mrg-btm">{{ __('Inventory Item Photo') }}</span>
                                <input class="d-none @error('avatar') is-invalid @enderror" type="file" name="avatar" accept=".png, .jpg, .jpeg" id="img-upload" value="{{ old('avatar') }}">
                            </label>
                            @error('avatar')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label>{{ __('Item Name') }} <span class="required">*</span></label>
                                <input autocomplete="off" type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}" placeholder="{{ __('Item Name') }}" required>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label>{{ __('Select Category') }} <span class="required">*</span></label>
                                <div class="mrg-top-0">
                                    <select class="selectize-group @error('category_id') is-invalid @enderror" name="category_id" required>
                                        <option value="" disabled selected>{{ __('Select Category') }}</option>
                                        @foreach($categories as $cat)
                                        <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                @error('category_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label>{{ __('Purchased Date') }}</label>
                                <div class="timepicker-input input-icon form-group">
                                    <i class="ti-time"></i>
                                    <input type="text" value="{{ old('purchased_date') }}" name="purchased_date" class="form-control datepicker-1" placeholder="mm/dd/yyyy" data-provide="datepicker">

                                    @error('purchased_date')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label>{{ __('Description') }}</label>
                                <textarea autocomplete="off" type="text" name="description" class="form-control @error('description') is-invalid @enderror" placeholder="{{ __('Description') }}" rows="3">{{ old('description') }}</textarea>

                                @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label>{{ __('Inventory Url') }}</label>
                                <input type="url" autocomplete="off" name="inventory_url" class="form-control @error('inventory_url') is-invalid @enderror" placeholder="{{ __('Inventory Url') }}" value="{{ old('inventory_url') }}">

                                @error('inventory_url')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="padding-10 text-center border bottom">
                                    <h4 class="card-title no-mrg-btm text-bold text-center">{{ __('Inventory Type') }}</h4>
                                </div>
                                <div class="tab-info center-tabs ">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item border right">
                                            <a href="#quantity_inventory_item_tab" class="nav-link active" role="tab" data-toggle="tab">{{ __('Quantity Inventory Item') }}</a>
                                            <input type="hidden" name="quantity_inventory_item" id="quantity_inventory_item" value="1">
                                        </li>
                                        <li class="nav-item">
                                            <a href="#unique_inventory_item_tab" class="nav-link" role="tab" data-toggle="tab">{{ __('Unique Inventory Item') }}</a>
                                            <input type="hidden" name="unique_inventory_item" id="unique_inventory_item" value="0">
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane fade in active" id="quantity_inventory_item_tab">
                                            <div class="pdd-horizon-15 pdd-vertical-20">
                                                <div class="row">
                                                    <div class="col-lg-6 col-md-6">
                                                        <div class="form-group">
                                                            <label>{{ __('Purchase Price per Unit') }} <span class="required">*</span></label>
                                                            <input autocomplete="off" type="number" name="quantity_price_per_unit" class="form-control @error('quantity_price_per_unit') is-invalid @enderror" value="{{ old('quantity_price_per_unit') }}" placeholder="{{ __('Purchase Price per Unit') }}" min="1">

                                                            @error('quantity_price_per_unit')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-6 col-md-6">
                                                        <div class="form-group">
                                                            <label>{{ __('Default Selling Price') }} <span class="required">*</span></label>
                                                            <input autocomplete="off" type="number" name="quantity_default_selling_price" class="form-control @error('quantity_default_selling_price') is-invalid @enderror" value="{{ old('quantity_default_selling_price') }}" placeholder="{{ __('Default Selling Price') }}" min="1">

                                                            @error('quantity_default_selling_price')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-6 col-md-6">
                                                        <div class="form-group">
                                                            <label>{{ __('SKU') }}</label>
                                                            <input autocomplete="off" type="text" name="quantity_sku" class="form-control @error('quantity_sku') is-invalid @enderror" value="{{ old('quantity_sku') }}" placeholder="{{ __('SKU') }}" min="1">

                                                            @error('quantity_sku')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-6 col-md-6">
                                                        <div class="form-group">
                                                            <label>{{ __('Global Quantity') }}</label>
                                                            <input autocomplete="off" type="number" name="quantity_global_quantity" class="form-control @error('quantity_global_quantity') is-invalid @enderror" value="{{ old('quantity_global_quantity') }}" placeholder="{{ __('Global Quantity') }}" min="1">

                                                            @error('quantity_global_quantity')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-6 col-md-6">
                                                        <div class="form-group">
                                                            <label>{{ __('Low Quantity') }}</label>
                                                            <input autocomplete="off" type="number" name="quantity_low_quantity" class="form-control @error('quantity_low_quantity') is-invalid @enderror" value="{{ old('quantity_low_quantity') }}" placeholder="{{ __('Low Quantity') }}" min="1">

                                                            @error('quantity_low_quantity')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-6 col-md-6">
                                                        <div class="form-group">
                                                            <label>{{ __('Unit Of Measure') }}</label>
                                                            <div class="mrg-top-0">
                                                                <select class="selectize-group @error('quantity_unit_id') is-invalid @enderror" name="quantity_unit_id">
                                                                    <option value="" disabled selected>{{ __('Select Unit Of Measure') }}</option>
                                                                    @foreach($units as $unit)
                                                                        <option value="{{ $unit->id }}">{{ $unit->name." (".$unit->short_form.")" }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>

                                                            @error('quantity_unit_id')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    {{-- <div class="col-lg-6 col-md-6">
                                                        <div class="form-group">
                                                            <label>{{ __('Sorting Priority') }}</label>
                                                            <div class="mrg-top-0">
                                                                <select class="selectize-group @error('quantity_shorting_priority') is-invalid @enderror" name="quantity_shorting_priority">
                                                                    <option value="" disabled selected>{{ __('Select Sorting Priority') }}</option>
                                                                    <option value="Highest">Highest</option>
                                                                    <option value="High">High</option>
                                                                    <option value="Normal">Normal</option>
                                                                    <option value="Low">Low</option>
                                                                    <option value="Lowest">Lowest</option>
                                                                </select>
                                                            </div>

                                                            @error('quantity_shorting_priority')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div> --}}
                                                </div>
                                            </div>
                                        </div>

                                        <div role="tabpanel" class="tab-pane fade" id="unique_inventory_item_tab">
                                            <div class="pdd-horizon-15 pdd-vertical-20">
                                                <div class="row">
                                                    <div class="col-lg-6 col-md-6">
                                                        <div class="form-group">
                                                            <label>{{ __('Purchase Price') }} <span class="required">*</span></label>
                                                            <input autocomplete="off" type="number" name="unique_purchase_price" class="form-control @error('unique_purchase_price') is-invalid @enderror" value="{{ old('unique_purchase_price') }}" placeholder="{{ __('Purchase Price') }}" min="1">

                                                            @error('unique_purchase_price')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-6 col-md-6">
                                                        <div class="form-group">
                                                            <label>{{ __('Default Selling Price') }} <span class="required">*</span></label>
                                                            <input autocomplete="off" type="number" name="unique_default_selling_price" class="form-control @error('unique_default_selling_price') is-invalid @enderror" value="{{ old('unique_default_selling_price') }}" placeholder="{{ __('Default Selling Price') }}" min="1">

                                                            @error('unique_default_selling_price')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-6 col-md-6">
                                                        <div class="form-group">
                                                            <label>{{ __('SKU') }}</label>
                                                            <input autocomplete="off" type="text" name="unique_sku" class="form-control @error('unique_sku') is-invalid @enderror" value="{{ old('unique_sku') }}" placeholder="{{ __('SKU') }}" min="1">

                                                            @error('unique_sku')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-6 col-md-6">
                                                        <div class="form-group">
                                                            <label>{{ __('Serial Number') }}</label>
                                                            <input autocomplete="off" type="text" name="unique_serial_number" class="form-control @error('unique_serial_number') is-invalid @enderror" value="{{ old('unique_serial_number') }}" placeholder="{{ __('Serial Number') }}" min="1">

                                                            @error('unique_serial_number')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-6 col-md-6">
                                                        <div class="form-group">
                                                            <label>{{ __('Unit Of Measure') }}</label>
                                                            <div class="mrg-top-0">
                                                                <select class="selectize-group @error('unique_unit_id') is-invalid @enderror" name="unique_unit_id">
                                                                    <option value="" disabled selected>{{ __('Select Unit Of Measure') }}</option>
                                                                    @foreach($units as $unit)
                                                                        <option value="{{ $unit->id }}">{{ $unit->name." (".$unit->short_form.")" }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>

                                                            @error('unique_unit_id')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    {{-- <div class="col-lg-6 col-md-6">
                                                        <div class="form-group">
                                                            <label>{{ __('Sorting Priority') }}</label>
                                                            <div class="mrg-top-0">
                                                                <select class="selectize-group @error('unique_shorting_priority') is-invalid @enderror" name="unique_shorting_priority">
                                                                    <option value="" disabled selected>{{ __('Select Sorting Priority') }}</option>
                                                                    <option value="Highest">Highest</option>
                                                                    <option value="High">High</option>
                                                                    <option value="Normal">Normal</option>
                                                                    <option value="Low">Low</option>
                                                                    <option value="Lowest">Lowest</option>
                                                                </select>
                                                            </div>

                                                            @error('unique_shorting_priority')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div> --}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer border top">
                    <ul class="list-unstyled list-inline pull-right">
                        <li class="list-inline-item">
                            <button type="submit" id="new_item_submit" class="btn btn-dark btn-sm text-bold">
                                <i class="ti-save"></i>
                                {{ __('Add Item') }}
                            </button>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </form>
</div>
