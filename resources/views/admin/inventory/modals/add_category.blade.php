{{-- Upload File Modal --}}
<div class="modal fade" id="add_category_item">
    <form action="{{ route('admin.inventory.category.store') }}" method="post" id="add_category_item_from">
        @csrf
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="float-left"><b><span id="social_name">{{ __('Add New Category') }}</span></b></h4>
                    <button class="btn btn-default btn-icon btn-rounded p-l-8 p-r-7 p-t-6 p-b-4" data-dismiss="modal">
                        <i class="ti-close"></i>
                    </button>
                </div>
                <div class="modal-body media-details-modal">
                    <div class="row">                                        
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>{{ __('Category Name') }} <span class="required">*</span></label>
                                <input autocomplete="off" type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}" placeholder="{{ __('Category Name') }}">

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
        
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label>{{ __('Description') }}</label>
                                <textarea autocomplete="off" type="text" name="description" class="form-control @error('description') is-invalid @enderror" placeholder="{{ __('Description') }}" rows="3">{{ old('description') }}</textarea>

                                @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>{{ __('Inventory Type') }}</label>
                                <div class="mrg-top-0">
                                    <select class="selectize-group @error('inventory_type') is-invalid @enderror" name="inventory_type">
                                        <option value="" disabled selected>{{ __('Select Inventory Type') }}</option>
                                        <option value="Quantity Inventory Item">{{ __('Quantity Inventory Item') }}</option>
                                        <option value="Unique Inventory Item">{{ __('Unique Inventory Item') }}</option>
                                    </select>
                                </div>

                                @error('inventory_type')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>{{ __('Default Selling Price Markup') }} <sup>(in <b>%</b>)</sup></label>
                                <input autocomplete="off" type="number" name="default_selling_price_markup" class="form-control @error('default_selling_price_markup') is-invalid @enderror" value="{{ old('default_selling_price_markup') }}" placeholder="0.00" min="1">

                                @error('default_selling_price_markup')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer border top">
                    <ul class="list-unstyled list-inline pull-right">
                        <li class="list-inline-item">
                            <button type="submit" id="new_category_submit" class="btn btn-dark btn-sm text-bold">
                                <i class="ti-save"></i>
                                {{ __('Add Category') }}
                            </button>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </form>
</div>
