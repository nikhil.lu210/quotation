@extends('layouts.admin.app')

@section('page_title', 'ACCOUNTS | New Customer')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/selectize/dist/css/selectize.default.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/summernote/dist/summernote.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS Here */
        .form-control{
            padding: 0.700rem 0.75rem;
        }
        .selectize-input {
            padding: 0.640rem 0.75rem;
        }

        .responsive-image{
            border-radius: 5px;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
            height: 80px;
            width: 100%;
        }
        .card-block.task-file.p-0 {
            height: 80px;
        }
        .selectize-dropdown.single{
            z-index: 99999999;
        }
        .selectize-dropdown [data-selectable], .selectize-dropdown .optgroup-header {
            padding: 15px 8px !important;
        }
        a.deactive{
            cursor: pointer;
        }
        li.social-media-list{
            border: 0px !important;
            border-bottom: 1px solid #e6ecf5 !important;
            margin-bottom: 10px;
        }
        li.social-media-list:last-child{
            border: 0px !important;
            border-bottom: 0px solid #e6ecf5 !important;
            margin-bottom: 0px;
        }
    </style>
@endsection

@section('main_content')
<div class="container-fluid">
    <div class="page-title">
        <h4>{{ __('Add New Customer') }}</h4>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <form action="#" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card">
                    <div class="card-block p-25">
                        <div class="card">
                            <div class="card-heading">
                                <h4 class="card-title float-right text-bold">{{ __('Business Contact Information') }}</h4>
                            </div>
                            <div class="card-block p-25">
                                <div class="row justify-content-center m-b-20">
                                    <div class="col-lg-4 text-center">
                                        <div>
                                            <label for="img-upload" class="pointer">
                                                <img id="img-preview" src="{{ asset('assets/images/others/img-10.jpg') }}"  width="117" alt="">

                                                <span class="btn btn-default display-block no-mrg-btm">{{ __('Choose Profile Photo') }}</span>
                                                <input class="d-none @error('avatar') is-invalid @enderror" type="file" name="avatar" accept=".png, .jpg, .jpeg" id="img-upload" value="{{ old('avatar') }}">
                                            </label>
                                            @error('avatar')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>{{ __('Full Name') }} <span class="required">*</span></label>
                                            <input autocomplete="off" type="text" name="full_name" class="form-control @error('full_name') is-invalid @enderror" value="{{ old('full_name') }}" placeholder="{{ __('Full Name') }}" required>

                                            @error('full_name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>{{ __('Email') }}</label>
                                            <input autocomplete="off" type="email" name="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" placeholder="yourmail@mail.com">

                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>{{ __('Mobile Number') }}</label>
                                            <input autocomplete="off" type="text" name="mobile" class="form-control @error('mobile') is-invalid @enderror" value="{{ old('mobile') }}" placeholder="+44 0800 689 3176">

                                            @error('mobile')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>{{ __('Office Phone') }}</label>
                                            <input autocomplete="off" type="text" name="office_phone" class="form-control @error('office_phone') is-invalid @enderror" value="{{ old('office_phone') }}" placeholder="+44 0800 689 3176">

                                            @error('office_phone')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>{{ __('Website') }}</label>
                                            <input autocomplete="off" type="text" name="website" class="form-control @error('website') is-invalid @enderror" value="{{ old('website') }}" placeholder="www.customerwebsite.com">

                                            @error('website')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>{{ __('Select Account Manager') }}</label>
                                            <div class="mrg-top-0">
                                                <select class="@error('account_manager') is-invalid @enderror" name="account_manager" id="selectize-group" >
                                                    <option value="" disabled selected>{{ __('Select Account Manager') }}</option>
                                                    <option value="1">£ GBP</option>
                                                    <option value="2">$ USD</option>
                                                    <option value="3">৳ BDT</option>
                                                </select>
                                            </div>

                                            @error('account_manager')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="card">
                            <div class="card-heading">
                                <h4 class="card-title float-right text-bold">{{ __('Billing and Shipping Addresses') }}</h4>
                            </div>
                            <div class="card-block p-25">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>{{ __('Billing Address Line 1') }}</label>
                                                    <input autocomplete="off" type="text" name="billing_address_line_one" class="form-control @error('billing_address_line_one') is-invalid @enderror" value="{{ old('billing_address_line_one') }}" placeholder="{{ __('Billing Address Line 1') }}">

                                                    @error('billing_address_line_one')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>{{ __('Billing Address Line 2') }}</label>
                                                    <input autocomplete="off" type="text" name="billing_address_line_two" class="form-control @error('billing_address_line_two') is-invalid @enderror" value="{{ old('billing_address_line_two') }}" placeholder="{{ __('Billing Address Line 2') }}">

                                                    @error('billing_address_line_two')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>{{ __('City') }}</label>
                                                    <input autocomplete="off" type="text" name="billing_city" class="form-control @error('billing_city') is-invalid @enderror" value="{{ old('billing_city') }}" placeholder="{{ __('City') }}">

                                                    @error('billing_city')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>{{ __('State') }}</label>
                                                    <input autocomplete="off" type="text" name="billing_state" class="form-control @error('billing_state') is-invalid @enderror" value="{{ old('billing_state') }}" placeholder="{{ __('State') }}">

                                                    @error('billing_state')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>{{ __('Postal Code / Zip Code') }}</label>
                                                    <input autocomplete="off" type="text" name="billing_post" class="form-control @error('billing_post') is-invalid @enderror" value="{{ old('billing_post') }}" placeholder="{{ __('Postal Code / Zip Code') }}">

                                                    @error('billing_post')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>{{ __('Country') }}</label>
                                                    <input autocomplete="off" type="text" name="billing_country" class="form-control @error('billing_country') is-invalid @enderror" value="{{ old('billing_country') }}" placeholder="{{ __('Country') }}">

                                                    @error('billing_country')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>{{ __('Shipping Address Line 1') }}</label>
                                                    <input autocomplete="off" type="text" name="shipping_address_line_one" class="form-control @error('shipping_address_line_one') is-invalid @enderror" value="{{ old('shipping_address_line_one') }}" placeholder="{{ __('Shipping Address Line 1') }}">

                                                    @error('shipping_address_line_one')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>{{ __('Shipping Address Line 2') }}</label>
                                                    <input autocomplete="off" type="text" name="shipping_address_line_two" class="form-control @error('shipping_address_line_two') is-invalid @enderror" value="{{ old('shipping_address_line_two') }}" placeholder="{{ __('Shipping Address Line 2') }}">

                                                    @error('shipping_address_line_two')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>{{ __('City') }}</label>
                                                    <input autocomplete="off" type="text" name="shipping_city" class="form-control @error('shipping_city') is-invalid @enderror" value="{{ old('shipping_city') }}" placeholder="{{ __('City') }}">

                                                    @error('shipping_city')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>{{ __('State') }}</label>
                                                    <input autocomplete="off" type="text" name="shipping_state" class="form-control @error('shipping_state') is-invalid @enderror" value="{{ old('shipping_state') }}" placeholder="{{ __('State') }}">

                                                    @error('shipping_state')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>{{ __('Postal Code / Zip Code') }}</label>
                                                    <input autocomplete="off" type="text" name="shipping_post" class="form-control @error('shipping_post') is-invalid @enderror" value="{{ old('shipping_post') }}" placeholder="{{ __('Postal Code / Zip Code') }}">

                                                    @error('shipping_post')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>{{ __('Country') }}</label>
                                                    <input autocomplete="off" type="text" name="shipping_country" class="form-control @error('shipping_country') is-invalid @enderror" value="{{ old('shipping_country') }}" placeholder="{{ __('Country') }}">

                                                    @error('shipping_country')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer border top">
                        <ul class="list-unstyled list-inline pull-left">
                            <li class="list-inline-item">
                                <div class="checkbox checkbox-primary font-size-12">
                                    <input id="status" name="status" type="checkbox" checked required>
                                    <label for="status" class="m-b-0 text-bold text-primary">Activate This Business?</label>
                                </div>
                            </li>
                        </ul>
                        <ul class="list-unstyled list-inline float-right">
                            <li class="list-inline-item">
                                <button type="submit" class="btn btn-dark btn-sm text-bold">
                                    <i class="ti-save"></i>
                                    {{ __('Add Customer') }}
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
            </form>
        </div>
    </div>

    {{-- @include('superadmin.system.modals.team.update_modal') --}}
</div>
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>

    <script src="{{ asset('assets/vendors/selectize/dist/js/standalone/selectize.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('assets/js/forms/form-elements.js') }}"></script>

@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    // $('#img-preview').css('background-image', 'url('+e.target.result +')');
                    $('#img-preview').attr('src', e.target.result);
                    $('#img-preview').hide();
                    $('#img-preview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#img-upload").change(function() {
            readURL(this);
        });
    </script>

    <script>
        // Custom Script Here
        $(document).ready(function(){


            $('.social-selected input.is_input').click(function(){
                var input_id = $(this).attr('id');
                var arr = input_id.split('_');
                var media_str = '#social_media_id_' + arr[arr.length-1];
                var media_list_str = '#social_div_' + arr[arr.length-1];
                var input_disable_str = media_list_str + ' .removeDis';

                var media = $(media_str);
                var media_list = $(media_list_str);
                var input_disable = $(input_disable_str);


                if($(this).is(":checked")){
                    // alert("Checkbox is checked.");
                    media_list.removeClass('d-none');
                    input_disable.removeAttr('disabled', 'true');
                }
                else if($(this).is(":not(:checked)")){
                    // alert("Checkbox is unchecked.");
                    media_list.addClass('d-none');
                    input_disable.attr('disabled', 'true');
                }
            });


            $('.social-selected .media_clicked').click(function(){
                var click = $(this).attr('class');
                click = click.split(' ');
                click = '.' + click[click.length-1];

                var spClick = click.split('_');
                spClick = spClick[spClick.length-1];

                var social_auth = '.social_auth_' + spClick;

                if($(click).is(":checked")){
                    // alert("Checkbox is checked.");
                    $(social_auth).attr('disabled', 'true');
                    $(social_auth).removeAttr('required', 'required');
                }
                else if($(click).is(":not(:checked)")){
                    // alert("Checkbox is unchecked.");
                    $(social_auth).removeAttr('disabled', 'true');
                    $(social_auth).attr('required', 'required');
                }

            });
        });
    </script>
@endsection
