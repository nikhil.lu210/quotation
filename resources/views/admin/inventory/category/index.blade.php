@extends('layouts.admin.app')

@section('page_title', 'INVENTORY | Category')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/selectize/dist/css/selectize.default.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/summernote/dist/summernote.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS Here */
        /* .form-control{
            padding: 0.700rem 0.75rem;
        } */
        .selectize-input {
            padding: 0.54rem 0.75rem;
        }
        .card-block.task-file.p-0 {
            height: 80px;
        }
        .selectize-dropdown.single{
            z-index: 99999999;
        }
        .selectize-dropdown [data-selectable], .selectize-dropdown .optgroup-header {
            padding: 15px 8px !important;
        }
    </style>
@endsection

@section('main_content')
<div class="container-fluid">
    <div class="page-title">
        <h4>{{ __('Category List') }}</h4>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-heading">
                    <h4 class="card-title float-right">{{ __('All Categories') }}</h4>

                    <a href="javascript:void(0);" data-toggle="modal" data-target="#add_category_item" class="btn btn-dark btn-sm float-left m-b-0">{{ __('Add New Category') }}</a>
                </div>
                <hr class="m-t-0">
                <div class="card-block">
                    <div class="table-overflow">
                        <table id="dt-opt" class="table table-lg table-hover table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <th class="text-center">{{ __('Action') }}</th>
                                    <th class="text-center">{{ __('Status') }}</th>
                                    <th class="text-center">{{ __('Quantity') }}</th>
                                    <th class="text-center">{{ __('Description') }}</th>
                                    <th class="text-center">{{ __('Category Name') }}</th>
                                    <th class="text-center">#</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($categories as $item => $category)
                                <tr>
                                    <td class="text-center">
                                        <div class="m-t-5 dropdown">
                                            <a href="#" class="btn btn-icon btn-flat btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="ti-more"></i></a>
                                            <ul class="dropdown-menu">
                                                {{-- <li>
                                                    <a href="#">
                                                        <i class="ti-eye pdd-right-10 text-info"></i>
                                                        <span>{{ __('Details') }}</span>
                                                    </a>
                                                </li> --}}
                                                <li>
                                                    @if ($category->status == 1)
                                                        <a href="{{ route('admin.inventory.category.status', ['id' => encrypt($category->id)]) }}" class="confirmation">
                                                            <i class="ti-close pdd-right-10 text-danger"></i>
                                                            <span>{{ __('Deactivate') }}</span>
                                                        </a>
                                                    @else
                                                        <a href="{{ route('admin.inventory.category.status', ['id' => encrypt($category->id)]) }}" class="confirmation">
                                                            <i class="ti-check pdd-right-10 text-success"></i>
                                                            <span>{{ __('Active') }}</span>
                                                        </a>
                                                    @endif

                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="m-t-5">
                                            <span class="text-dark">
                                                @if ($category->status == 1)
                                                    <i class="ti-check pdd-right-10 text-success"></i>
                                                @else
                                                    <i class="ti-close pdd-right-10 text-danger"></i>
                                                @endif
                                            </span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="m-t-5">
                                            <span class="text-dark">
                                                <b>{{ $category->inventories->count() }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="m-t-5">
                                            <span class="text-dark">
                                                <b>{{ $category->description }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="m-t-5">
                                            <span class="text-dark">
                                                <b>{{ $category->name }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="m-t-5">
                                            <span class="text-dark">
                                                <b>{{ $item+1 }}</b>
                                            </span>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('admin.inventory.modals.add_category')
</div>
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>

    <script src="{{ asset('assets/vendors/selectize/dist/js/standalone/selectize.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('assets/js/forms/form-elements.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
        $('.selectize-group').selectize({
            sortField: 'text'
        });
    </script>
@endsection
