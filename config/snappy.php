<?php

return array(

    /**
     * You have to change permission on server to reading the pdf
     * In your server go to quotation\vendor\h4cc\wkhtmltopdf-amd64\bin\wkhtmltopdf-amd64
     * And change the wkhtmltopdf-amd64 file permission as 755
     */

    'pdf' => array(
        'enabled' => true,
        // 'binary' => base_path('vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64'),
        // 'binary' => '/usr/local/bin/wkhtmltopdf', //For Linux Server
        // 'binary' => base_path('vendor\wemersonjanuario\wkhtmltopdf-windows\bin\64bit\wkhtmltopdf'), // For Windows Server
        'binary' => (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? base_path('vendor\wemersonjanuario\wkhtmltopdf-windows\bin\64bit\wkhtmltopdf') : '/usr/local/bin/wkhtmltopdf',
        // 'binary' => (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? base_path('vendor\wemersonjanuario\wkhtmltopdf-windows\bin\64bit\wkhtmltopdf') : base_path('vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64'),
        'timeout' => false,
        'options' => array(),
        'env'     => array(),
    ),
    'image' => array(
        'enabled' => true,
        'binary'  => 'vendor/h4cc/wkhtmltoimage-amd64/wkhtmltoimage',
        'timeout' => false,
        'options' => array(),
        'env'     => array(),
    ),


);
