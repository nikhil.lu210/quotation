<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class SetLanguage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()){
            \App::setLocale(Auth::user()->lang);
        } else{
            \App::setLocale('en');
        }
        return $next($request);
    }
}
