<?php

namespace App\Http\Controllers\Admin\Invoice;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Currency;
use App\Models\Invoice;
use App\Models\Inventory;
use App\Models\InvoiceCart;
use App\Models\Billing;
use App\Models\Customer;
use App\Models\Tax;
use App\User;
use DateTime;
use PDF;
use Mail;

class InvoiceController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * invoiceIndex
     */
    public function invoiceIndex()
    {
        $invoices = Invoice::with(['customer', 'currency'])->get();
        // dd($invoices);
        return view('admin.invoice.index', compact(['invoices']));
    }


    /**
     * invoiceShow
     */
    public function invoiceShow($id)
    {
        $id = decrypt($id);

        $invoice = Invoice::with(
            [
                'customer'  => function($query){
                    $query->select('id', 'name', 'email', 'mobile_number', 'billing_address_1', 'billing_city', 'billing_state', 'billing_postal', 'billing_country');
                },

                'currency' => function($query){
                    $query->select('id', 'sign');
                },

                'items.item' => function($query){
                    $query->select('id', 'avatar', 'name', 'description', 'inventory_url');
                },

                'user' => function($query){
                    $query->select('id', 'name', 'email');
                },

                'company' => function($query){
                    $query->select('*');
                }

            ])->where('id', $id)->firstOrFail();
        // dd($invoice);
        return view('admin.invoice.show', compact('invoice'));
    }


    /**
     * invoiceEdit
     */
    public function invoiceEdit($id)
    {
        $id = decrypt($id);

        $invoice = Invoice::with(
            [
                'customer'  => function($query){
                    $query->select('id', 'name', 'email', 'mobile_number', 'billing_address_1', 'billing_city', 'billing_state', 'billing_postal', 'billing_country');
                },

                'currency' => function($query){
                    $query->select('id', 'sign');
                },

                'items.item' => function($query){
                    $query->select('id', 'avatar', 'name', 'description');
                },

                'user' => function($query){
                    $query->select('id', 'name', 'email');
                },

                'company' => function($query){
                    $query->select('*');
                }

            ])->where('id', $id)->firstOrFail();

        $currencies = Currency::where('status',1)->get();
        $admins = User::select('id', 'name')->get();
        $customers = Customer::select('id', 'name')->get();
        $inventories = Inventory::select('id', 'name')->get();
        $default_payment_term = Billing::where('name', 'default_payment_term')->where('text', '!=', null)->first();
        $tax = Tax::where('is_default', 1)->first();

        return view('admin.invoice.edit', compact(['currencies', 'invoice', 'admins', 'customers', 'inventories', 'default_payment_term', 'tax']));
    }


    /**
     * invoiceCreate
     */
    public function invoiceCreate()
    {
        $currencies = Currency::where('status',1)->get();
        $last_inv = Invoice::orderBy('id', 'DESC')->first();
        $last_inv = ($last_inv == null) ? 1:$last_inv->id+1;
        $admins = User::select('id', 'name')->get();
        $customers = Customer::select('id', 'name')->get();
        $inventories = Inventory::select('id', 'name')->get();
        $default_payment_term = Billing::where('name', 'default_payment_term')->where('text', '!=', null)->first();
        $tax = Tax::where('is_default', 1)->first();
        return view('admin.invoice.create', compact(['currencies', 'last_inv', 'admins', 'customers', 'inventories', 'default_payment_term', 'tax']));
    }


    /**
     * getItem
     */
    public function getItem($id)
    {
        $item = Inventory::where('id',$id)->first();
        return response()->json($item);
    }


    /**
     * getItem
     */
    public function getCustomer($id)
    {
        $item = Customer::find($id);
        return response()->json($item);
    }


    /**
     * invoiceStore
     */
    public function invoiceStore(Request $request){
        // dd($request->all());
        $invoice_id = null;

        $this->validate($request, array(
            'customer_id'         =>  'required',
            'invoice_number'    =>  'required',
            'currency'    =>  'required',
            "items"             =>  'required | array | min:1',
        ));

        $new_invoice = new Invoice();

        $new_invoice->company_id = auth()->user()->company_id;
        $new_invoice->customer_id = $request->customer_id;

        $currency = explode('-', $request->currency);
        $new_invoice->currency_id = $currency[0];

        if($request->sales_person != null) $new_invoice->sales_person = $request->sales_person;
        $new_invoice->inv_number = $request->invoice_number;

        if($request->expire_date != null){
            $expire_date = new DateTime($request->expire_date);
            $new_invoice->exp_date = $expire_date->format('Y-m-d');
        }



        if($request->client_email != null) $new_invoice->client_email = $request->client_email;
        if($request->mobile_number != null) $new_invoice->mobile_number = $request->mobile_number;
        if($request->client_note != null) $new_invoice->client_note = $request->client_note;
        if($request->payment_term != null) $new_invoice->payment_term = $request->payment_term;
        if($request->overall_discount != null) $new_invoice->discount = $request->overall_discount;
        if($request->tax != null) $new_invoice->tax = $request->tax;
        
        if($request->cart_grand_total != null) $new_invoice->total_dis_price = $request->cart_grand_total;

        if($request->analysis_total_cost != null) $new_invoice->total_cost = $request->analysis_total_cost;

        if($new_invoice->save()){
            $current_invoice = Invoice::select('id')
                                ->where('customer_id', $request->customer_id)
                                ->where('sales_person', $new_invoice->sales_person)
                                ->where('status', 1)
                                ->orderBy('id', 'DESC')
                                ->firstOrFail();
            $invoice_id = $current_invoice->id;

            foreach($request->items as $key => $item){
                $cart = new InvoiceCart();

                $cart->item_id          = $item['item_id'];
                $cart->invoice_id       = $current_invoice->id;
                $cart->quantity         = $item['quantity'];
                $cart->total_price      = $item['total_price'];
                $cart->custom_price     = $item['custom_price'];
                $cart->discount         = $item['discount'];
                $cart->dis_price        = $item['dis_price'];
                $cart->original_price   = $item['original_price'];
                $cart->secret_id        = $key;

                if( ! $cart->save() ){
                    //confirmation message
                    toast('Invoice has not created. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
                    $current_invoice->delete();
                    return redirect()->back();
                }
            }
        } else{
            //confirmation message
            toast('Invoice has not created. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }

        // $email = ($request->client_email != null) ? $request->client_email:Customer::findOrFail($request->customer_id)->email;

        // $data = array(
        //     'name'	 =>	auth()->user()->company->name,
        //     'email'	 => $email,
        //     'subject' => "New Invoice"

        // );

        // $invoice = Invoice::with(['customer', 'currency', 'items.item', 'user', 'company'])->where('id', $invoice_id)->firstOrFail();
        // Mail::send('admin.invoice.pdf', compact('invoice'), function($message) use ($data) {
        //     $message->to($data['email']);
        //     $message->subject($data['subject']);
        //     // $message->from('sperrowmailtest@gmail.com');
        //     $message->from('sperrowmailtest@gmail.com', $data['name']);
        // });

        //confirmation message
        toast('Invoice has been Created Successfully.', 'success');

        return redirect()->route('admin.invoice.invoice.index');

    }


    /**
     * invoiceUpdate
     */
    public function invoiceUpdate(Request $request, $id){
        // dd($request->all());

        $id = decrypt($id);
        $invoice = Invoice::with('items')->findOrFail($id);

        foreach($invoice->items as $item){
            if(!$item->delete()){
                toast('Invoice has not updated. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
                return redirect()->back();
            }
        }
        

        $invoice_id = null;

        $this->validate($request, array(
            'customer_id'         =>  'required',
            'invoice_number'    =>  'required',
            'currency'    =>  'required',
            "items"             =>  'required | array | min:1',
        ));



        $invoice->company_id = auth()->user()->company_id;
        $invoice->customer_id = $request->customer_id;

        $currency = explode('-', $request->currency);
        $invoice->currency_id = $currency[0];

        if($request->sales_person != null) $invoice->sales_person = $request->sales_person;
        $invoice->inv_number = $request->invoice_number;

        if($request->expire_date != null){
            $expire_date = new DateTime($request->expire_date);
            $invoice->exp_date = $expire_date->format('Y-m-d');
        }



        if($request->client_email != null) $invoice->client_email = $request->client_email;
        if($request->mobile_number != null) $invoice->mobile_number = $request->mobile_number;
        if($request->client_note != null) $invoice->client_note = $request->client_note;
        if($request->payment_term != null) $invoice->payment_term = $request->payment_term;
        if($request->overall_discount != null) $invoice->discount = $request->overall_discount;
        if($request->tax != null) $invoice->tax = $request->tax;
        
        if($request->cart_grand_total != null) $invoice->total_dis_price = $request->cart_grand_total;

        if($request->analysis_total_cost != null) $invoice->total_cost = $request->analysis_total_cost;

        if($invoice->save()){
            $invoice_id = $invoice->id;

            foreach($request->items as $key => $item){
                $cart = new InvoiceCart();

                $cart->item_id          = $item['item_id'];
                $cart->invoice_id       = $invoice_id;
                $cart->quantity         = $item['quantity'];
                $cart->total_price      = $item['total_price'];
                $cart->custom_price     = $item['custom_price'];
                $cart->discount         = $item['discount'];
                $cart->dis_price        = $item['dis_price'];
                $cart->original_price   = $item['original_price'];
                $cart->secret_id        = $key;

                if( ! $cart->save() ){
                    //confirmation message
                    toast('Invoice has not updated. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
                    $current_invoice->delete();
                    return redirect()->back();
                }
            }
        } else{
            //confirmation message
            toast('Invoice has not updated. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }

        // $email = ($request->client_email != null) ? $request->client_email:Customer::findOrFail($request->customer_id)->email;

        // $data = array(
        //     'name'	 =>	auth()->user()->company->name,
        //     'email'	 => $email,
        //     'subject' => "New Invoice"

        // );

        // $invoice = Invoice::with(['customer', 'currency', 'items.item', 'user', 'company'])->where('id', $invoice_id)->firstOrFail();
        // Mail::send('admin.invoice.pdf', compact('invoice'), function($message) use ($data) {
        //     $message->to($data['email']);
        //     $message->subject($data['subject']);
        //     // $message->from('sperrowmailtest@gmail.com');
        //     $message->from('sperrowmailtest@gmail.com', $data['name']);
        // });

        //confirmation message
        toast('Invoice has been Updated Successfully.', 'success');

        return redirect()->route('admin.invoice.invoice.index');

    }




    public function sendEmail($invoice_id)
    {
        $id = decrypt($invoice_id);

        $invoice = Invoice::with(['customer', 'currency', 'items.item', 'user', 'company'])->where('id', $id)->firstOrFail();

        $email = ($invoice->client_email != null) ? $invoice->client_email : $invoice->customer->email;


        $data = array(
            'name'	 =>	auth()->user()->company->name,
            'email'	 => $email,
            'subject' => "New Quotation",
        );

        // dd($data);

        Mail::send('admin.invoice.email', compact('invoice'), function($message) use ($data) {

            $message->to($data['email']);
            $message->subject($data['subject']);
            // $message->from('sperrowmailtest@gmail.com');
            $message->from('sperrowmailtest@gmail.com', $data['name']);
        });

        //confirmation message
        toast('Invoice Mail Sent Successfully.', 'success');

        return redirect()->back();
    }




    /**
     * for generating pdf
     */
    public function pdfGenerate($id){
        $id = decrypt($id);
        $invoice = Invoice::with(['customer', 'currency', 'items.item', 'user', 'company'])->where('id', $id)->firstOrFail();

        $pdf = PDF::loadView('admin.invoice.pdf', compact(['invoice']));
        $pdf->setOption('enable-javascript', true);
        $pdf->setOption('javascript-delay', 2000);
        $pdf->setOption('enable-smart-shrinking', true);
        $pdf->setOption('no-stop-slow-scripts', true);
        $name = $invoice->inv_number.'.pdf';
        return $pdf->download($name);
    }





    public function pdfView($id)
    {
        $id = decrypt($id);

        $invoice = Invoice::with(['customer', 'currency', 'items.item', 'user', 'company'])->where('id', $id)->firstOrFail();
        return view('admin.invoice.pdf', compact('invoice'));
    }
}
