<?php

namespace App\Http\Controllers\Admin\Setting\Inventory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Unit;
use App\Models\InventorySettings;;

class InventoryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * =====================================================
     * ===============< Inventory Settings >================
     * =====================================================
     */

    /**
     * inventoryIndex
     * @return inventoryIndex
     */
    public function inventoryIndex()
    {
        return view('admin.setting.inventory.inventory.index');
    }


    /**
     * changeInventoryStatus
     * @return changeInventoryStatus
     */
    public function changeInventoryStatus(Request $request)
    {
        $inventory = InventorySettings::where('type', $request->type)->where('name', $request->name)->first();
        if($inventory != null){
            $inventory->status = ($inventory->status == 1) ? 0:1;
            if($inventory->save()){
                $name = ucfirst(str_replace("_"," ",$request->name));
                $value = $name." Status changed";
                $report = array(
                    'status' => 'success',
                    'value'  => $value
                );
                return response($report);
            } else{
                $report = array(
                    'status' => 'error',
                    'value'  => 'Status does not changed'
                );
                return response($report);
            }
        } else{
            $report = array(
                'status' => 'error',
                'value'  => 'Something worng'
            );
            return response($report);
        }

    }





    /**
     * =====================================================
     * ==================< Unit Settings >==================
     * =====================================================
     */
    /**
     * unitIndex
     * @return unitIndex
     */
    public function unitIndex()
    {
        $units = Unit::all();
        return view('admin.setting.inventory.unit.index', compact(['units']));
    }


    /**
     * unitStore
     * @return unitStore
     */
    public function unitStore(Request $request)
    {
        $this->validate($request, array(
            'name'     => 'required | string',
            'short_form'    => 'required | string | max:10',
        ));

        $unit = new Unit();

        $unit->name         =   $request->name;
        $unit->short_form   =   $request->short_form;

        $unit->save();

        toast('New Unit Created Successfully.', 'success')->autoClose(2000)->timerProgressBar();
        return redirect()->back();

    }




    /**
     * =====================================================
     * ================< Shipping Settings >================
     * =====================================================
     */
    /**
     * shippingIndex
     * @return shippingIndex
     */
    public function shippingIndex()
    {
        return view('admin.setting.inventory.shipping.index');
    }
}
