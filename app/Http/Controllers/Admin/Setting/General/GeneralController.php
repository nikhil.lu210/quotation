<?php

namespace App\Http\Controllers\Admin\Setting\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

use App\User;
use App\Models\Company;
use Auth;

class GeneralController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * companyIndex
     */
    public function companyIndex()
    {
        $company = auth()->user()->company;
        return view('admin.setting.general.company.index', compact(['company']));
    }

    /**
     * companyIndex
     */
    public function companyUpdate(Request $request, $company_id)
    {
        $this->validate($request, array(
            'name' => 'required | string'
        ));

        $company_id = decrypt($company_id);
        $company = Company::findOrFail($company_id);

        if($request->hasFile('logo')){

            $this->validate($request, array(
                "logo" => 'required | image | mimes:jpeg,png,jpg,gif,svg | max:1024 | dimensions:max_width=150,max_height=65',
            ));

            $image_data=file_get_contents($request->logo);

            $encoded_image=base64_encode($image_data);

            $company->logo = 'data:image/png;base64,'.$encoded_image;
        }


        $company->name = $request->name;
        if($request->country != null)
            $company->country = $request->country;
        if($request->city != null)
            $company->city = $request->city;
        if($request->state != null)
            $company->state = $request->state;
        if($request->postal_code != null)
            $company->postal_code = $request->postal_code;
        if($request->address_1 != null)
            $company->address_1 = $request->address_1;
        if($request->address_2 != null)
            $company->address_2 = $request->address_2;

        if($company->save()){
            toast('Company Updated Successfully', 'success')->autoClose(2000)->timerProgressBar();
        } else{
            toast('Company Does not Updated', 'error')->autoClose(2000)->timerProgressBar();
        }
        return redirect()->back();


    }



    /**
     * =====================================================
     * ===============< Account Settings >================
     * =====================================================
     */

    /**
     * accountIndex
     * @return accountIndex
     */
    public function accountIndex()
    {
        $users = User::all();
        return view('admin.setting.general.account.index', compact(['users']));
    }

    /**
     * accountShow
     * @return accountShow
     */
    public function accountShow($id)
    {
        $user = User::findOrFail(decrypt($id));
        return view('admin.setting.general.account.show', compact(['user']));
    }




    /**
     * changeLang
     */
    public function changeLang(Request $request)
    {
        $user = User::findOrFail(Auth::user()->id);
        $user->lang = $request->lang;

        if($user->save()){
            toast('Language Changed Successfully', 'success')->autoClose(2000)->timerProgressBar();
        } else{
            toast('Language Does not Changed', 'error')->autoClose(2000)->timerProgressBar();
        }
        return redirect()->back();
    }

    /**
     * changeCompany
     */
    public function changeCompany(Request $request)
    {
        $user = User::findOrFail(Auth::user()->id);
        $user->company_id = $request->id;

        if($user->save()){
            toast('Company Changed Successfully', 'success')->autoClose(2000)->timerProgressBar();
        } else{
            toast('Company Does not Changed', 'error')->autoClose(2000)->timerProgressBar();
        }
        return redirect()->back();
    }






    /**
     * accountStore
     */
    public function accountStore(Request $request)
    {
        $this->validate($request, array(
            'name' => 'required | string | max:255',
            'email' => 'required | string | email | max:255 | unique:users',
            'password' => 'required | string | min:8 | confirmed',
        ));
        $user = new User();

        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);

        if($user->save()){
            toast('User Created Successfully', 'success')->autoClose(2000)->timerProgressBar();
        } else{
            toast('User Does not Created', 'error')->autoClose(2000)->timerProgressBar();
        }

        return redirect()->back();
    }

    /**
     * accountUpdate
     */
    public function accountUpdate(Request $request, $id)
    {
        $user = User::findOrFail(decrypt($id));
        
        if($user->email != $request->email){
            $this->validate($request, array(
                'email' => 'required | string | email | max:255 | unique:users',
            ));
        }

        $user->name = $request->name;
        $user->email = $request->email;

        if($request->hasFile('avatar')){

            $this->validate($request, array(
                "avatar" => 'required | image | mimes:jpeg,png,jpg,gif,svg | max:2024',
            ));

            $image_data=file_get_contents($request->avatar);

            $encoded_image=base64_encode($image_data);

            $user->avatar = 'data:image/png;base64,'.$encoded_image;
        }

        if($user->save()){
            toast('User Updated Successfully', 'success')->autoClose(2000)->timerProgressBar();
        } else{
            toast('User Does not Updated', 'error')->autoClose(2000)->timerProgressBar();
        }

        return redirect()->back();


    }

    /**
     * accountPassword
     */
    public function accountPassword(Request $request, $id)
    {
        $user = User::findOrFail(decrypt($id));

        // dd($request);
        $this->validate($request, [
            'old_password'          => 'required | string | min:8',
            'password'          => 'required | string | min:8 | same:password_confirmation',
        ]);

        if( !Hash::check($request->old_password, Auth::user()->password) ){
            toast('Current Password Does Not Match...', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
        elseif($request->old_password == $request->password){
            toast('Both are same...', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
        else{
            $user = Auth::user();

            $user->password = Hash::make($request->password);

            if($user->save()){
                toast('Password has been changed Successfully', 'success')->autoClose(2000)->timerProgressBar();
            } else{
                toast('Password Does not Changed', 'error')->autoClose(2000)->timerProgressBar();
            }
            
            return redirect()->back();
        }
    }

    /**
     * accountStatus
     */
    public function accountStatus($id)
    {
        $user = User::findOrFail(decrypt($id));

        if(auth()->user()->id != $user->id){

            $user->status = ($user->status == 1)? 0:1;

            if($user->save()){
                toast('User Status Changed Successfully', 'success')->autoClose(2000)->timerProgressBar();
            } else{
                toast('User Status Does not Changed', 'error')->autoClose(2000)->timerProgressBar();
            }
        } else{
            toast('This User Logged In', 'warning')->autoClose(2000)->timerProgressBar();
        }
        return redirect()->back();
        
    }
}
