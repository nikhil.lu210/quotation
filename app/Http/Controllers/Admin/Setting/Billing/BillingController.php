<?php

namespace App\Http\Controllers\Admin\Setting\Billing;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Currency;
use App\Models\Billing;
use App\Models\Tax;

class BillingController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * billingIndex
     */
    public function billingIndex()
    {
        $currencies = Currency::select('id', 'status', 'name', 'sign', 'is_default')->get();
        return view('admin.setting.billing.billing.index', compact(['currencies']));
    }


    /**
     * currencyStore
     */
    public function currencyStore(Request $request)
    {
        $this->validate($request, array(
            'name'     => 'required | string | max:3 | unique:currencies',
            'sign'     => 'required | string | max:5 | unique:currencies',
        ));
        // dd($request);

        $data = new Currency();

        $data->name = $request->name;
        $data->sign = $request->sign;
        if(isset($request->is_default) && $request->is_default == 'on'){
            $is_defaults = Currency::where('is_default', true)->get();
            foreach($is_defaults as $is_default){
                $is_default->is_default = false;
                $is_default->save();
            }
            $data->is_default = true;
        }

        if($data->save()){
            toast('Currency Added Successfully.', 'success')->autoClose(2000)->timerProgressBar();
        } else{
            toast('Currency Doesn\'n Saved.', 'error')->autoClose(2000)->timerProgressBar();
        }


        return redirect()->back();
    }

    /**
     * currencyUpdate
     */
    public function currencyUpdate(Request $request)
    {
        $data = Currency::findOrFail(decrypt($request->id));
        if($data->name != $request->name){
            $this->validate($request, array(
                'name'     => 'required | string | max:3 | unique:currencies',
            ));
        }
        if($data->sign != $request->sign){
            $this->validate($request, array(
                'sign'     => 'required | string | max:5 | unique:currencies',
            ));
        }

        $data->name = $request->name;
        $data->sign = $request->sign;
        if(isset($request->is_default) && $request->is_default == 'on'){
            $is_defaults = Currency::where('is_default', true)->get();
            foreach($is_defaults as $is_default){
                $is_default->is_default = false;
                $is_default->save();
            }
            $data->is_default = true;
        }

        if($data->save()){
            toast('Currency Added Successfully.', 'success')->autoClose(2000)->timerProgressBar();
        } else{
            toast('Currency Doesn\'n Saved.', 'error')->autoClose(2000)->timerProgressBar();
        }


        return redirect()->back();
    }


    /**
     * currencyStatus
     */
    public function currencyStatus($id)
    {
        // dd($request);

        $data = Currency::findOrFail(decrypt($id));

        if($data->is_default != 1){
            $data->status = ($data->status == 1) ? false : true;
            if($data->save()){
                if($data->status == 1)
                    toast('Currency Activated.', 'success')->autoClose(2000)->timerProgressBar();
                else
                    toast('Currency Deactivated.', 'error')->autoClose(2000)->timerProgressBar();
            } else{
                toast('Something wrong, try again!', 'error')->autoClose(2000)->timerProgressBar();
            }
        } else{
            toast('First Set Default Other Currency item!', 'warning')->autoClose(2000)->timerProgressBar();
        }
        return redirect()->back();
    }

    /**
     * isDefault
     */
    public function isDefault($id)
    {
        // dd($request);

        $data = Currency::findOrFail(decrypt($id));

        $is_defaults = Currency::where('is_default', true)->get();
        foreach($is_defaults as $is_default){
            $is_default->is_default = false;
            $is_default->save();
        }
        $data->is_default = true;
        if($data->save()){
            toast('Currency set as Default.', 'success')->autoClose(2000)->timerProgressBar();
        } else{
            toast('Something wrong, try again!', 'error')->autoClose(2000)->timerProgressBar();
        }
        return redirect()->back();
    }


    /**
     * billingUpdate
     */
    public function billingUpdate(Request $request)
    {
        // dd($request->all());
        $type = array(
            'invoice_name'  => 'text',
            'estimate_name' => 'text',
            'estimate_expiry'   => 'number',
            'default_payment_term'  => 'textarea',
            'set_zeroed_out_invoice'    => 'checkbox',
            'prevent_duplicate_line_item'   => 'checkbox',
            'show_sku'  => 'checkbox',
            'add_item_description'  => 'checkbox',
            'pdf_added_language'    => 'checkbox',
        );
        
        $checkboxes = Billing::where('type', 'checkbox')->get();
        foreach($checkboxes as $check){
            $check->status = 0;
            $check->save();
        }

        $datas = $request->all();
        $i=0;
        foreach($datas as $key => $data){
            if($i == 0) { $i = 1; continue; }
            $billing = Billing::where('type', $type[$key])->where('name', $key)->first();

            if($billing != null){
                if($type[$key] == 'text') $billing->text = $data;
                elseif($type[$key] == 'number') $billing->number = $data;
                elseif($type[$key] == 'textarea') $billing->text = $data;
                elseif($type[$key] == 'checkbox') $billing->status = 1;
                if(!$billing->save()){
                    toast('Something wrong.', 'error')->autoClose(2000)->timerProgressBar();
                    return redirect()->back();
                }
            } else{
                toast('Something wrong.', 'error')->autoClose(2000)->timerProgressBar();
                return redirect()->back();
            }
        }

        toast('Updated successfully.', 'success')->autoClose(2000)->timerProgressBar();
        return redirect()->back();
    }


    /**
     * taxIndex
     */
    public function taxIndex()
    {
        $taxes = Tax::all();
        return view('admin.setting.billing.tax.index', compact('taxes'));
    }

    /**
     * taxStore
     */
    public function taxStore(Request $request)
    {
        $this->validate($request, array(
            'name'  => 'required | string',
            'rate'  => 'required'
        ));

        $tax = new Tax();

        $tax->name = $request->name;
        if($request->registration != null) $tax->registration = $request->registration;
        $tax->rate = $request->rate;
        if(isset($request->is_default) && $request->is_default == 'on'){
            $is_defaults = Tax::where('is_default', true)->get();
            foreach($is_defaults as $is_default){
                $is_default->is_default = false;
                $is_default->save();
            }
            $tax->is_default = true;
        }

        if($tax->save()){
            toast('Tax Added Successfully.', 'success')->autoClose(2000)->timerProgressBar();
        } else{
            toast('Tax Doesn\'n Saved.', 'error')->autoClose(2000)->timerProgressBar();
        }

        return redirect()->back();
    }


    
    /**
     * taxUpdate
     */
    public function taxUpdate(Request $request)
    {
        $tax = Tax::findOrFail(decrypt($request->id));
        $this->validate($request, array(
            'name'  => 'required | string',
            'rate'  => 'required'
        ));

        $tax->name = $request->name;
        if($request->registration != null) $tax->registration = $request->registration;
        $tax->rate = $request->rate;
        if(isset($request->is_default) && $request->is_default == 'on'){
            $is_defaults = Tax::where('is_default', true)->get();
            foreach($is_defaults as $is_default){
                $is_default->is_default = false;
                $is_default->save();
            }
            $tax->is_default = true;
        }

        if($tax->save()){
            toast('Tax Update Successfully.', 'success')->autoClose(2000)->timerProgressBar();
        } else{
            toast('Tax Doesn\'n Updated.', 'error')->autoClose(2000)->timerProgressBar();
        }

        return redirect()->back();
    }


    /**
     * taxStatus
     */
    public function taxStatus($id)
    {
        // dd($request);

        $data = Tax::findOrFail(decrypt($id));

        if($data->is_default != 1){
            $data->status = ($data->status == 1) ? false : true;
            if($data->save()){
                if($data->status == 1)
                    toast('Tax Activated.', 'success')->autoClose(2000)->timerProgressBar();
                else
                    toast('Tax Deactivated.', 'error')->autoClose(2000)->timerProgressBar();
            } else{
                toast('Something wrong, try again!', 'error')->autoClose(2000)->timerProgressBar();
            }
        } else{
            toast('First Set Default Other Tax item!', 'warning')->autoClose(2000)->timerProgressBar();
        }

        
        return redirect()->back();
    }

    /**
     * taxIsDefault
     */
    public function taxIsDefault($id)
    {
        // dd($request);

        $data = Tax::findOrFail(decrypt($id));

        $is_defaults = Tax::where('is_default', true)->get();
        foreach($is_defaults as $is_default){
            $is_default->is_default = false;
            $is_default->save();
        }
        $data->is_default = true;
        if($data->save()){
            toast('Tax set as Default.', 'success')->autoClose(2000)->timerProgressBar();
        } else{
            toast('Something wrong, try again!', 'error')->autoClose(2000)->timerProgressBar();
        }
        return redirect()->back();
    }



    /**
     * emailIndex
     */
    public function emailIndex()
    {
        return view('admin.setting.billing.email.index');
    }
}
