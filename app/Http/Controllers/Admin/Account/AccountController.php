<?php

namespace App\Http\Controllers\Admin\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Models\Customer;


class AccountController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * customerIndex
     */
    public function customerIndex()
    {
        $customers = Customer::with('manager')->get();
        return view('admin.account.index', compact(['customers']));
    }


    /**
     * customerCreate
     */
    public function customerCreate()
    {
        $managers = User::select('id', 'name', 'status')->where('status', 1)->get();
        return view('admin.account.create', compact(['managers']));
    }


    /**
     * customerCreate
     */
    public function customerShow($id)
    {
        $id = decrypt($id);
        $customer = Customer::findOrFail($id);
        $managers = User::select('id', 'name', 'status')->where('status', 1)->get();
        return view('admin.account.show', compact(['customer', 'managers']));
    }


    /**
     * customerStore
     */
    public function customerStore(Request $request)
    {
         $this->validate($request, array(
             'name'     => 'required | string',
             'email'    => 'required | email',
         ));

        // dd($request->all());

        $customer = new Customer();

        if($request->hasFile('avatar')){

            $this->validate($request, array(
                "avatar" => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1024',
            ));

            $image_data=file_get_contents($request->avatar);

            $encoded_image=base64_encode($image_data);

            $customer->avatar = 'data:image/png;base64,'.$encoded_image;
        }



        $customer->name = $request->name;
        $customer->email = $request->email;

        if($request->mobile)
            $customer->mobile_number = $request->mobile;

        if($request->office_phone)
            $customer->office_phone = $request->office_phone;

        if($request->website)
            $customer->website = $request->website;

        if($request->account_manager)
            $customer->manager_id = $request->account_manager;

        if($request->billing_address_line_one)
            $customer->billing_address_1 = $request->billing_address_line_one;

        if($request->billing_address_line_two)
            $customer->billing_address_2 = $request->billing_address_line_two;

        if($request->billing_city)
            $customer->billing_city = $request->billing_city;

        if($request->billing_state)
            $customer->billing_state = $request->billing_state;

        if($request->billing_post)
            $customer->billing_postal = $request->billing_post;

        if($request->billing_country)
            $customer->billing_country = $request->billing_country;

        if($request->shipping_address_line_one)
            $customer->shipping_address_1 = $request->shipping_address_line_one;

        if($request->shipping_address_line_two)
            $customer->shipping_address_2 = $request->shipping_address_line_two;

        if($request->shipping_city)
            $customer->shipping_city = $request->shipping_city;

        if($request->shipping_state)
            $customer->shipping_state = $request->shipping_state;

        if($request->shipping_postal)
            $customer->shipping_post = $request->shipping_post;

        if($request->shipping_country)
            $customer->shipping_country = $request->shipping_country;

        if($customer->save()){
            toast('Customer has been Created.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->route('admin.account.customer.index');
        } else {
            toast('Customer Does not created.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }

    }


    /**
     * customerUpdate
     */
    public function customerUpdate(Request $request, $id)
    {
        $id = decrypt($id);
        $customer = Customer::findOrFail($id);

         $this->validate($request, array(
             'name'     => 'required | string',
             'email'    => 'required | email',
         ));

        // dd($request->all());


        if($request->hasFile('avatar')){

            $this->validate($request, array(
                "avatar" => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1024',
            ));

            $image_data=file_get_contents($request->avatar);

            $encoded_image=base64_encode($image_data);

            $customer->avatar = 'data:image/png;base64,'.$encoded_image;
        }



        $customer->name = $request->name;
        $customer->email = $request->email;

        if($request->mobile)
            $customer->mobile_number = $request->mobile;

        if($request->office_phone)
            $customer->office_phone = $request->office_phone;

        if($request->website)
            $customer->website = $request->website;

        if($request->account_manager)
            $customer->manager_id = $request->account_manager;

        if($request->billing_address_line_one)
            $customer->billing_address_1 = $request->billing_address_line_one;

        if($request->billing_address_line_two)
            $customer->billing_address_2 = $request->billing_address_line_two;

        if($request->billing_city)
            $customer->billing_city = $request->billing_city;

        if($request->billing_state)
            $customer->billing_state = $request->billing_state;

        if($request->billing_post)
            $customer->billing_postal = $request->billing_post;

        if($request->billing_country)
            $customer->billing_country = $request->billing_country;

        if($request->shipping_address_line_one)
            $customer->shipping_address_1 = $request->shipping_address_line_one;

        if($request->shipping_address_line_two)
            $customer->shipping_address_2 = $request->shipping_address_line_two;

        if($request->shipping_city)
            $customer->shipping_city = $request->shipping_city;

        if($request->shipping_state)
            $customer->shipping_state = $request->shipping_state;

        if($request->shipping_postal)
            $customer->shipping_post = $request->shipping_post;

        if($request->shipping_country)
            $customer->shipping_country = $request->shipping_country;

        if($customer->save()){
            toast('Customer Details Has Been Updated.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        } else {
            toast('Customer Details Has Not Updated.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }

    }

    /**
     * delete customer
     *
     * @return CustomerDelete
     */
    public function customerDelete($id){

        $id = decrypt($id);
        $customer = Customer::findOrFail($id);

        if($customer->delete()){
            toast('Customer Deleted Successfully.', 'warning')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        } else {
            toast('Customer Does not Deleted.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
    }


    /**
     * customerStatus
     */
    public function customerStatus($id)
    {
        $id = decrypt($id);
        $customer = Customer::findOrFail($id);
        $customer->status = ($customer->status == 1) ? 0:1;

        if($customer->save()){
            toast('Customer Status Has Been Changed.', 'success')->autoClose(2000)->timerProgressBar();
        } else{
            toast('Customer Status Doesn\'t changed.', 'error')->autoClose(2000)->timerProgressBar();
        }
        return redirect()->back();
    }
}
