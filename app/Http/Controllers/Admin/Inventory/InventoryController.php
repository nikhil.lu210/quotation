<?php

namespace App\Http\Controllers\Admin\Inventory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Category;
use App\Models\Unit;
use App\Models\Inventory;
use App\Models\Currency;
use DateTime;

class InventoryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * inventoryIndex
     */
    public function inventoryIndex()
    {
        $categories = Category::select('id', 'name')->get();
        $units = Unit::select('id', 'name', 'short_form')->get();
        $inventories = Inventory::with(['category', 'unit'])->get();
        $currency = Currency::where('is_default', 1)->first();
        return view('admin.inventory.inventory.index', compact(['categories', 'units', 'inventories', 'currency']));
    }


    /**
     * inventoryShow
     */
    public function inventoryShow($id)
    {
        $id = decrypt($id);
        $inventory = Inventory::with(['category', 'unit'])->where('id', $id)->firstOrfail();
        $categories = Category::select('id', 'name')->get();
        $units = Unit::select('id', 'name', 'short_form')->get();
        $currency = Currency::where('is_default', 1)->first();
        return view('admin.inventory.inventory.show', compact(['inventory', 'currency', 'categories', 'units']));
    }

    /**
     * inventoryShow
     */
    public function inventoryDelete($id)
    {
        $id = decrypt($id);
        $inventory = Inventory::where('id', $id)->firstOrfail();

        if($inventory->delete()){
            toast('Inventory has been Deleted.', 'warning')->autoClose(2000)->timerProgressBar();
        } else{
            toast('Inventory Does not Deleted.', 'error')->autoClose(2000)->timerProgressBar();
        }

        return redirect()->back();
    }

    /**
     * inventoryStore
     */
    public function inventoryStore(Request $request)
    {
        // dd($request);
        $this->validate($request, array(
            'name'          =>  'required | string',
            'category_id'   =>  'required'
        ));

        if($request->quantity_inventory_item == 1){
            $this->validate($request, array(
                'quantity_price_per_unit'          =>  'required',
                'quantity_default_selling_price'   =>  'required',
            ));
        }
        if($request->unique_inventory_item == 1){
            $this->validate($request, array(
                'unique_purchase_price'          =>  'required',
                'unique_default_selling_price'          =>  'required',
            ));
        }

        if($request->inventory_url != null){
            $this->validate($request, array(
                'inventory_url'          =>  'regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
            ));
        }

        $inventory = new Inventory();

        $inventory->name = $request->name;
        $inventory->cat_id = $request->category_id;

        if($request->hasFile('avatar')){

            $this->validate($request, array(
                "avatar" => 'required | image | mimes:jpeg,png,jpg,gif,svg | max:2048',
            ));

            $image_data=file_get_contents($request->avatar);

            $encoded_image=base64_encode($image_data);

            $inventory->avatar = 'data:image/png;base64,'.$encoded_image;
        }

        if($request->purchased_date != null){
            $date = new DateTime($request->purchased_date);
            $inventory->purchased_date = $date->format('Y-m-d');
        }

        $inventory->description =  ($request->description != null) ? $request->description:null;
        
        $inventory->inventory_url =  ($request->inventory_url != null) ? $request->inventory_url:null;

        $inventory->inventory_type = ($request->quantity_inventory_item == 1) ? 'quantity_inventory_item':'unique_inventory_item';

        if($request->quantity_inventory_item == 1){
            $inventory->purchase_price_per_unit = $request->quantity_price_per_unit;
            $inventory->default_selling_price = $request->quantity_default_selling_price;

            $inventory->sku = ($request->quantity_sku != null) ? $request->quantity_sku:null;
            $inventory->global_quantity = ($request->quantity_global_quantity != null) ? $request->quantity_global_quantity:null;
            $inventory->low_quantity = ($request->quantity_low_quantity != null) ? $request->quantity_low_quantity:null;
            $inventory->unit_id = ($request->quantity_unit_id != null) ? $request->quantity_unit_id:null;
            $inventory->sort_priority = ($request->quantity_shorting_priority != null) ? $request->quantity_shorting_priority:null;
        } else {
            $inventory->purchase_price = $request->unique_purchase_price;
            $inventory->default_selling_price = $request->unique_default_selling_price;

            $inventory->sku = ($request->unique_sku != null) ? $request->unique_sku:null;
            $inventory->serial_number = ($request->unique_serial_number != null) ? $request->unique_serial_number:null;
            $inventory->unit_id = ($request->unique_unit_id != null) ? $request->unique_unit_id:null;
            $inventory->sort_priority = ($request->unique_shorting_priority != null) ? $request->unique_shorting_priority:null;
        }

        if($inventory->save()){
            toast('New Inventory has been Created.', 'success')->autoClose(2000)->timerProgressBar();
        } else{
            toast('New Inventory Does not Created.', 'error')->autoClose(2000)->timerProgressBar();
        }

        return redirect()->back();

    }


    /**
     * inventoryUpdate
     */
    public function inventoryUpdate(Request $request, $id)
    {

        $id = decrypt($id);
        $inventory = Inventory::findOrFail($id);
        // dd($request);
        $this->validate($request, array(
            'name'          =>  'required | string',
            'category_id'   =>  'required'
        ));

        if($inventory->inventory_type == 'quantity_inventory_item'){
            $this->validate($request, array(
                'quantity_price_per_unit'          =>  'required',
                'quantity_default_selling_price'   =>  'required',
            ));
        }
        if($inventory->inventory_type == 'unique_inventory_item'){
            $this->validate($request, array(
                'unique_purchase_price'          =>  'required',
                'unique_default_selling_price'          =>  'required',
            ));
        }

        if($request->inventory_url != null){
            $this->validate($request, array(
                'inventory_url'          =>  'regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
            ));
        }

        $inventory->name = $request->name;
        $inventory->cat_id = $request->category_id;

        if($request->hasFile('avatar')){

            $this->validate($request, array(
                "avatar" => 'required | image | mimes:jpeg,png,jpg,gif,svg | max:2048',
            ));

            $image_data=file_get_contents($request->avatar);

            $encoded_image=base64_encode($image_data);

            $inventory->avatar = 'data:image/png;base64,'.$encoded_image;
        }

        if($request->purchased_date != null){
            $date = new DateTime($request->purchased_date);
            $inventory->purchased_date = $date->format('Y-m-d');
        }

        $inventory->description =  ($request->description != null) ? $request->description:null;
        
        if($request->inventory_url != null)
            $inventory->inventory_url =   $request->inventory_url;

        if($inventory->inventory_type == 'quantity_inventory_item'){
            $inventory->purchase_price_per_unit = $request->quantity_price_per_unit;
            $inventory->default_selling_price = $request->quantity_default_selling_price;

            $inventory->sku = $request->quantity_sku;
            $inventory->global_quantity = $request->quantity_global_quantity;
            $inventory->low_quantity = $request->quantity_low_quantity;
            $inventory->unit_id = $request->quantity_unit_id;
            $inventory->sort_priority =  $request->quantity_shorting_priority;
        } else {
            $inventory->purchase_price = $request->unique_purchase_price;
            $inventory->default_selling_price = $request->unique_default_selling_price;

            $inventory->sku =  $request->unique_sku;
            $inventory->serial_number = $request->unique_serial_number;
            $inventory->unit_id = $request->unique_unit_id;
            $inventory->sort_priority = $request->unique_shorting_priority;
        }

        if($inventory->save()){
            toast('Inventory item has been updated.', 'success')->autoClose(2000)->timerProgressBar();
        } else{
            toast('Inventory item Does not updated.', 'error')->autoClose(2000)->timerProgressBar();
        }

        return redirect()->back();

    }


    /**
     * categoryIndex
     */
    public function categoryIndex()
    {
        $categories = Category::with(['Inventories'])->get();
        return view('admin.inventory.category.index', compact(['categories']));
    }


    /**
     * categoryStore
     */
    public function categoryStore(Request $request)
    {
        $this->validate($request, array(
            'name'  =>  'required | string | max:100'
        ));

        $category = new Category();

        $category->name = $request->name;
        if( $request->description )
            $category->description = $request->description;
        if( $request->inventory_type )
            $category->inventory_type = $request->inventory_type;
        if( $request->default_selling_price_markup )
            $category->default_selling_price = $request->default_selling_price_markup;

        if($category->save()){
            toast('Category has been Created.', 'success')->autoClose(2000)->timerProgressBar();
        } else{
            toast('Category Does not Created.', 'error')->autoClose(2000)->timerProgressBar();
        }

        return redirect()->back();
    }

    /**
     * categoryDestroy
     */
    public function categoryDestroy($id)
    {
        $id = decrypt($id);
        $category = Category::findOrFail($id);

        if($category->delete()){
            toast('Category has been deleted.', 'warning')->autoClose(2000)->timerProgressBar();
        } else{
            toast('Category Does not deleted.', 'error')->autoClose(2000)->timerProgressBar();
        }
        return redirect()->back();
    }

    /**
     * categoryStatus
     */
    public function categoryStatus($id)
    {
        $id = decrypt($id);
        $category = Category::findOrFail($id);
        $category->status = ($category->status == 1) ? 0:1;

        if($category->save()){
            toast('Category Status Has Been Changed.', 'warning')->autoClose(2000)->timerProgressBar();
        } else{
            toast('Category Status Doesn\'t changed.', 'error')->autoClose(2000)->timerProgressBar();
        }
        return redirect()->back();
    }


}
