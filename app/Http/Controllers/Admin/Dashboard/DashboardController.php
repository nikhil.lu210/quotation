<?php

namespace App\Http\Controllers\Admin\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use App\Models\Invoice;

class DashboardController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $invoices = Invoice::with(['customer', 'currency'])->get();
        // dd($invoices);
        return view('admin.dashboard.index', compact(['invoices']));
    }
}
