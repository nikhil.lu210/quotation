<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Category extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Relation one to Many
     *
     * @return custom relation data
     */
    public function inventories()
    {
        return $this->hasMany('App\Models\Inventory', 'cat_id', 'id');
    }


}
