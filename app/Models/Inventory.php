<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Inventory extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Relation one to Many
     *
     * @return custom relation data
     */
    public function category()
    {
        return $this->belongsTo('App\Models\Category', 'cat_id', 'id');
    }

    /**
     * Relation one to Many
     *
     * @return custom relation data
     */
    public function unit()
    {
        return $this->belongsTo('App\Models\Unit', 'unit_id', 'id');
    }
}
