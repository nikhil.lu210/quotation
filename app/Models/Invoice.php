<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Invoice extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Relation one to Many
     *
     * @return custom relation data
     */
    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'customer_id', 'id');
    }

    /**
     * Relation one to Many
     *
     * @return custom relation data
     */
    public function company()
    {
        return $this->belongsTo('App\Models\Company', 'company_id', 'id');
    }

    /**
     * Relation one to Many
     *
     * @return custom relation data
     */
    public function currency()
    {
        return $this->belongsTo('App\Models\Currency', 'currency_id', 'id');
    }

    /**
     * Relation one to Many
     *
     * @return custom relation data
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'sales_person', 'id');
    }

    /**
     * Relation one to Many
     *
     * @return custom relation data
     */
    public function items()
    {
        return $this->hasMany('App\Models\InvoiceCart', 'invoice_id', 'id');
    }
}
