<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Company extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * relation with admins
     *
     * one to many relationship
     *
     * @return admins
     */
    public function users(){
        return $this->hasMany('App\User', 'company_id');
    }
}
