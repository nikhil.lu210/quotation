<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Customer extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * relation with manager
     *
     * many to one relationship
     *
     * @return Manager
     */
    public function manager(){
        return $this->belongsTo('App\User', 'manager_id');
    }
}
