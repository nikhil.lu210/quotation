<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class InvoiceCart extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    
    /**
     * Relation one to Many
     *
     * @return custom relation data
     */
    public function item()
    {
        return $this->belongsTo('App\Models\Inventory', 'item_id', 'id');
    }
}
