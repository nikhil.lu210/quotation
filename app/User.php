<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // ============================== Relational Models ===================================

    /**
     * relation with customer
     *
     * one to many relationship
     *
     * @return Customers
     */
     public function customers(){
         return $this->hasMany('App\Models\Customer', 'manager_id');
     }

    /**
     * relation with customer
     *
     * one to many relationship
     *
     * @return Customers
     */
     public function company(){
         return $this->belongsTo('App\Models\Company', 'company_id');
     }
}
